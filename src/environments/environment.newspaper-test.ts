import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel, HistogramFieldType,
  SettingsModel,
  SortOrder
} from '../app/shared/models/settings.model';

export const environment: SettingsModel = {
  name: 'Digitalisierte Zeitungen',
  production: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-ezasnew.ub.unibas.ch/",

// optionale Header-Anzeige-Einstellungen
  headerSettings: {
    // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
    disable: false,
    showPortalName: true,
    // use this department name instead of the portal name
    logoSubTitle: "Universitätsbibliothek",
    // display this logo at the right side of the header
    // departmentLogoUrl:
    // use this URL for the link of "departmentLogoUrl:"
    // departmentUrl:
    // disable whole language bar if true otherwise the bar is displayed
    disableLanguageBar: false,

  },
  proxyUrl: "https://ub-zas-test-proxy.ub.unibas.ch/v1/rdv_query/es_proxy/",
  moreProxyUrl: "https://ub-zas-test-proxy.ub.unibas.ch/v1/rdv_query/further_snippets/",
  inFacetSearchProxyUrl: "https://ub-zas-test-proxy.ub.unibas.ch/v1/rdv_query/facet_search/",
  iiifProxyUrl: "https://ub-zas-test-proxy.ub.unibas.ch/v1/rdv_query/iiif_flex_pres/",

  // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
  // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
  // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
  // angezeigt (mit messageTitleKey: optional).
  iiifRestrictions: [
    {
      field: "object_type",
      messageTitleKey: "search-results.view-mode.IIIF.object_type.message.title",
      messageTextKey: "search-results.view-mode.IIIF.object_type.message.text",
      values: [
        {label: "Zeitungsartikel", value: {"id": "Zeitungsartikel"}},
        {label: "retrodigitalisierte Artikel", value: {"id": "retrodigitalisierte Artikel"}},
        {label: "elektronische Artikel (ab 2013)", value: {"id": "elektronische Artikel (ab 2013)"}}
      ]
    }
  ],

  detailProxyUrl: "https://ub-zas-test-proxy.ub.unibas.ch/v1/rdv_object/object_view/",
  navDetailProxyUrl: "https://ub-zas-test-proxy.ub.unibas.ch/v1/rdv_query/next_objectview/",

  // die Webapp kann auch die lokale Version verwenden
  universalViewerUrl: "assets/uv/uv.html",

  // Extra-Konfiguration für den Universal Viewer (UV)
  // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
  // uvViewerConfig: "https://ub-universalviewer.ub.unibas.ch/uv/lib/zettelkatalog.json",

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "date",
      order: SortOrder.ASC,
      display: "select-sort-fields.date_asc"
    },
    {
      field: "date",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "Quelldatum": {
      "field": "date",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "env.facetFields.Quelldatum",
      "operator": "AND",
      "showAggs": true,
      "order": 0,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "newspaper": {
      "field": "newspaper.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.Quellname",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true
    },
    "issue_type": {
      "field": "pages.issue_type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ausgabetyp",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "issue_number": {
      "field": "issue_number.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ausgaben-Nummer",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 4,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "goobi_name": {
      "field": "goobi_name.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Goobi-Name",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 5,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "page": {
      "field": "page",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Seitenanzahl",
      "operator": "AND",
      "showAggs": true,
      "order": 11,
      "size": 100,
      "expandAmount": 10
    } as HistogramFieldModel,
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {
    "Quelldatum": {
      "field": "Quelldatum.keyword",
      "facetType": FacetFieldType.RANGE,
      "label": "Quelldatum",
      "from": "1960-01-01",
      "to": "2200-01-01",
      "min": "1960-01-01",
      "max": "2200-01-01",
      "showMissingValues": true,
      "order": 1,
      "expandAmount": 5
    }
  },

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "extraInfo": true,
      "field": "title",
      "label": "Dossiertitel",
      "landingpage": true,
      "sort": "title.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "field": "Quellname",
      "label": "Zeitung",
      "sort": "Quellname.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "field": "Quelldatum",
      "label": "Quelldatum",
      "sort": "Quelldatum.keyword"
    }
  ],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    "descr_fuv": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_fuv",
      "label": "Firmen und Verb\u00e4nde"
    },
    "descr_person": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_person",
      "label": "Personen"
    },
    "descr_sach": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_sach",
      "label": "Sachdeskriptor"
    },
    "object_type": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "object_type",
      "label": "Objekttyp"
    }
  }

};
