import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel, HistogramFieldType,
  SettingsModel,
  SortOrder
} from '../app/shared/models/settings.model';

export const environment: SettingsModel = {
  name: 'Porträtsammlung',
  production: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-ezasnew.ub.unibas.ch/",

  proxyUrl: "http://127.0.0.1:5000/v1/rdv_query/es_proxy/",
  moreProxyUrl: "http://127.0.0.1:5000/v1/rdv_query/further_snippets/",
  inFacetSearchProxyUrl: "http://127.0.0.1:5000/v1/rdv_query/facet_search/",

  detailProxyUrl: "http://127.0.0.1:5000/v1/rdv_object/object_view/",
  navDetailProxyUrl: "http://127.0.0.1:5000/v1/rdv_query/next_objectview/",


  externalAboutUrl: "https://ub-easyweb.ub.unibas.ch/de/ub-wirtschaft-swa/swa-446/",

  // die Webapp kann auch die lokale Version verwenden
  universalViewerUrl: "assets/uv/uv.html",

  // Extra-Konfiguration für den Universal Viewer (UV)
  // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
  // uvViewerConfig: "https://ub-universalviewer.ub.unibas.ch/uv/lib/zettelkatalog.json",

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "cat_type": {
      "field": "cat_type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Objekttyp",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true
    },
    "cat_person": {
      "field": "cat_person.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Person",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    }
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {
  },

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": true,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [
  ],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
  }

};
