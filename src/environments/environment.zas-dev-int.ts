import {
  FacetFieldType,
  HistogramFieldModel, HistogramFieldType,
  SettingsModel,
} from '../app/shared/models/settings.model';
import { environment as dev } from './environment.zas-dev';
export const environment: SettingsModel = {
  ...dev,
  name: 'Rechercheportal Zeitungsausschnitte (intern) Beta',
  baseUrl: "https://ub-zas.ub.unibas.ch/",

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    ...dev.facetFields,
    "ki_descr_sach_prefLabel": {
      "field": "ki_descr_sach_prefLabel.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Themen KI (experimentell)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "pages": {
      "field": "pages",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Seiten-Anzahl",
      "operator": "AND",
      "showAggs": true,
      "order": 11,
      "size": 100,
      "expandAmount": 10
    } as HistogramFieldModel
    ,
    "Farbcode": {
      "field": "Farbcode.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bearbeitungscode",
      "operator": "OR",
      "showAggs": true,
      "order": 12,
      "size": 100,
      "expandAmount": 10
    } as HistogramFieldModel,
  "Export": {
    "field": "Export.keyword",
    "facetType": FacetFieldType.SIMPLE,
    "label": "Export",
    "operator": "OR",
    "showAggs": true,
    "order": 13,
    "size": 100,
    "expandAmount": 10
  } as HistogramFieldModel,
}
};
