import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel, HistogramFieldType,
  SettingsModel,
  SortOrder
} from '../app/shared/models/settings.model';

export const environment: SettingsModel = {
  name: 'Rechercheportal Zeitungsausschnitte',
  production: false,

  // per Default werden Preview Images angezeigt
  // showImagePreview: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "https://ub-zas.ub.unibas.ch/",

  // optionale Header-Anzeige-Einstellungen
  headerSettings: {
    // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
    disable: false,
    showPortalName: true,
    // use this department name instead of the portal name
    logoSubTitle: "Universitätsbibliothek",
    // display this logo at the right side of the header
    departmentLogoUrl: "https://wirtschaftsarchiv.ub.unibas.ch/typo3conf/ext/site_configurator/Resources/Public/Logos/Logo_Unibas_SWA_ALL.svg?1589207645",
    // use this URL for the link of "departmentLogoUrl:"
    departmentUrl: "https://wirtschaftsarchiv.ub.unibas.ch/de/home/",
    // disable whole language bar if true otherwise the bar is displayed
    disableLanguageBar: false,
    // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
    showBetaBar: true,
    // optionaler lokalisierter Kontakt-Text:
    betaBarContact: {name: "Martin Reisacher", email: "martin.reisacher@unibas.ch"},
  },
  proxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/es_proxy/",
  moreProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/further_snippets/",
  inFacetSearchProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/facet_search/",
  popupQueryProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/popup_query/",
  iiifProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/iiif_flex_pres/",

  // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
  // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
  // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
  // angezeigt (mit messageTitleKey: optional).
  iiifRestrictions: [
    {
      field: "object_type",
      messageTitleKey: "search-results.view-mode.IIIF.object_type.message.title",
      messageTextKey: "search-results.view-mode.IIIF.object_type.message.text",
      values: [
        {label: "Zeitungsausschnitt", value: {"id": "Zeitungsausschnitt"}},
        {label: "retrodigitalisierter Zeitungsausschnitt", value: {"id": "retrodigitalisierter Zeitungsausschnitt"}},
        {label: "elektronischer Zeitungsausschnitt (ab 2013)", value: {"id": "elektronischer Zeitungsausschnitt (ab 2013)"}}
      ]
    }
  ],

  detailProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_object/object_view/",
  navDetailProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/next_objectview/",


  externalAboutUrl: "https://ub-easyweb.ub.unibas.ch/de/ub-wirtschaft-swa/swa-446/",

  // die Webapp kann auch die lokale Version verwenden
  universalViewerUrl: "assets/uv/uv.html",

  // Extra-Konfiguration für den Universal Viewer (UV)
  // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
  uvViewerConfig: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "Quelldatum",
      order: SortOrder.ASC,
      display: "select-sort-fields.date_asc"
    },
    {
      field: "Quelldatum",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "Quelldatum": {
      "field": "Quelldatum",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "env.facetFields.Quelldatum",
      "operator": "AND",
      "showAggs": true,
      "order": 0,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "title": {
      "field": "title.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.title",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true
    },
    "stw_ids": {
      "field": "stw_ids.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.descr_sach_prefLabel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "hierarchy_filter": {
      "field": "hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "env.facetFields.descr_sach_hierarchy_filter",
      "operator": "AND",
      "order": 4,
      "size": 100,
      "expandAmount": 100
    },
    "descr_fuv": {
      "field": "descr_fuv.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.descr_fuv",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 5,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "descr_person": {
      "field": "descr_person.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.descr_person",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 6,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "Quellname": {
      "field": "Quellname.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.Quellname",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 7,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "object_type": {
      "field": "object_type.keyword",
      "facetType": FacetFieldType.SUBCATEGORY,
      "label": "env.facetFields.object_type",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 8,
      "size": 100,
      "expandAmount": 10
    },
    "Sprache": {
      "field": "Sprache.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.Sprache",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 9,
      "size": 100,
      "expandAmount": 5
    },
    "descr_place": {
      "field": "descr_place.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.fct_place",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true
    },
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {
    "Quelldatum": {
      "field": "Quelldatum.keyword",
      "facetType": FacetFieldType.RANGE,
      "label": "Quelldatum",
      "from": "1960-01-01",
      "to": "2200-01-01",
      "min": "1960-01-01",
      "max": "2200-01-01",
      "showMissingValues": true,
      "order": 1,
      "expandAmount": 5
    }
  },

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "extraInfo": true,
      "field": "title",
      "label": "Dossiertitel",
      "landingpage": true,
      "sort": "title.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "field": "Quellname",
      "label": "Zeitung",
      "sort": "Quellname.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "field": "Quelldatum",
      "label": "Quelldatum",
      "sort": "Quelldatum.keyword"
    }
  ],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    "descr_fuv": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_fuv",
      "label": "Firmen und Verb\u00e4nde"
    },
    "descr_person": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_person",
      "label": "Personen"
    },
    "descr_sach": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_sach",
      "label": "Sachdeskriptor"
    },
    "object_type": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "object_type",
      "label": "Objekttyp"
    }
  }

};
