import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel, HistogramFieldType,
  SettingsModel,
  SortOrder
} from '../app/shared/models/settings.model';

export const environment: SettingsModel = {
  name: 'Rechercheportal Zeitungsausschnitte',
  production: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-ezasnew.ub.unibas.ch/",

  proxyUrl: "http://ub-zas-test-proxy.ub.unibas.ch/v1/rdv_query/es_proxy/",
  moreProxyUrl: "http://ub-zas-test-proxy.ub.unibas.ch/v1/rdv_query/further_snippets/",
  inFacetSearchProxyUrl: "http://ub-zas-test-proxy.ub.unibas.ch/v1/rdv_query/facet_search/",

  detailProxyUrl: "http://ub-zas-test-proxy.ub.unibas.ch/v1/rdv_object/object_view/",
  navDetailProxyUrl: "http://ub-zas-test-proxy.ub.unibas.ch/v1/rdv_query/next_objectview/",


  externalAboutUrl: "https://ub-easyweb.ub.unibas.ch/de/ub-wirtschaft-swa/swa-446/",

  // die Webapp kann auch die lokale Version verwenden
  universalViewerUrl: "assets/uv/uv.html",

  // Extra-Konfiguration für den Universal Viewer (UV)
  // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
  // uvViewerConfig: "https://ub-universalviewer.ub.unibas.ch/uv/lib/zettelkatalog.json",

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "Quelldatum",
      order: SortOrder.ASC,
      display: "select-sort-fields.date_asc"
    },
    {
      field: "Quelldatum",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "Quelldatum": {
      "field": "Quelldatum",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Quelldatum",
      "operator": "AND",
      "showAggs": true,
      "order": 0,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "title": {
      "field": "title.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Dossiertitel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true
    },
    "descr_sach_prefLabel": {
      "field": "descr_sach_prefLabel.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Themen",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "descr_sach_prefLabel_not": {
      "field": "descr_sach_prefLabel.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Themen (NOT)",
      "operator": "NOT",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "ki_descr_sach_prefLabel": {
      "field": "ki_descr_sach_prefLabel.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Themen KI (experimentell)",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "ki_descr_sach_prefLabel_not": {
      "field": "ki_descr_sach_prefLabel.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Themen KI NOT (experimentell)",
      "operator": "NOT",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "descr_sach_hierarchy_filter": {
      "field": "descr_sach_hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "Themen systematisch",
      "operator": "AND",
      "order": 4,
      "size": 100,
      "expandAmount": 100
    },
    "descr_fuv": {
      "field": "descr_fuv.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Firmen und Organisationen",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 5,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "descr_person": {
      "field": "descr_person.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Personen",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 6,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "Quellname": {
      "field": "Quellname.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Zeitung",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 7,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "object_type": {
      "field": "object_type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Objekttyp",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 8,
      "size": 100,
      "expandAmount": 10
    },
    "Sprache": {
      "field": "Sprache.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sprache",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 9,
      "size": 100,
      "expandAmount": 5
    },
    "fct_place": {
      "field": "fct_place.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true
    },
    "pages": {
      "field": "pages",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Seiten-Anzahl",
      "operator": "AND",
      "showAggs": true,
      "order": 11,
      "size": 100,
      "expandAmount": 10
    } as HistogramFieldModel,
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {
    "Quelldatum": {
      "field": "Quelldatum.keyword",
      "facetType": FacetFieldType.RANGE,
      "label": "Quelldatum",
      "from": "1960-01-01",
      "to": "2200-01-01",
      "min": "1960-01-01",
      "max": "2200-01-01",
      "showMissingValues": true,
      "order": 1,
      "expandAmount": 5
    }
  },

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": true,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "extraInfo": true,
      "field": "title",
      "label": "Dossiertitel",
      "landingpage": true,
      "sort": "title.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "field": "Quellname",
      "label": "Zeitung",
      "sort": "Quellname.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "field": "Quelldatum",
      "label": "Quelldatum",
      "sort": "Quelldatum.keyword"
    }
  ],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    "descr_fuv": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_fuv",
      "label": "Firmen und Verb\u00e4nde"
    },
    "descr_person": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_person",
      "label": "Personen"
    },
    "descr_sach": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_sach",
      "label": "Sachdeskriptor"
    },
    "object_type": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "object_type",
      "label": "Objekttyp"
    }
  }

};
