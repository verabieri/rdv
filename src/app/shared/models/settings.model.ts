/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * To be implemented by settings (environment) files
 */
export interface SettingsModel {
  /**
   * Name
   */
  readonly name?: string;

  /**
   * Used in production
   */
  readonly production: boolean;

  /**
   * Display preview images of documents.
   * Images are displayed if this field is absent or true.
   */
  readonly showImagePreview?: boolean;

  /**
   * Backend store
   */
  readonly backend: Backend;

  /**
   * Base URL (used as prefix for links)
   */
  readonly baseUrl: string;

  /**
   * URL of proxy script
   */
  readonly proxyUrl: string;

  /**
   * URL of more proxy script
   */
  readonly moreProxyUrl?: string;

  /**
   * Used if result list is displayed in IIIF Universal viewer.
   */
  readonly iiifProxyUrl?: string;

  /**
   * Used to get popup info with normal search results. Only used
   * for external links.
   */
  readonly popupQueryProxyUrl?: string;

  /**
   * URL of detail proxy script
   */
  readonly detailProxyUrl?: string;

  /**
   * URL of next detail proxy script
   */
  readonly navDetailProxyUrl?: string;

  /**
   * In-facet search proxy endpoint.
   */
  readonly inFacetSearchProxyUrl?: string;

  /**
   * About link to external system.
   */
  readonly externalAboutUrl?: string;

  /**
   * Help link to external system.
   */
  readonly externalHelpUrl?: string;

  /**
   * About link to external system.
   */
  readonly universalViewerUrl?: string;

  /**
   * Optional config for IIIF viewer.
   */
  readonly uvViewerConfig?: string;

  /**
   * Search field configurations
   */
  readonly searchFields: SearchFieldsModel;

  /**
   * Available sort options.
   */
  readonly sortFields?: SortFieldsModel[];

  /**
   * Filter configurations
   */
  readonly filterFields: FilterFieldsModel;

  /**
   * Facet configurations
   */
  readonly facetFields: FacetFieldsModel;

  /**
   * Range configurations
   * @Deprecated put these facets into facetFields too
   */
  readonly rangeFields: RangeFieldsModel;

  /**
   * Selectable element per page options
   */
  readonly rowOpts: number[];

  /**
   * Result list display options
   */
  readonly queryParams: QueryParamsModel;

  /**
   * Basket element list display options
   */
  readonly basketConfig: {
    readonly queryParams: QueryParamsModel,
  };

  /**
   * Show export list button
   */
  readonly showExportList: {
    /**
     * In basket element list
     */
    readonly basket: boolean,
    /**
     * In result list
     */
    readonly table: boolean,
  };

  /**
   *
   */
  readonly tableFields: TableColumnModel[];
  readonly extraInfos: ExtraInfosModel;

  /**
   * Customize header area (optional).
   */
  readonly headerSettings?: HeaderSettingsModel;

  /**
   * When switching to IIIF result view only the listed facet values are preserved.
   * All other selected values of this facet are automatically removed.
   * If no selected value remains, the first facet value of the list is automatically selected.
   * If messageTextKey: is specified, a localized toast message is displayed
   * (with optional messageTitleKey:).
   */
  readonly iiifRestrictions?: IIIFRestriction[];
}

export interface IIIFRestriction {
  field: string;
  messageTitleKey?: string;
  messageTextKey: string;
  values: any[];
}

/**
 * Used backend
 */
export enum Backend {
  'ELASTIC' = 'elastic',
  'SOLR' = 'solr'
}

/**
 * Sort order of selected row in list
 */
export enum SortOrder {
  'ASC' = 'asc',
  'DESC' = 'desc',
}

/**
 * Type of extra information field. If set to `LINK`, a link is created out of the text
 */
export enum ExtraInfoFieldType {
  'LINK' = 'link',
  'TEXT' = 'text',
}

export enum FacetFieldType {
  'SIMPLE' = 'simple',
  'RANGE' = 'range',
  'HISTOGRAM' = 'histogram',
  'HIERARCHIC' = 'hierarchic',
  'SUBCATEGORY' = 'subcat',
}

export enum HistogramFieldType {
  DATE = 'date',
  INT = 'int',
  GEO = 'geo'
}

/**
 * Queryable search fields
 */
interface SearchFieldsModel {
  /**
   * List elements. Key is sent to backend, while value is shown in UI
   */
  readonly options: any;
  /**
   * Preselected element
   */
  readonly preselect: string[];
}

/**
 * Available sort option.
 */
export interface SortFieldsModel {
  /**
   * A field.
   */
  field: string;
  /**
   * Sort direction.
   */
  order: SortOrder;
  /**
   * Label displayed in UI.
   */
  display: string;
}

/**
 * Search filter configuration
 */
interface FilterFieldsModel {
  [index: string]: {
    /**
     * Field name
     */
    readonly field: string;
    /**
     * Displayed label
     */
    readonly label: string;
    /**
     * Url used for remotely fetching filter options
     */
    readonly url?: string;
    /**
     * Available options. Empty if options are fetched remotely.
     */
    readonly options: {
      /**
       * Displayed label
       */
      readonly label: string;
      /**
       * Option value
       */
      readonly value: string;
    }[];
  }
}

export interface CommonFacetFieldModel {
  /**
   * Facet's name
   */
  readonly field: string;

  /**
   * Displayed label
   */
  readonly label: string;

  /**
   * List order relative to other facet AND range categories
   */
  readonly order: number;

  /**
   * Number of facet values to show if "more" button is clicked.
   */
  readonly expandAmount?: number;

  /**
   * Type of facet.
   */
  readonly facetType?: FacetFieldType;
  /**
   * Passed to request to define how many entries shall be returned from backen on first call.
   */
  readonly size?: number;

  /**
   * Is search in facet values wanted?
   */
  readonly searchWithin?: boolean;
}

export interface FacetFieldModel extends CommonFacetFieldModel {
  /**
   * Default operator for combining individual facet elements
   */
  readonly operator: string;

  /**
   * List of all available operators
   */
  readonly operators?: string[];
}

export interface HistogramFieldModel extends FacetFieldModel {
  /**
   * Type of histogram.
   */
  data_type: HistogramFieldType;

  /**
   * Aggregation
   */
  showAggs: boolean;
}

export interface RangeFieldModel extends CommonFacetFieldModel {
  /**
   * First originally displayed value
   */
  readonly from: any;
  /**
   * Last possible value
   */
  readonly max: any;
  /**
   * First possible value
   */
  readonly min: any;
  /**
   * Show results who lacks respective values nevertheless
   */
  readonly showMissingValues: boolean;
  /**
   * Last originally displayed value
   */
  readonly to: any;
}

/**
 * Facet categories configuration
 */
export interface FacetFieldsModel {
  [index: string]: FacetFieldModel;
}

/**
 * Range categories configuration
 */
export interface RangeFieldsModel {
  [index: string]: RangeFieldModel
}

/**
 * Result list configuration
 */
export interface QueryParamsModel {
  /**
   * Number of elements / rows per page
   */
  readonly rows: number;
  /**
   * Original offset (0 if not set explicitly)
   */
  readonly offset?: number;
  /**
   * Sort by field
   */
  readonly sortField: string;
  /**
   * Sort order
   */
  readonly sortOrder: SortOrder
  /**
   * Display search result in IIIF Universal Viewer or list.
   */
  readonly showInIIIF?: boolean;
}

/**
 * Configuration for table column
 */
export interface TableColumnModel {
  /**
   * Additional css classes used in fields
   */
  readonly css: string;
  /**
   * Enable button to show additional information (per field; disabled per default)
   */
  readonly extraInfo?: boolean;
  /**
   * Column name (equal to field name in backend)
   */
  readonly field: string;
  /**
   * Displayed label of column
   */
  readonly label: string;
  /**
   * Link to detailed view (per field; disabled per default)
   */
  readonly landingpage?: boolean;
  /**
   * Column name used for sorting
   */
  readonly sort: string;

  /**
   * Flag to indicate that this column contains the name of a field.
   */
  readonly isTitle?: boolean;
}

/**
 * Field's additional information configuration (information shown when extra
 * information drop-down is activated)
 */
interface ExtraInfosModel {
  [index: string]: {
    /**
     * Content type of field
     */
    readonly display: ExtraInfoFieldType;
    /**
     * Field's name
     */
    readonly field: string;
    /**
     * Displayed label
     */
    readonly label: string;
  }
}

interface HeaderSettingsModel {
  // disable whole header if true otherwise the header is displayed
  disable?: boolean;
  // display portal name in separate area
  showPortalName?: boolean;
  // use this department name instead of the portal name
  logoSubTitle?: string;
  // display this logo at the right side of the header
  departmentLogoUrl?: string;
  // use this URL for the link of "departmentLogoUrl:"
  departmentUrl?: string;
  // disable whole language bar if true otherwise the bar is displayed
  disableLanguageBar?: boolean;
  // display FR language button in header
  showFrenchLanguageSwitch?: boolean;
  // display IT language button in header
  showItalianLanguageSwitch?: boolean;
  // displays red beta bar on top of UI
  showBetaBar?: boolean;
  // optionally display the contact's email in beta bar
  betaBarContact?: { name: string, email: string };
}
