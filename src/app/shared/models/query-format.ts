/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * Query parameters. Used for communication with backend.
 */
export class QueryFormat {
  /**
   * Selected search fields
   */
  searchFields = {};
  /**
   * Selected facet fields
   */
  facetFields = {};
  /**
   * Selected filters
   */
  filterFields = {};
  /**
   * Selected ranges
   * @Deprecated use histogramFields instead
   */
  rangeFields = {};
  /**
   * Selected histograms.
   */
  histogramFields = {};
  /**
   * Selected hierarchic fields.
   */
  hierarchicFields = {};
  /**
   * Settings for result list
   */
  queryParams = {
    "rows": 10,
    "start": 0,
    "sortField": "",
    "sortDir": ""
  };
  /**
   * Optionally search in facet instead of database.
   */
  inFacetSearchPrefix?: string;
  inFacetSearchField?: string;
  /**
   * set this field to load more docs for current search.
   */
  search_after?: any;

  /**
   * Set this flag to true if the search result shall be displayed in the
   * IIIF Universal viewer.
   */
  resultInIIIF?: boolean;
  /**
   * Run special search which is used for external links.
   */
  popupId?: string;
  /**
   * Current language: de | en
   */
  lang: string;
  /**
   * For facet filters in URL, return their labels etc. too.
   */
  return_selection?: string;
  /**
   * Informs backend to store the corresponding IIF Universal Viewer manifest URL.
   */
  persist?: boolean;
}
