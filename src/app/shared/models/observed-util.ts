import {select, Store} from "@ngrx/store";
import * as fromSearch from "../../search/reducers";
import {combineLatest} from "rxjs/index";
import {map} from "rxjs/operators";

/**
 * Returns an observer which is triggered on any change of search params.
 * @param {Store<State>} searchStore
 * @returns {Observable<any>}
 */
export function searchParamObserver(searchStore: Store<fromSearch.State>) {
  return combineLatest([
    searchStore.pipe(
      select(fromSearch.getQueryParams),
      map(val => {
          return {
            queryParams: {
              rows: val.rows,
              sortDir: val.sortOrder,
              sortField: val.sortField,
              start: val.offset
            },
            resultInIIIF: val.showInIIIF
          };
        }
      ),
    ),
    searchStore.pipe(select(fromSearch.getCombinedNewSearchQueries))
  ]).pipe(
    map(val => Object.assign(val[0], val[1])));
}
