/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Injectable} from '@angular/core';
import {QueryFormat} from '../models/query-format';
import {Observable} from 'rxjs';
import {Basket} from '../../search/models/basket.model';
import {Error} from "tslint/lib/error";

@Injectable({
  providedIn: 'root'
})
export abstract class BackendSearchService {
  abstract getBackendDataComplex(queryFormat: QueryFormat): Observable<any>;

  abstract getBackendDetailData(id: string, fullRecord: boolean): Observable<any>;

  /**
   * Get following or preceding document.
   * @param {string} id
   * @param {QueryFormat} queryFormat
   * @param {boolean} next if true get following else preceding
   * @returns {Observable<any>}
   */
  navBackendDetailData(id: string, queryFormat: QueryFormat, next: boolean): Observable<any> {
    throw new Error("Did you forget to override the method BackendSearchService.navBackendDetailData()?");
  }

  abstract getBackendDataBasket(basket: Basket): Observable<any>;

  /**
   * Converts hierarchical value in deep url format to backend specific value.
   * Opposite of getDeepUrlHierarchicValue().
   * @param deepUrlFormat
   * @returns {any}
   */
  buildHierarchicValue(deepUrlFormat): any {
    throw new Error("Did you forget to override the method BackendSearchService.buildHierarchicValue()?");
  }

  /**
   * Converts hierarchical value in backend specific format to deep url value.
   * Opposite of buildHierarchicValue().
   * @param hierarchicValue
   * @returns {[any, any]}
   */
  getDeepUrlHierarchicValue(hierarchicValue): [any, any] {
    throw new Error("Did you forget to override the method BackendSearchService.getDeepUrlHierarchicValue()?");
  }

  /**
   * Get start and end of a histogram value.
   * The return array always contains two values, one of them is maybe undefined.
   * @returns {[number]}
   */
  getHistogramRange(selectedHistogramValue): [number, number] {
    throw new Error("Did you forget to override the method BackendSearchService.getHistogramRange()?");
  }

  /**
   * Search within a facet.
   * @param {QueryFormat} queryFormat
   * @returns {Observable<any>}
   */
  getInFacetSearchBackendData(queryFormat: QueryFormat): Observable<any> {
    throw new Error("Did you forget to override the method BackendSearchService.getInFacetSearchBackendData()?");
  }

  /**
   * Load more docs of current search.
   * @param {QueryFormat} queryFormat
   * @returns {Observable<any>}
   */
  getMoreBackendDataComplex(queryFormat: QueryFormat): Observable<any> {
    throw new Error("Did you forget to override the method BackendSearchService.getMoreBackendDataComplex()?");
  }
}
