/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {inject, TestBed} from '@angular/core/testing';

import {FacetValueProcessor, SearchActions, UrlParamsService} from './url-params.service';
import {Action, StoreModule} from "@ngrx/store";

import * as fromSearch from '../../search/reducers';


class TestProc implements FacetValueProcessor {
  collectedInfos = {facets: [], histogram: [], hierarhic: [], operators: []};

  processSimpleFacet?(field: string, value: string): SearchActions[] {
    this.collectedInfos.facets.push({field, value});
    return [];
  }

  processHistogramFacet?(field: string, from: string, to: string): SearchActions[] {
    this.collectedInfos.histogram.push({field, from, to});
    return [];
  }

  processHierarchicFacet?(field: string, label: string, values: string[]): SearchActions[] {
    this.collectedInfos.hierarhic.push({field, values});
    return [];
  }

  processFacetOperator?(field: string, operator: string): SearchActions[] {
    this.collectedInfos.operators.push({field, operator});
    return [];
  }
}

describe('UrlParamService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({
          ...fromSearch.reducers,
        })],
      providers: [UrlParamsService]
    });
  });

  it('should be created', inject([UrlParamsService], (service: UrlParamsService) => {
    expect(service).toBeTruthy();
    const queryParams = "facets=Quellname%7ELe+Temps%21Der+Bund%0A";
    const up = new URLSearchParams(queryParams);
    const proc: TestProc = new TestProc();
    // don't use decodeURIComponent(), because "+" aren't resolved!
    service.parseFacet(up.get("facets"), proc);
    expect(proc.collectedInfos.facets.length).toEqual(2);
    expect(proc.collectedInfos.facets[0]).toEqual({field: 'Quellname', value: 'Le Temps'});
    expect(proc.collectedInfos.facets[1]).toEqual({field: 'Quellname', value: 'Der Bund\x0a'});
  }));
});
