import {UrlSerializer, UrlTree, DefaultUrlSerializer, Params} from "@angular/router";

export class URLSearchParamsUrlSerializer implements UrlSerializer {
  parse(url: string): UrlTree {
    const dus = new DefaultUrlSerializer();
    const ut = dus.parse(url);
    const newQueryParams = this.handleUrlParams();
    ut.queryParams = newQueryParams;
    return ut;
  }

  handleUrlParams(): Params {
    const urlParams = new URLSearchParams(window.location.search);
    const newParams: any = {};
    urlParams.forEach((v, key) => {
      newParams[key] = urlParams.getAll(key);
      if (newParams[key].length === 1) {
        newParams[key] = newParams[key][0];
      }
    });
    return newParams;
  }

  serialize(tree: UrlTree): string {
    const dus = new DefaultUrlSerializer();
    let url = dus.serialize(tree);
    const qmPos = url.indexOf('?');
    if (qmPos >= 0) {
      const urlParams = new URLSearchParams();
      Object.keys(tree.queryParams).map((k) => {
        const val = tree.queryParams[k];
        if (Array.isArray(val)) {
          for (const v of val) {
            urlParams.append(k, v);
          }
        } else {
          urlParams.set(k, val);
        }
      });
      const newQueryParams = "" + urlParams;
      url = url.substring(0, qmPos + 1) + newQueryParams;
      if (tree.fragment) {
        url += "#" + encodeURIComponent(tree.fragment);
      }
    }
    return url;
  }

}
