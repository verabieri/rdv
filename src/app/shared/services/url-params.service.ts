/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Injectable} from '@angular/core';
import {environment} from "../../../environments/environment";
import {
  FacetFieldModel,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType,
  SortFieldsModel,
  SortOrder
} from "../models/settings.model";
import {AbstractHistogramFacetComponent, EmittedValues, ValidatorResult} from "../../search/components/abstract-histogram-facet.component";
import {stringToEnumValue} from "../utils";
import {take} from "rxjs/operators";
import * as fromSearch from "../../search/reducers";
import {Action, select, Store} from "@ngrx/store";
import {BackendSearchService} from "./backend-search.service";
import * as fromFormActions from "../../search/actions/form.actions";
import * as fromQueryActions from "../../search/actions/query.actions";

export type SearchActions = fromFormActions.AddFacetValue
  | fromFormActions.AddHistogramBoundaries
  | fromFormActions.AddHierarchicFacetValue
  | fromFormActions.UpdateSearchFieldValue
  | fromQueryActions.SetSortOrder
  | fromQueryActions.SetSortField
  | fromFormActions.UpdateFacetOperator;

export interface FacetValueProcessor {
  processSimpleFacet?(field: string, label: string, value: any): SearchActions[];

  processHistogramFacet?(field: string, label: string, value: any): SearchActions[];

  processHierarchicFacet?(field: string, label: string, values: any): SearchActions[];

  processFacetOperator?(field: string, operator: string): SearchActions[];
}

export interface SearchValueProcessor {
  processSimpleSearchValue(searchText: string, searchField: string): SearchActions[];

  processSorting(sortField: string, sortDir: SortOrder): SearchActions[];
}

export enum UrlParamNames {
  FACETS = 'facets',
  SEARCHED_TEXT = 'searchText',
  SEARCHED_FIELD = 'searchField',
  SORT_FIELD = 'sortField',
  SORT_DIR = 'sortDir',
  OPS = 'ops',
}

interface UrlFacetBuilder {
  key: string;
  operator?: string;

  toUrlString(): string;

  setOperator(op: string);
}

export class SelectedFacet implements UrlFacetBuilder {
  key: string;
  value: string[];
  operator?: string;

  constructor(field: string) {
    this.key = field;
    this.value = [];
  }

  setOperator(op: string) {
    this.operator = op;
  }

  addValue(value: string) {
    this.value.push(value);
  }

  toUrlString(): string {
    return this.value.join("!");
  }
}

export class SelectedHistogram implements UrlFacetBuilder {
  key: string;
  value: [any, any][];
  operator?: string;

  constructor(field: string) {
    this.key = field;
    this.value = [];
  }

  setOperator(op: string) {
    this.operator = op;
  }

  addRange(from, to) {
    this.value.push([from, to]);
  }

  toUrlString(): string {
    let s = "";
    for (let i = 0; i < this.value.length; i++) {
      s += "(";
      if (this.value[i][0] !== undefined) {
        s += this.value[i][0];
      }
      s += "!";
      if (this.value[i][1] !== undefined) {
        s += this.value[i][1];
      }
      s += ")";
    }
    return s;
  }
}

export class SelectedHierarchicFacet implements UrlFacetBuilder {
  key: string;
  value: { fst: string, snd: string, label: string }[];
  operator?: string;

  constructor(field: string) {
    this.key = field;
    this.value = [];
  }

  setOperator(op: string) {
    this.operator = op;
  }

  addValue(fstValue: string, sndValue: string, label: string) {
    this.value.push({fst: fstValue, snd: sndValue, label: label});
  }

  toUrlString(): string {
    let s = "";
    for (let i = 0; i < this.value.length; i++) {
      s += "(";
      s += this.value[i].fst;
      s += "!";
      s += this.value[i].snd;
      s += "!";
      // pre-encode "(" and ")" to simplify matching when deep url is parsed
      s += this.value[i].label.replace("(", "%28").replace(")", "%29");
      s += ")";
    }
    return s;
  }
}


export class ReplaceUrlParams {
  protected searchedText?: string;
  protected searchedField?: string;
  protected facets: UrlFacetBuilder[] = [];
  protected sortField?: string;
  protected sortDir?: string;

  constructor(protected searchStore: Store<fromSearch.State>, protected backendSearchService: BackendSearchService) {
    this.prepareCurrentSearch();
    this.prepareCurrentSimpleFacets();
    this.prepareCurrentHistograms();
    this.prepareCurrentHierarchicFacets();
    this.prepareCurrentSorting();
  }

  setSearch(searchedText: string, searchedField: string) {
    searchedText = searchedText.trim();
    if (searchedText.length === 0) {
      searchedField = "";
    }
    this.searchedText = searchedText;
    this.searchedField = searchedField;
  }

  addFacet(key: string, value: string) {
    this.ensureFacetField(key).addValue(value);
  }

  protected ensureFacetField(key: string) {
    let f = this.findFacet(key) as SelectedFacet;
    if (!f) {
      f = new SelectedFacet(key);
      this.facets.push(f);
    }
    return f;
  }

  setFacetOperator(key: string, op: string) {
    this.ensureFacetField(key).setOperator(op);
  }

  addHistogram(key: string, from, to) {
    this.ensureHistogramField(key).addRange(from, to);
  }

  protected ensureHistogramField(key: string) {
    let h = this.findFacet(key) as SelectedHistogram;
    if (!h) {
      h = new SelectedHistogram(key);
      this.facets.push(h);
    }
    return h;
  }

  setHistogramOperator(key: string, op: string) {
    this.ensureHistogramField(key).setOperator(op);
  }

  addHierarchicFacet(key: string, [fst, snd]: string[], label: string) {
    this.ensureHierarchicField(key).addValue(fst, snd, label);
  }

  protected ensureHierarchicField(key: string) {
    let h = this.findFacet(key) as SelectedHierarchicFacet;
    if (!h) {
      h = new SelectedHierarchicFacet(key);
      this.facets.push(h);
    }
    return h;
  }

  setHierarchicOperator(key: string, op: string) {
    this.ensureHierarchicField(key).setOperator(op);
  }

  protected findFacet(field: string): UrlFacetBuilder {
    for (const h of this.facets) {
      if (h.key === field) {
        return h;
      }
    }
    return undefined;
  }

  /**
   * Builds object to call this.router.navigate().
   * URL encoding is done by URLSearchParamsUrlSerializer.
   * @returns {{}}
   */
  toUrlParams(): {} {
    const params = {};
    if (this.searchedText) {
      params[UrlParamNames.SEARCHED_TEXT.toString()] = this.searchedText;
      params[UrlParamNames.SEARCHED_FIELD.toString()] = this.searchedField;
    }
    if (this.sortField) {
      params[UrlParamNames.SORT_FIELD.toString()] = this.sortField;
      params[UrlParamNames.SORT_DIR.toString()] = this.sortDir;
    }
    for (const facetBuilder of this.facets) {
      const fv = facetBuilder.toUrlString();
      if (fv.length > 0) {
        let currentFacets = params[UrlParamNames.FACETS.toString()];
        if (!currentFacets) {
          currentFacets = [];
          params[UrlParamNames.FACETS.toString()] = currentFacets;
        }
        currentFacets.push(facetBuilder.key + "~" + fv);
      }
      if (facetBuilder.operator) {
        let currentOps = params[UrlParamNames.OPS.toString()];
        if (!currentOps) {
          currentOps = [];
          params[UrlParamNames.OPS.toString()] = currentOps;
        }
        currentOps.push(facetBuilder.key + "~" + facetBuilder.operator);
      }
    }

    if (Object.keys(params).length > 0) {
      return params;
    }
    return undefined;
  }

  setSorting(sortField: string, sortDir: string) {
    this.sortField = sortField;
    this.sortDir = sortDir;
  }

  public prepareCurrentSimpleFacets() {
    let activeFacets;
    this.searchStore.select(fromSearch.getFacetValues).pipe(take(1)).subscribe((v) => activeFacets = v);
    const keys = Object.keys(activeFacets);
    for (const key of keys) {
      for (const value of activeFacets[key].values) {
        this.addFacet(key, value.value.id);
      }
      if (environment.facetFields[key].operator !== activeFacets[key].operator) {
        this.setFacetOperator(key, activeFacets[key].operator);
      }
    }
  }

  public prepareCurrentHierarchicFacets() {
    let activeHierarchicFacets;
    this.searchStore.select(fromSearch.getHierarchyValues).pipe(take(1)).subscribe((v) => activeHierarchicFacets = v);
    const keys = Object.keys(activeHierarchicFacets);
    for (const key of keys) {
      for (const value of activeHierarchicFacets[key].values) {
        const deepUrlValue = this.backendSearchService.getDeepUrlHierarchicValue(value.value);
        this.addHierarchicFacet(key, deepUrlValue, value.label);
      }
      if (environment.facetFields[key].operator !== activeHierarchicFacets[key].operator) {
        this.setHierarchicOperator(key, activeHierarchicFacets[key].operator);
      }
    }
  }

  public prepareCurrentHistograms() {
    let activeHistograms;
    this.searchStore.select(fromSearch.getHistogramValues).pipe(take(1)).subscribe((v) => activeHistograms = v);
    const keys = Object.keys(activeHistograms);
    for (const key of keys) {
      for (const value of activeHistograms[key].values) {
        const [from, to] = this.backendSearchService.getHistogramRange(value.value);
        this.addHistogram(key, from, to);
      }
      if (environment.facetFields[key].operator !== activeHistograms[key].operator) {
        this.setHistogramOperator(key, activeHistograms[key].operator);
      }
    }
  }

  public prepareCurrentSearch() {
    let searched;
    this.searchStore.pipe(select(fromSearch.getSearchValues)).pipe(take(1)).subscribe((v) => searched = v);
    if (searched) {
      const searchedKeys = Object.keys(searched);
      if (searchedKeys.length > 0) {
        const searchKey = searchedKeys[0];
        this.setSearch(searched[searchKey].value, searchKey);
      }
    }
  }

  public prepareCurrentSorting() {
    let sortField, sortDir;
    this.searchStore.pipe(select(fromSearch.getResultSortField)).pipe(take(1)).subscribe((v) => sortField = v);
    this.searchStore.pipe(select(fromSearch.getResultSortOrder)).pipe(take(1)).subscribe((v) => sortDir = v);
    const defaultSorting = environment.sortFields ? environment.sortFields[0] : undefined;
    if (defaultSorting !== undefined && (sortField !== defaultSorting.field || sortDir !== defaultSorting.order)) {
      for (const key in environment.facetFields) {
        if (environment.facetFields[key].field === sortField) {
          this.setSorting(key, sortDir);
          break;
        }
      }
    } else {
      // don't add default sorting to url
      this.setSorting(undefined, undefined);
    }
  }

}

@Injectable({
  providedIn: 'root'
})
export class UrlParamsService {

  constructor(protected searchStore: Store<fromSearch.State>, protected backendSearchService: BackendSearchService) {
    // NOP
  }

  parseOperator(opsParam: string, proc: FacetValueProcessor): SearchActions[] {
    let newActions = [];

    const separatorIndex = opsParam.indexOf('~');
    if (separatorIndex >= 0) {
      const facetName = opsParam.substring(0, separatorIndex);
      const operatorValueStr = opsParam.substring(separatorIndex + 1);
      const field: FacetFieldModel = environment.facetFields[facetName];
      if (field) {
        newActions = newActions.concat(proc.processFacetOperator(facetName, operatorValueStr));
      } else {
        console.error("User provided unknown field '" + facetName + "'. Known are: " + Object.keys(environment.facetFields));
      }
    } else {
      console.error("Ignoring bad facet operator request param: '" + opsParam + "'");
    }

    return newActions;
  }

  /**
   * Parse one "facet" request param. Repeat "facet" for multiple facet filters.
   *
   * Simple facet examples:
   * - facets=title~Schweizerische+Nationalbank
   * - facets=title~Schweizerische+Nationalbank!Lohn
   *
   * Histogram facet examples:
   * - facets=Quelldatum~(2000-12-23!2004-12-30)
   * - facets=Quelldatum~(!2004-12-30)
   * - facets=Quelldatum~(2000-12-23!)
   * - facets=Quelldatum~(2000%2F2019!2000%2F2019)
   * - facets=Quelldatum~(2000%2F2019!2000%2F2019)(2010%2F2019!2010%2F2019)
   * - facets=pages~(1!100)
   * - facets=pages~(1!100)(200!500)
   * Note: Spaces aren't allowed.
   * Note2: Non day values aren't validated (eg. 2000/2019).
   *
   * Hierarchic facet example:
   * - facets=descr_sach_hierarchy_filter~(7.1.28.!stw_http:%2F%2Fzbw.eu%2Fstw%2Fdescriptor%2F27630-4!Sicherheit+%2528allg.%2529)
   * Hint: Note the trailing "." in "7.1.28."!
   *
   * @param {string} facetsParam the value of a 'facet' request parameter
   * @param {FacetValueProcessor} proc
   * @returns {boolean}
   */
  parseFacet(facetsParam: string, proc: FacetValueProcessor): SearchActions[] {
    let newActions = [];
    const separatorIndex = facetsParam.indexOf('~');
    if (separatorIndex >= 0) {
      const facetName = facetsParam.substring(0, separatorIndex);
      const facetValueStr = facetsParam.substring(separatorIndex + 1);
      const field: FacetFieldModel = environment.facetFields[facetName];
      if (field) {
        switch (field.facetType) {
          case FacetFieldType.SIMPLE:
          case FacetFieldType.SUBCATEGORY:
            newActions = newActions.concat(this.parseSimpleFacet(facetName, facetValueStr, proc));
            break;
          case FacetFieldType.HISTOGRAM:
            newActions = newActions.concat(this.parseHistogramFacet(facetName, facetValueStr, field as HistogramFieldModel, proc));
            break;
          case FacetFieldType.HIERARCHIC:
            newActions = newActions.concat(this.parseHierarchicFacet(facetName, facetValueStr, field, proc));
            break;
          default:
            console.error("Can't handle facet type: " + field.facetType);
        }
      } else {
        console.error("User provided unknown field '" + facetName + "'. Known are: " + Object.keys(environment.facetFields));
      }
    } else {
      console.error("Ignoring bad facet value request param: '" + facetsParam + "'");
    }

    return newActions;
  }

  protected parseSimpleFacet(facetName: string,
                             facetValueStr: string,
                             proc: FacetValueProcessor): SearchActions[] {
    const facetValueList = facetValueStr.split("!");
    let newActions = [];
    let facetValues;
    this.searchStore.select(fromSearch.getFacetValues).pipe(take(1)).subscribe((v) => facetValues = v);
    const facetValuesByValue = (id) => {
      if (facetValues && facetValues[facetName]) {
        for (const av of facetValues[facetName].values) {
          if (id === av.value.id) {
            return av;
          }
        }
      }
      return undefined;
    };

    for (const facetValue of facetValueList) {
      const av = facetValuesByValue(facetValue);
      let label;
      let value;
      // keep original facet value which contains perhaps more information than the one parsed from the url
      if (av) {
        label = av.label;
        value = av.value;
      } else {
        label = "" + facetValue;
        value = {id: facetValue};
      }
      newActions = newActions.concat(proc.processSimpleFacet(facetName, label, value));
    }
    return newActions;
  }

  /**
   * Parses histogram facet values.
   * @param {string} facetName
   * @param {string} facetValue
   * @param {HistogramFieldModel} field
   * @param {FacetValueProcessor} proc
   * @returns {boolean}
   */
  protected parseHistogramFacet(facetName: string,
                                facetValue: string,
                                field: HistogramFieldModel,
                                proc: FacetValueProcessor): SearchActions[] {
    const facetPattern = /\(([^!]*)!([^)]*)\)/;
    let range;
    let newActions = [];
    let activeHistograms;
    this.searchStore.select(fromSearch.getHistogramValues).pipe(take(1)).subscribe((v) => activeHistograms = v);
    const activeHistogramByValue = (gte, lte) => {
      if (activeHistograms && activeHistograms[facetName]) {
        for (const av of activeHistograms[facetName].values) {
          if (gte === av.value.gte && lte === av.value.lte) {
            return av;
          }
        }
      }
      return undefined;
    };

    while (range = facetValue.match(facetPattern)) {
      const wholeMatchedString = range[0];
      const start = range[1].trim();
      const end = range[2].trim();
      let validated;
      switch (field.data_type) {
        case HistogramFieldType.INT:
          validated = this.intRangeValidator(start, end);
          break;
        case HistogramFieldType.DATE:
          validated = this.dateRangeValidator(start, end);
          break;
        case HistogramFieldType.GEO:
          validated = this.geoRangeValidator(start, end);
          break;
      }
      if (validated && validated.ok) {
        let label;
        let value;
        const av = activeHistogramByValue(validated.ok.from, validated.ok.to);
        // keep original facet value which contains perhaps more information than the one parsed from the url
        if (av) {
          label = av.label;
          value = av.value;
          // TODO empty label should be fixed in backend (in /popup response)
          if (!label) {
            const labelValue = AbstractHistogramFacetComponent.buildLabelAndValue(validated.ok.from, validated.ok.to);
            label = labelValue.label;
          }
        } else {
          const labelValue = AbstractHistogramFacetComponent.buildLabelAndValue(validated.ok.from, validated.ok.to);
          label = labelValue.label;
          value = labelValue.value;
        }
        newActions = newActions.concat(proc.processHistogramFacet(facetName, label, value));
      } else {
        console.error("Ignoring bad facet range request param " + facetName + ": '" + wholeMatchedString + "'");
      }
      facetValue = facetValue.substring(wholeMatchedString.length);
    }

    if (facetValue.length > 0) {
      console.error("Ignoring remaining facet range request param " + facetName + ": '" + facetValue + "'");
    }

    return newActions;
  }

  /**
   * Parses hierarchic facet request param.
   * @param {string} facetName
   * @param {string} facetValue
   * @param {FacetFieldModel} field
   * @param {FacetValueProcessor} proc
   * @returns {boolean}
   */
  protected parseHierarchicFacet(facetName: string,
                                 facetValue: string,
                                 field: FacetFieldModel,
                                 proc: FacetValueProcessor): SearchActions[] {
    let newActions = [];
    let range;
    let activeHierarchics;
    this.searchStore.select(fromSearch.getHierarchyValues).pipe(take(1)).subscribe((v) => activeHierarchics = v);
    const activeHierarchicByValue = (id, pos) => {
      if (activeHierarchics && activeHierarchics[facetName]) {
        for (const av of activeHierarchics[facetName].values) {
          if (id === av.value.id && pos === av.value.pos) {
            return av;
          }
        }
      }
      return undefined;
    };

    while (range = facetValue.match(/\(([^!]*)!([^!]*)!([^)]*)\)/)) {
      const wholeMatchedValue = range[0];
      const firstValue = range[1].trim();
      const secondValue = range[2].trim();
      let label = range[3].trim().replace("%28", "(").replace("%29", ")");

      let value;
      const av = activeHierarchicByValue(secondValue, firstValue);
      // keep original facet value which contains perhaps more information than the one parsed from the url
      if (av) {
        label = av.label;
        value = av.value;
      } else {
        value = this.backendSearchService.buildHierarchicValue([firstValue, secondValue]);
      }
      const na = proc.processHierarchicFacet(facetName, label, value);
      if (na.length === 0) {
        console.error("Ignoring bad hierarchic facet request param " + facetName + ": '" + wholeMatchedValue + "'");
      }
      newActions = newActions.concat(na);
      facetValue = facetValue.substring(wholeMatchedValue.length);
    }
    return newActions;
  }

  /**
   * Handles "searchText" and "searchField" request params.
   * Examples:
   * - searchText=Bern
   * - searchText=Bern&searchField=search0
   * Note: If only one search field is configured in the current environment, then this field is picked-up automatically.
   * Otherwise a user has to provide the specific search field.
   *
   * @param {string} searchText
   * @param {string} searchField
   * @param defaultSearchFields
   * @param {SearchValueProcessor} proc
   * @returns {boolean}
   */
  parseSearch(searchText: string,
              searchField: string,
              defaultSearchFields,
              proc: SearchValueProcessor): SearchActions[] {
    let newActions = [];

    if (searchText) {
      searchText = searchText.trim();
    }

    if (searchText.length > 0) {
      if (searchField) {
        searchField = searchField.trim();
      }
      let searchFieldIsOk = false;
      if (searchField) {
        // check that provided search field is supported ...
        if (defaultSearchFields[searchField]) {
          searchFieldIsOk = true;
        }
      } else {
        // use default search field if only one is configured
        // otherwise user has to provide searchField request param
        if (Object.keys(defaultSearchFields).length === 1) {
          searchField = "search0";
          searchFieldIsOk = true;
        }
      }
      if (searchFieldIsOk) {
        newActions = newActions.concat(proc.processSimpleSearchValue(searchText, searchField));
      } else {
        console.error("Ignoring bad simple search request params " + searchField + ": '" + searchText + "'");
      }
    }

    return newActions
  }

  parseSorting(sortKey: string,
               sortDir: string,
               defaultSortFields: SortFieldsModel[],
               proc: SearchValueProcessor): SearchActions[] {
    sortKey = sortKey.trim();
    sortDir = sortDir.trim();
    let newActions = [];

    const dir: SortOrder = stringToEnumValue<typeof SortOrder, SortOrder>(SortOrder, sortDir);
    if (dir) {
      const facetField = environment.facetFields[sortKey].field;
      for (const sortField of defaultSortFields) {
        if (sortField.field === facetField && sortField.order === dir) {
          newActions = newActions.concat(proc.processSorting(sortField.field, dir));
        }
      }
    }
    return newActions;
  }

  intRangeValidator(fromStr: string, toStr: string): ValidatorResult {
    try {
      if ((!fromStr || fromStr.length === 0) && (!toStr || toStr.length === 0)) {
        return {error: "empty"};
      }
      let fromNumber, toNumber;
      const obj: EmittedValues = {};
      if (fromStr && fromStr.length > 0) {
        fromNumber = parseInt(fromStr, 10);
        obj.from = "" + fromNumber;
      }
      if (toStr && toStr.length > 0) {
        toNumber = parseInt(toStr, 10);
        obj.to = "" + toNumber;
      }
      if (fromNumber !== undefined && toNumber !== undefined) {
        if (fromNumber > toNumber) {
          return {error: "fromGreaterThanTo"};
        }
      }
      return {ok: obj};
    } catch (ex) {
      console.error("Caught exception during processing", ex);
      return {error: "processingFailure"};
    }
  }

  /**
   * This validation method is used by the "deep url" code too, where other formats
   * should work than allowed by the date input fields.
   * @param {string} fromStr
   * @param {string} toStr
   * @returns {ValidatorResult}
   */
  dateRangeValidator(fromStr: string, toStr: string): ValidatorResult {
    try {
      const emptyFrom = (!fromStr || fromStr.length === 0);
      const emptyTo = (!toStr || toStr.length === 0);
      const obj: EmittedValues = {};

      if (emptyFrom && emptyTo) {
        return {error: "empty"};
      }

      const dayPattern = /\d\d\d-\d\d-\d\d/;
      if ((emptyFrom || fromStr.match(dayPattern)) && (emptyTo || toStr.match(dayPattern))) {
        let fromDate: Date, toDate: Date;
        if (!emptyFrom) {
          fromDate = new Date(fromStr);
          obj.from = fromDate.toISOString().substring(0, 10);
        }
        if (!emptyTo) {
          toDate = new Date(toStr);
          obj.to = toDate.toISOString().substring(0, 10);
        }
        if (fromDate !== undefined && toDate !== undefined) {
          if (fromDate.getTime() > toDate.getTime()) {
            return {error: "fromGreaterThanTo"};
          }
        }
      } else {
        // pass through non day formats ...
        obj.from = fromStr;
        obj.to = toStr;
      }
      return {ok: obj};
    } catch (ex) {
      console.error("Caught exception during processing", ex);
      return {error: "processingFailure"};
    }
  }

  geoRangeValidator(fromStr: string, toStr: string): ValidatorResult {
    try {
      if (fromStr.length === 0 && toStr.length === 0) {
        return {error: "empty"};
      }
      fromStr = fromStr.trim();
      toStr = toStr.trim();
      const obj: EmittedValues = {};
      if (fromStr.length > 0) {
        obj.from = fromStr;
      }
      if (toStr.length > 0) {
        obj.to = toStr;
      }
      return {ok: obj};
    } catch (ex) {
      console.error("Caught exception during processing", ex);
      return {error: "processingFailure"};
    }
  }
}
