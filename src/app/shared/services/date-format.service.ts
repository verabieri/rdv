import {Injectable} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";

import {format} from 'date-fns';
import {enGB, de, it, fr} from 'date-fns/locale';
import {AbstractHistogramFacetComponent} from "../../search/components/abstract-histogram-facet.component";

// see UniversalViewerComponent language mapping!
const localeMap = {
  de: de,
  en: enGB,
  it: it,
  fr: fr
};

@Injectable({
  providedIn: 'root'
})
export class DateFormatService {

  constructor(protected translate: TranslateService) {
  }

  public formatLabel(v) {
    const dateResolution = v.value ? v.value.type : undefined;
    if (dateResolution) {
      let label;
      const date = new Date(v.value.gte);
      const year = date.getFullYear();
      const century = Math.floor(year / 100);
      const options = {locale: localeMap[this.translate.currentLang]};
      const dayPattern = this.translate.instant('date.label.day-pattern');
      switch (dateResolution) {
        case "century":
          label = this.translate.instant('date.label.century',
            {value: year, startYear: year, endYear: new Date(v.value.lte).getFullYear()});
          break;
        case "decade":
          label = this.translate.instant('date.label.decade',
            {value: year % 100, year: year, century: century + 1});
          break;
        case "year":
          label = this.translate.instant('date.label.year', {value: year});
          break;
        case "month":
          const month = format(date, 'LLL', options);
          label = this.translate.instant('date.label.month', {value: month, year: year});
          break;
        case "day":
          const day = format(date, dayPattern, options);
          label = this.translate.instant('date.label.day', {value: day});
          break;
        case "day-range": // used if type is missing in date histogram
          const startDay = v.value.gte ? format(new Date(v.value.gte), dayPattern, options) : undefined;
          const endDay = v.value.lte ? format(new Date(v.value.lte), dayPattern, options) : undefined;
          const rangeLabel = AbstractHistogramFacetComponent.buildLabelAndValue(startDay, endDay).label;
          label = this.translate.instant('date.label.day', {value: rangeLabel});
          break;
      }
      if (label) {
        v = {...v};
        v.label = label;
      }
    }
    return v;
  }

}
