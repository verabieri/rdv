/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';

import {Injectable} from '@angular/core';

import {Observable} from "rxjs";
import {QueryFormat} from "app/shared/models/query-format";
import {Basket} from 'app/search/models/basket.model';
import {HttpClient} from '@angular/common/http';
import {BackendSearchService} from './backend-search.service';
import {SettingsModel, SortOrder} from "../models/settings.model";
import {TranslateService} from "@ngx-translate/core";

@Injectable()
export class ElasticBackendSearchService extends BackendSearchService {

  //URL auf PHP-Proxy
  private proxyUrl: string;
  private moreProxyUrl: string;
  private detailProxyUrl: string;
  private navDetailProxyUrl: string;
  private iiifProxyUrl: string;
  //URL auf PHP-Proxy-Endpunkt für Suche innerhalb von Facetten
  private inFacetSearchProxyUrl: string;
  //URL auf PHP-Proxy-Endpunkt für Suche mit zusätzlichem Popup Info.
  private popupQueryProxyUrl: string;
  //Felder, die bei normaler Suche fuer die Treffertabelle geholt werden
  private tableFields: string[] = [];

  //Felder, die bei der Detailsuche geholt werden
  private detailFields: string[] = [];

  //Http Service injekten
  constructor(private http: HttpClient, protected translate: TranslateService) {

    super();

    //Main-Config laden
    const mainConfig: SettingsModel = environment;

    //proxyUrls setzen
    this.proxyUrl = mainConfig.proxyUrl;
    this.moreProxyUrl = mainConfig.moreProxyUrl;
    this.inFacetSearchProxyUrl = mainConfig.inFacetSearchProxyUrl;
    this.detailProxyUrl = mainConfig.detailProxyUrl;
    this.navDetailProxyUrl = mainConfig.navDetailProxyUrl;
    this.iiifProxyUrl = mainConfig.iiifProxyUrl;
    this.popupQueryProxyUrl = mainConfig.popupQueryProxyUrl;

    //Felder sammeln, die bei normaler Suche geholt werden sollen
    for (const field of mainConfig.tableFields) {

      //ID Feld kommt sowieso (allerdings als _id und nicht als id in _source), daher id nicht in Liste der geholten Felder einfuegen
      if (field.field !== "id") {

        //Feld-Namen in tempArray sammeln
        this.tableFields.push(field.field);
      }
    }

    //Felder sammeln, die bei Detailsuche geholt werden sollen
    for (const key of Object.keys(mainConfig.extraInfos)) {

      //Feld-Namen in tempArray sammeln
      this.detailFields.push(mainConfig.extraInfos[key].field);
    }
  }

  //Daten in Elasticsearch suchen
  getBackendDataComplex(queryFormat: QueryFormat): Observable<any> {

    //Anfrageobjekt erzeugen
    const complexQueryFormat = {};

    this.handleQueryParams(complexQueryFormat, queryFormat);

    //Ueber Anfrage-Formate gehen
    this.handleSearchFields(queryFormat, complexQueryFormat);

    this.handleTextSearch(complexQueryFormat);

    this.handleFilters(queryFormat, complexQueryFormat);

    this.handleSimpleFacets(queryFormat, complexQueryFormat);

    this.setRangeFacets(queryFormat, complexQueryFormat);

    this.setHistogramFacets(queryFormat, complexQueryFormat);

    this.setHierarchyFacets(queryFormat, complexQueryFormat);

    //Liste der zu holenden Tabellenfelder
    this.handleReturnFields(complexQueryFormat);

    this.handleLanguage(queryFormat, complexQueryFormat);

    if (queryFormat.return_selection) {
      complexQueryFormat["return_selection"] = queryFormat.return_selection;
    }

    if (queryFormat.persist) {
      complexQueryFormat["persist"] = queryFormat.persist;
    }

    // console.error("ES QUERY", JSON.stringify(complexQueryFormat, null, 2));

    //HTTP-Anfrage an Elasticsearch
    let url;
    if (queryFormat.popupId) {
      url = this.popupQueryProxyUrl;
      complexQueryFormat["popup_query"] = queryFormat.popupId;
    } else {
      url = (queryFormat.resultInIIIF === true) ? this.iiifProxyUrl : this.proxyUrl;
    }

    return this.http
      .post(url,
        JSON.stringify(complexQueryFormat),
        {headers: {'Content-Type': 'application/json', 'X-Request-Type': 'search'}})
      .pipe(map((response: any) => response));
  }

  getMoreBackendDataComplex(queryFormat: QueryFormat): Observable<any> {
    //Anfrageobjekt erzeugen
    const complexQueryFormat = {};

    this.handleQueryParams(complexQueryFormat, queryFormat);

    //Ueber Anfrage-Formate gehen
    this.handleSearchFields(queryFormat, complexQueryFormat);

    this.handleTextSearch(complexQueryFormat);

    this.handleFilters(queryFormat, complexQueryFormat);

    this.handleSimpleFacets(queryFormat, complexQueryFormat);

    this.setRangeFacets(queryFormat, complexQueryFormat);

    this.setHistogramFacets(queryFormat, complexQueryFormat);

    this.setHierarchyFacets(queryFormat, complexQueryFormat);

    this.handleSearchAfterParams(queryFormat, complexQueryFormat);

    this.handleLanguage(queryFormat, complexQueryFormat);

    //Liste der zu holenden Tabellenfelder
    this.handleReturnFields(complexQueryFormat);

    // console.log("ES MORE QUERY", JSON.stringify(complexQueryFormat, null, 2));

    //HTTP-Anfrage an Elasticsearch
    return this.http

    //POST Anfrage
      .post(this.moreProxyUrl,
        JSON.stringify(complexQueryFormat),
        {headers: {'Content-Type': 'application/json', 'X-Request-Type': 'search'}})
      .pipe(
        //Antwort als JSON weiterreichen
        map((response: any) => response));
  }

  private handleReturnFields(complexQueryFormat) {
    let esProxyQueryParams: any = complexQueryFormat['query_params'];
    if (!esProxyQueryParams) {
      complexQueryFormat['query_params'] = esProxyQueryParams = {};
    }
    esProxyQueryParams.source = this.tableFields;
  }

  private handleLanguage(queryFormat: QueryFormat | Basket, complexQueryFormat) {
    complexQueryFormat['lang'] = queryFormat.lang || "de";
  }

  //Suchparameter fuer Paging und Sortierung direkt aus Queryformat uebernehmen
  private handleQueryParams(complexQueryFormat, queryFormat: QueryFormat) {
    const queryParams = queryFormat.queryParams;
    if (queryParams) {
      let esProxyQueryParams: any = complexQueryFormat['query_params'];
      if (!esProxyQueryParams) {
        complexQueryFormat['query_params'] = esProxyQueryParams = {};
      }
      esProxyQueryParams.size = queryParams.rows;
      esProxyQueryParams.sort = [{
        field: queryParams.sortField,
        order: queryParams.sortDir.toString()
      }];
      esProxyQueryParams.start = queryParams.start;
      complexQueryFormat['query_params'] = esProxyQueryParams;
    }
  }

  private handleSearchAfterParams(queryFormat: QueryFormat, complexQueryFormat) {
    complexQueryFormat['search_after_values'] = queryFormat.search_after;
    complexQueryFormat['search_after_order'] = queryFormat.queryParams.sortDir;
  }

  private handleSearchFields(queryFormat: QueryFormat, complexQueryFormat) {
    if (queryFormat.searchFields) {
      for (const key of Object.keys(queryFormat.searchFields)) {

        //Schnellzugriff auf dieses Suchfeld
        const searchfield_data = queryFormat.searchFields[key];

        //Wenn Wert gesetzt ist (z.B. bei der Titel-Suche)
        if (searchfield_data.value.trim()) {

          //Wenn query-Bereich noch nicht angelegt wurde
          if (complexQueryFormat["query"] === undefined) {

            //Geruest einer Boolquery aufbauen (Titel:Freiheit AND Person:Martin He*), einzelne Suchfelder werden per AND verknuepft
            complexQueryFormat["query"] = {
              "bool": {
                "must": []
              }
            };
          }

          //Inputstring rechts trunkieren und zu Array umwandeln: "martin helfer welt" => ["martin", "helfer", "welt*"]
          const input_value_array = searchfield_data.value.trim().split(" ");

          //Wildcard query_string-Suche aufbauen, dazu Inputstring-Arraywerte per AND verknupefen
          const queryString = {
            "query_string": {

              //["martin", "helfer", "welt*"] -> "martin AND helfer AND welt*"
              "query": input_value_array.join(" AND ")
            }
          };

          //Wenn nicht in Freitext gesucht wird
          if (searchfield_data.field !== "all_text") {

            //Passendes Suchfeld setzen
            queryString["query_string"]["searchFields"] = [searchfield_data.field]
          }

          //Bool-Queries sammeln
          complexQueryFormat["query"]["bool"]["must"].push(queryString);
        }
      }
    }
  }

  private handleTextSearch(complexQueryFormat) {
    //Wenn es keine komplexe Suche gibt (also keine Werte in den Suchfeldern stehen)
    if (complexQueryFormat["query"] === undefined) {

      //match_all = *:* Anfrage-Parameter setzen fuer proxy
      complexQueryFormat["match_all"] = true;
    }
  }

  private ensureEsProxyFacetObject(complexQueryFormat): any {
    let esProxyFacetParams: any = complexQueryFormat['facets'];
    if (!esProxyFacetParams) {
      complexQueryFormat['facets'] = esProxyFacetParams = {};
    }
    return esProxyFacetParams;
  }

  private handleSimpleFacets(queryFormat: QueryFormat, complexQueryFormat) {
    const esProxyFacetParams: any = this.ensureEsProxyFacetObject(complexQueryFormat);
    if (queryFormat.facetFields) {
      for (const key of Object.keys(queryFormat.facetFields)) {

        //Schnellzugriff auf Infos dieser Facette
        // noinspection JSUnusedLocalSymbols
        const facetData = {...queryFormat.facetFields[key]};
        // map env fact type to elastic search query facet type
        // subcat maps to subcat
        if (facetData.facet_type === 'simple') {
          facetData.facet_type = 'basic';
        }
        facetData.size = (environment.facetFields[key].size !== undefined) ? environment.facetFields[key].size : 10;

        //Infos zu Felder, benutzer Verknuepfung und ausgewaehlten Werten
        esProxyFacetParams[key] = facetData;
      }
    }
  }

  private handleFilters(queryFormat: QueryFormat, complexQueryFormat) {
    if (queryFormat.filterFields) {
      for (const key of Object.keys(queryFormat.filterFields)) {

        //Schnellzugriff auf Infos dieses Filters
        const filterData = queryFormat.filterFields[key];

        //Ueber ausgewaehlte Filterwerte dieses Filters gehen (["Artikel", "Buch"] bei Filter "Typ")
        filterData.values.forEach((item, index) => {

          //Zu Beginn gibt es noch keinen Filterbereich -> anlegen
          if (complexQueryFormat["filter"] === undefined) {

            //Filterbereich mit must-clause-Array (AND-Verknuepfung) anlegen. Must query kombiniert die einzelnen Filter per AND.
            //Die einzelnen Werte eines Filters sind dann per OR verknuepft: type:Artikel AND ort:(Berlin OR Bremen)
            complexQueryFormat["filter"] = {
              "bool": {
                "must": []
              }
            };
          }

          //Bei 1. Filterwert ("Aritkel") dieses Filters ("Dokumentyp")
          if (index === 0) {

            //should-clause (=OR-Verknuepfung) anlegen fuer diesen Filter (Artikel OR Buch).
            //Wenn Filterung per AND (Region = Aushangsort AND Druckort), muss hier must statt should gesetzt werden
            complexQueryFormat["filter"]["bool"]["must"].push({"bool": {"should": []}});
          }

          //passenden Index im must-Array finden = letzter Eintrag
          const mustIndex = complexQueryFormat["filter"]["bool"]["must"].length - 1;

          //Filterwert in should-clause dieses Filteres einfuegen als term-query (exakter Treffer auf keyword-type)
          complexQueryFormat["filter"]["bool"]["must"][mustIndex]["bool"]["should"].push({"term": {[filterData.field]: item}});
        });
      }
    }
  }

  getInFacetSearchBackendData(queryFormat: QueryFormat): Observable<any> {
    //Anfrageobjekt erzeugen
    const complexQueryFormat = {};

    this.handleQueryParams(complexQueryFormat, queryFormat);

    this.handleSimpleFacets(queryFormat, complexQueryFormat);

    this.setHistogramFacets(queryFormat, complexQueryFormat);

    this.setHierarchyFacets(queryFormat, complexQueryFormat);

    //Ueber Anfrage-Formate gehen
    this.handleSearchFields(queryFormat, complexQueryFormat);

    this.handleTextSearch(complexQueryFormat);

    this.handleFilters(queryFormat, complexQueryFormat);

    this.setInFacetSearchValues(queryFormat, complexQueryFormat);

    //Liste der zu holenden Tabellenfelder
    this.handleReturnFields(complexQueryFormat);

    this.handleLanguage(queryFormat, complexQueryFormat);

    // console.log("ES IN FACET QUERY", JSON.stringify(complexQueryFormat, null, 2)); // TODO

    //HTTP-Anfrage an Elasticsearch
    return this.http

      .post(this.inFacetSearchProxyUrl, JSON.stringify(complexQueryFormat), {
        headers: {
          'Content-Type': 'application/json',
          'X-Request-Type': 'search'
        }
      })
      .pipe(map((response: any) => response));
  }

  /**
   * @Deprecated use histogramFields instead.
   *
   * @param {QueryFormat} queryFormat
   * @param complexQueryFormat
   */
  private setRangeFacets(queryFormat: QueryFormat, complexQueryFormat) {
    if (queryFormat.rangeFields) {
      for (const key of Object.keys(queryFormat.rangeFields)) {

        //Zu Beginn gibt es noch keinen Rangebereich -> anlegen
        if (complexQueryFormat["range"] === undefined) {

          //Rangebereich = aggs anlegen und Infos sammeln (eigentliche Anfrage in php erstellt)
          complexQueryFormat["range"] = {};
        }

        //Rangeinfos weiterreichen
        complexQueryFormat["range"][key] = queryFormat.rangeFields[key];
      }
    }
  }

  /**
   * Add/replace histogram facets.
   *
   * @param {QueryFormat} queryFormat
   * @param complexQueryFormat
   */
  private setHistogramFacets(queryFormat: QueryFormat, complexQueryFormat) {
    if (queryFormat.histogramFields) {
      const esProxyFacetParams: any = this.ensureEsProxyFacetObject(complexQueryFormat);

      for (const key of Object.keys(queryFormat.histogramFields)) {

        const newFacetValue = {...queryFormat.histogramFields[key]};
        newFacetValue.show_aggs = newFacetValue.showAggs;
        delete newFacetValue.showAggs;
        newFacetValue.size = (environment.facetFields[key].size !== undefined) ? environment.facetFields[key].size : 10;
        newFacetValue.facet_type = newFacetValue.data_type;
        delete newFacetValue.data_type;
        // user input uses from/to while backend facet values use gte/lte
        newFacetValue.values = newFacetValue.values.map((v) => {
          const [from, to] = this.getHistogramRange(v);
          return {gte: from, lte: to};
        });
        esProxyFacetParams[key] = newFacetValue;
      }
    }
  }

  /**
   * Add/replace histogram facets.
   *
   * @param {QueryFormat} queryFormat
   * @param complexQueryFormat
   */
  private setHierarchyFacets(queryFormat: QueryFormat, complexQueryFormat) {
    if (queryFormat.hierarchicFields) {
      const esProxyFacetParams: any = this.ensureEsProxyFacetObject(complexQueryFormat);

      for (const key of Object.keys(queryFormat.hierarchicFields)) {

        //Rangeinfos weiterreichen
        const facetValue = {...queryFormat.hierarchicFields[key]};
        facetValue.facet_type = "hierarchy";
        facetValue.size = (environment.facetFields[key].size !== undefined) ? environment.facetFields[key].size : 10;
        esProxyFacetParams[key] = facetValue;
      }
    }
  }

  protected setInFacetSearchValues(queryFormat: QueryFormat, complexQueryFormat) {
    complexQueryFormat['facet_search_query'] = queryFormat.inFacetSearchPrefix;
    complexQueryFormat['facet_search_field'] = queryFormat.inFacetSearchField;
  }

//Detail-Daten aus Elasticsearch fuer zusaetzliche Zeile holen (abstract,...)
  getBackendDetailData(id: any, fullRecord: boolean = false): Observable<any> {

    //Objekt fuer Detailsuche
    const detailQueryFormat = {};

    //ID-Suche (nach 1 ID)
    detailQueryFormat["ids"] = [id];

    //Wenn nur ein Teil der Felder geholt weren soll
    if (!fullRecord) {

      //Liste der zu holenenden Felder
      detailQueryFormat["sourceFields"] = this.detailFields;
    }

    const url = this.detailProxyUrl + encodeURIComponent(id) + "/" + this.translate.currentLang;

    // console.log("getBackendDetailData", url, JSON.stringify(detailQueryFormat));

    //HTTP-Anfrage an Elasticsearch
    return this.http
      .get(url, {
        headers: {
          'Content-Type': 'application/json',
          'X-Request-Type': 'detailed'
        }
      });
  }

  navBackendDetailData(id: any, queryFormat: QueryFormat, next: boolean): Observable<any> {

    //Anfrageobjekt erzeugen
    const complexQueryFormat = {};

    this.handleQueryParams(complexQueryFormat, queryFormat);

    //Ueber Anfrage-Formate gehen
    this.handleSearchFields(queryFormat, complexQueryFormat);

    this.handleTextSearch(complexQueryFormat);

    this.handleFilters(queryFormat, complexQueryFormat);

    this.handleSimpleFacets(queryFormat, complexQueryFormat);

    this.setRangeFacets(queryFormat, complexQueryFormat);

    this.setHistogramFacets(queryFormat, complexQueryFormat);

    this.setHierarchyFacets(queryFormat, complexQueryFormat);

    complexQueryFormat['search_after_values'] = id;
    const inverseSearchDir = ((queryFormat.queryParams.sortDir === SortOrder.ASC.toString())
      ? SortOrder.DESC.toString() : SortOrder.ASC.toString());
    complexQueryFormat['search_after_order'] = next ? queryFormat.queryParams.sortDir : inverseSearchDir;

    //Liste der zu holenden Tabellenfelder
    this.handleReturnFields(complexQueryFormat);

    this.handleLanguage(queryFormat, complexQueryFormat);

    // console.log("ES NAV DETAIL QUERY", JSON.stringify(complexQueryFormat, null, 2));

    //HTTP-Anfrage an Elasticsearch
    return this.http

    //POST Anfrage
      .post(this.navDetailProxyUrl,
        JSON.stringify(complexQueryFormat),
        {headers: {'Content-Type': 'application/json', 'X-Request-Type': 'search'}})
      .pipe(
        //Antwort als JSON weiterreichen
        map((response: any) => response));
  }

  //Merklisten-Daten in Elasticsearch suchen (ueber IDs)
  getBackendDataBasket(basket: Basket): Observable<any> {

    //Anfrage-Objekt erstellen
    const basketQueryFormat = {};

    //Parameter fuer Paging und Sortierung direkt vom Merklisten-Objekt uebernehmen
    basketQueryFormat["queryParams"] = basket.queryParams;

    //zu suchende IDs on Merklisten-Objekt uebernehmen
    basketQueryFormat["ids"] = basket.ids;

    //Liste der Tabellenfelder mitgeben
    basketQueryFormat['sourceFields'] = this.tableFields;
    //console.log(JSON.stringify(basketQueryFormat));

    this.handleLanguage(basket, basketQueryFormat);

    //HTTP-Anfrage an Elasticsearch
    return this.http

    //POST Anfrage mit URL, Liste der IDs und Liste der Felder
      .post(this.proxyUrl, JSON.stringify(basketQueryFormat), {
        headers: {
          'Content-Type': 'application/json',
          'X-Request-Type': 'basket'
        }
      }).pipe(
        //von JSON-Antwort nur die Dokument weiterreichen
        map((response: any) => response));
  }


  /**
   * @Override
   * @param {array} deepUrlFormat first element is the position, second the id
   * @returns {{id: string, pos: string}}
   */
  buildHierarchicValue(deepUrlFormat): any {
    return {id: deepUrlFormat[1], pos: deepUrlFormat[0]};
  }

  /**
   * @Override
   * @param hierarchicValue
   * @returns {[any, any]}
   */
  getDeepUrlHierarchicValue(hierarchicValue): [any, any] {
    return [hierarchicValue.pos, hierarchicValue.id];
  }

  /**
   * @Override
   * @returns {[number]}
   */
  getHistogramRange(selectedHistogramValue): [any, any] {
    return [
      selectedHistogramValue.gte,
      selectedHistogramValue.lte
    ];
  }
}
