/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Location, registerLocaleData} from '@angular/common';
import {Route, RouterModule, Routes, UrlMatcher, UrlMatchResult, UrlSegment, UrlSegmentGroup, UrlSerializer} from "@angular/router";

import {AppComponent} from './core/components/app.component';
import {SearchComponent} from './search/components/search.component';
import {SearchModule} from './search/search.module';
import {backendSearchServiceProvider} from './shared/services/backend-search.service.provider';
import {StoreModule} from '@ngrx/store';
import {metaReducers, reducers} from './reducers';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {CoreModule} from './core/core.module';
import {EffectsModule} from '@ngrx/effects';
import {RemoteFilterConfigsEffects} from './core/effects/remote-filter-configs.effects';
import {NewSearchComponent} from "./search/components/new-search.component";
import {
  LocalizeParser, LocalizeRouterModule, LocalizeRouterSettings, ManualParserLoader, LocalizeRouterService
} from '@gilsdav/ngx-translate-router';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';
import localeEn from '@angular/common/locales/en';
import localeEnExtra from '@angular/common/locales/extra/en';
import localeIt from '@angular/common/locales/it';
import localeItExtra from '@angular/common/locales/extra/it';
import {PreviousRouteServiceProvider} from "./shared/services/previous-route-service-provider.service";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {LOCALIZED_ROUTER_PREFIX, PopupLandingComponent} from "./search/components/popup-landing.component";
import {URLSearchParamsUrlSerializer} from "./shared/services/URLSearchParamsUrlSerializer";

registerLocaleData(localeFr, 'fr-FR', localeFrExtra);
registerLocaleData(localeDe, 'de-DE', localeDeExtra);
registerLocaleData(localeEn, 'en-GB', localeEnExtra);
registerLocaleData(localeIt, 'it-IT', localeItExtra);


// needs custom url matcher, to catch not localized urls e.g. "/popup?..."
export function popupUrlMatcher(segments: UrlSegment[], group: UrlSegmentGroup, route: Route): UrlMatchResult {
  let res: UrlMatchResult = null;
  if (segments && segments.length === 1 && segments[0].path === 'popup') {
    res = {consumed: segments};
  }
  return res;
}

export const routes: Routes = [
  {path: '', pathMatch: 'full', component: NewSearchComponent},
  {path: 'old-search', component: SearchComponent},
  {path: 'popup', component: PopupLandingComponent},
  {matcher: popupUrlMatcher, component: PopupLandingComponent, data: {skipRouteLocalization: true}},
  {path: '**', component: NewSearchComponent}
];

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

/**
 * Root module
 */
@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    RouterModule.forRoot(routes),
    SearchModule,
    CoreModule,
    LocalizeRouterModule.forRoot(routes, {
      parser: {
        provide: LocalizeParser,
        useFactory: (translate, location, settings) =>
          new ManualParserLoader(translate, location, settings, ['de', 'en', 'fr', 'it'], LOCALIZED_ROUTER_PREFIX),
        deps: [TranslateService, Location, LocalizeRouterSettings]
      }
    }),
    StoreModule.forRoot(reducers, {metaReducers}),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([RemoteFilterConfigsEffects]),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    backendSearchServiceProvider,
    PreviousRouteServiceProvider,
    LocalizeRouterService,
    {
      provide: UrlSerializer,
      useClass: URLSearchParamsUrlSerializer
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
