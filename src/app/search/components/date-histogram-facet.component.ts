/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component} from "@angular/core";
import {Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import {MinMaxDateInputComponent} from "./min-max-date-input.component";
import {AbstractHistogramFacetComponent, ValidatorResult} from "./abstract-histogram-facet.component";
import {DateFormatService} from "../../shared/services/date-format.service";

/**
 * Displays date range facet.
 */
@Component({
  selector: 'app-date-histogram-facet',
  template: `
    <div class="facet facet_range">
      <div class="facet_range__min-max">
        <app-min-max-date-input #inputFields
                                [key]="key"
                                [min]="'1000-01-01'"
                                (submitted)="selectRange(key, $event.from, $event.to)"></app-min-max-date-input>
      </div>
      <ng-template #facetValueTemplate let-key="key" let-value="value" let-selected="selected">
        <button
          (click)="facetAction(key, value, selected)"
          type="button"
          class="facet__entry link-black {{selected ? 'facet__entry--selected' : ''}}">
          <span class="facet__name">{{formatLabel(value).label}}</span>
          <span class="facet__count"> ({{value.count}})</span>
        </button>
      </ng-template>
      <ul class="facet__list facet__list--selected" *ngIf="(facetFieldByKey$ | async)[key].values?.length > 0">
        <li *ngFor="let value of (facetFieldByKey$ | async)[key].values">
          <button
            (click)="facetAction(key, value, true)"
            type="button"
            class="facet__entry link-black facet__entry--selected">
            <span class="facet__name">{{formatLabel(value).label}}</span>
          </button>
        </li>
      </ul>
      <ul class="facet__list facet__list--available">
        <ng-container *ngIf="(facetFieldCountByKey$ | async)">
          <app-expandable
            [itemTemplate]="facetValueTemplate"
            [itemsPerExpansion]="facetFieldsConfig[key].expandAmount"
            [items]="buildTemplateItems(facetFieldByKey$ | async, facetFieldCountByKey$ | async)"></app-expandable>
        </ng-container>
      </ul>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DateHistogramFacetComponent extends AbstractHistogramFacetComponent<MinMaxDateInputComponent> {

  constructor(searchStore: Store<fromSearch.State>, protected dateFormatService: DateFormatService) {
    super(searchStore);
  }

  protected formatLabel(v) {
    if (!v.value.type) {
      v = {...v, value: {...v.value, type: 'day-range'}};
    }
    return this.dateFormatService.formatLabel(v);
  }
}
