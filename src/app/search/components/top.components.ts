import {ChangeDetectionStrategy, Component} from "@angular/core";
import {environment} from '../../../environments/environment';
import {SettingsModel} from "../../shared/models/settings.model";
import * as fromSearch from "../reducers";
import {select, Store} from "@ngrx/store";
import {Observable} from "rxjs/index";
import {LocalizeRouterService} from '@gilsdav/ngx-translate-router';
import {TranslateService} from "@ngx-translate/core";
import {ActivatedRoute, NavigationExtras, Router} from "@angular/router";

@Component({
  selector: 'app-top',
  template: `
    <ng-container *ngIf="env?.headerSettings?.disable !== true">
      <div class="top">
        <ng-container *ngIf="env?.headerSettings?.disableLanguageBar !== true">
          <div class="top__first-line-wrapper">
            <div class="top__first-line d-flex justify-content-center justify-content-sm-end">
              <div class="top__first-line-content">
                <a class="btn nav-about" *ngIf="!!env.externalAboutUrl"
                   href="{{env.externalAboutUrl}}"
                   target="_blank">{{'header.about' | translate}}</a>
                <a class="btn nav-about" *ngIf="!!env.externalHelpUrl"
                   href="{{env.externalHelpUrl}}"
                   target="_blank">{{'header.help' | translate}}</a>
                <button class="btn btn__lang" type="button" (click)="changeLanguage('de')"
                        [disabled]="langButtonDisabled('de')">de
                </button>
                <button class="btn btn__lang" type="button" (click)="changeLanguage('en')"
                        [disabled]="langButtonDisabled('en')">en
                </button>
                <button *ngIf="env?.headerSettings?.showFrenchLanguageSwitch === true"
                        class="btn btn__lang" type="button" (click)="changeLanguage('fr')"
                        [disabled]="langButtonDisabled('fr')">fr
                </button>
                <button *ngIf="env?.headerSettings?.showItalianLanguageSwitch === true"
                        class="btn btn__lang" type="button" (click)="changeLanguage('it')"
                        [disabled]="langButtonDisabled('it')">it
                </button>
              </div>
            </div>
          </div>
        </ng-container>
        <div class="top__owner-wrapper">
          <div class="top__owner">
            <div class="top__owner-content">
              <div>
                <img class="top__owner-content-logo" src="../../../assets/img/uni-basel-logo.svg">
              </div>
              <div class="top__owner-content-title">
                <a class="btn" routerLink="/">{{env?.headerSettings?.logoSubTitle || env.name}}</a>
              </div>
            </div>
            <ng-container *ngIf="env?.headerSettings?.departmentLogoUrl">
              <div class="top__owner-department-logo d-md-inline-block">
                <a href="{{env?.headerSettings?.departmentUrl || '/'}}" target="_blank">
                  <img src="{{env.headerSettings.departmentLogoUrl}}"/>
                </a>
              </div>
            </ng-container>
            <div style="clear: both;"></div>
          </div>
          <ng-container *ngIf="env?.headerSettings?.showPortalName === true">
            <div class="top__subtitle-wrapper">
              <div class="top__subtitle">{{env.name}}</div>
            </div>
          </ng-container>
          <!-- Hides currently not implemented features
          <div class="top__owner-content-status">
            <div class="top__owner-content-status-stored-results ml-3" *ngIf="(numberOfBaskets$ | async)">
              <span class="label">{{'header.basket' | translate}}</span> {{(numberOfBaskets$ | async)}}
            </div>
            <div class="top__owner-content-status-stored-searches ml-3" *ngIf="(numberOfSavedQueries$ | async)">
              <span class="label">{{'header.saved_queries' | translate}}</span> {{(numberOfSavedQueries$ | async)}}
            </div>
          </div> -->
        </div>
      </div>
    </ng-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopComponent {
  numberOfSavedQueries$: Observable<number>;
  numberOfBaskets$: Observable<number>;

  constructor(
    private _searchStore: Store<fromSearch.State>,
    protected localize: LocalizeRouterService,
    protected translate: TranslateService,
    protected route: ActivatedRoute
  ) {
    this.numberOfSavedQueries$ = _searchStore.pipe(select(fromSearch.getSavedQueriesCount));
    this.numberOfBaskets$ = _searchStore.pipe(select(fromSearch.getBasketCount));
  }

  get env(): SettingsModel {
    return environment;
  }

  changeLanguage(newLanguage: string) {
    const extras: NavigationExtras = {
      relativeTo: this.route,
      queryParamsHandling: 'preserve'
    };
    // don't use useNavigateMethod=false, because this will merge query
    // params with "," instead of repeating them
    this.localize.changeLanguage(newLanguage, extras, true);
  }

  isCurrentLang(langCode: string): boolean {
    return this.translate.currentLang === langCode;
  }

  langButtonDisabled(langCode: string): string {
    if (this.isCurrentLang(langCode)) {
      return "disabled";
    }
    return "";
  }
}
