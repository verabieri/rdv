/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component} from "@angular/core";
import {Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import {AbstractHistogramFacetComponent} from "./abstract-histogram-facet.component";
import {MinMaxIntInputComponent} from "./min-max-int-input.component";

/**
 * Displays int histogram facet hierarchically.
 */
@Component({
  selector: 'app-int-hierarchic-facet',
  template: `
    <ng-template #facetValueTemplate let-key="key" let-value="value" let-selected="selected">
      <button
        (click)="facetAction(key, value, selected)"
        type="button"
        class="facet__entry link-black {{selected ? 'facet__entry--selected' : ''}}">
        <span class="facet__name">{{value.label}}</span>
        <span class="facet__count"> ({{value.count}})</span>
      </button>
    </ng-template>
    <div class="facet hierarchic-facet">
      <div class="facet_range__min-max">
        <app-min-max-int-input #inputFields
                               [key]="key"
                               [min]="1"
                               (submitted)="selectRange(key, $event.from, $event.to)"></app-min-max-int-input>
      </div>
      <ul class="hierarchic-facet__path" *ngIf="(facetFieldByKey$ | async)[key].values.length > 0">
        <li *ngFor="let value of (facetFieldByKey$ | async)[key].values; last as isLast">
          <button *ngIf="!isLast"
                  (click)="replaceFacetValue(key, value)"
                  type="button"
                  class="btn link-black hierarchic-facet__path-element">
            <span class="facet__name">{{'hierarchic-facet.reset_to' | translate}} {{value.label}}</span>
          </button>
          <button *ngIf="isLast"
                  (click)="facetAction(key, value, true)"
                  type="button"
                  class="facet__entry link-black hierarchic-facet__path-element">
            <span class="facet__name">{{'hierarchic-facet.reset_to' | translate}} {{value.label}}</span>
          </button>
        </li>
      </ul>
      <ul
        *ngIf="facetFieldsConfig[key]"
        class="facet__list facet__list--available"
        [class.first-level]="(facetFieldByKey$ | async)[key].values.length === 0"
      >
        <app-expandable
          [itemTemplate]="facetValueTemplate"
          [itemsPerExpansion]="facetFieldsConfig[key].expandAmount"
          [items]="buildTemplateItems(facetFieldByKey$ | async, facetFieldCountByKey$ | async)"></app-expandable>
      </ul>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IntHierarchicFacetComponent extends AbstractHistogramFacetComponent<MinMaxIntInputComponent> {

  constructor(searchStore: Store<fromSearch.State>) {
    super(searchStore);
  }
}
