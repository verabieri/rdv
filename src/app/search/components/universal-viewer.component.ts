/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {Observable, Subscription} from "rxjs/index";
import {LocalizeRouterService} from "@gilsdav/ngx-translate-router";
import {environment} from "../../../environments/environment";
import {TranslateService} from "@ngx-translate/core";
import {select, Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import {take} from "rxjs/operators";

/**
 * Embed IIIF universal viewer.
 */
@Component({
  selector: 'app-universal-viewer',
  template: `
    <div class="detailed__universalviewer-container">
      <iframe #uv class="detailed__universalviewer" [src]="frameSrc | safe" allowfullscreen></iframe>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UniversalViewerComponent implements OnDestroy, AfterViewInit, OnChanges {
  @ViewChild("uv", {static: false}) iframeRef: ElementRef<HTMLIFrameElement>;
  @Input() manifestUrl: string;

  // see DateFormatService language mapping!
  // TODO create de-DE/it-IT uv-*extension.config.json? map de/it to 4 char locale
  uvLanguageMap = {"en": "en-GB", "fr": "fr-FR"};

  frameSrc: string;

  languageChangeSubscription: Subscription;

  searchFields$: Observable<any>;
  searchFieldByKey$: Observable<any>;

  /**
   * @ignore
   */
  constructor(protected searchStore: Store<fromSearch.State>,
              protected translate: TranslateService) {
    this.frameSrc = "";
    this.searchFields$ = searchStore.pipe(select(fromSearch.getSearchValues));
    this.searchFieldByKey$ = searchStore.pipe(select(fromSearch.getSearchValuesByKey));
  }

  protected setUniversalViewerUrl() {
    if (this.manifestUrl) {
      let locales = "en-GB";
      if (this.uvLanguageMap[this.translate.currentLang]) {
        locales = this.uvLanguageMap[this.translate.currentLang];
      }
      // embed.js loads 2.2.0 version only, but not 3.X.Y!
      // doesn't work too! so a specific local version is used
      // needs cache buster param "c" in order to force reload (changes after "#" aren't sufficient)
      let baseUvUrl = environment.universalViewerUrl;
      if (!baseUvUrl) {
        baseUvUrl = "assets/uv/uv.html";
      }
      const paramSeparator = (baseUvUrl.indexOf("?") < 0) ? "?" : "&";
      let uvUrl = baseUvUrl + paramSeparator + "c=" + (new Date()).getTime() + "#" +
        "?manifest=" + encodeURIComponent(this.manifestUrl) +
        "&locales=" + locales + "&c=0&m=0&s=0&cv=0&r=0";
      if (environment.uvViewerConfig) {
        uvUrl += "&config=" + encodeURIComponent(environment.uvViewerConfig);
      }

      let firstSearchField;
      this.searchFields$.pipe(take(1)).subscribe((fields) => firstSearchField = Object.keys(fields)[0]);
      let searchedValue;
      this.searchFieldByKey$.pipe(take(1)).subscribe((field) => searchedValue = field(firstSearchField).value);
      if (searchedValue) {
        uvUrl += "&h=" + encodeURIComponent(searchedValue);
      }

      // avoid iframe's url in browser history
      if (this.iframeRef) {
        const iframe = this.iframeRef.nativeElement;
        const iframeParent = iframe.parentElement;
        iframe.remove();
        iframe.src = uvUrl;
        iframeParent.append(iframe);
      } else {
        this.frameSrc = uvUrl;
      }
    }
  }

  protected languageChanged() {
    this.setUniversalViewerUrl();
  }

  ngOnDestroy(): void {
    if (this.languageChangeSubscription) {
      this.languageChangeSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    this.languageChangeSubscription = this.translate.onLangChange.subscribe(() => this.languageChanged());
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.setUniversalViewerUrl();
  }

}
