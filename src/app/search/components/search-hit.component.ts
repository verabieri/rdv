/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Store} from "@ngrx/store";

import * as fromSearch from '../reducers';

import {empty} from "../../shared/utils";
import {SettingsModel} from "../../shared/models/settings.model";
import {environment} from "../../../environments/environment";

/**
 * Displays a row in the result or  list
 */
@Component({
  selector: 'app-search-hit',
  template: `
    <div class="search-hit row no-gutters">
      <div class="search-hit__basket text-left text-sm-center mb-2 mb-sm-0 col-12 col-sm-1">
        <app-basket-icon *ngIf="env?.showExportList?.basket === true" [basketElement]="doc.id"></app-basket-icon>
      </div>
      <div class="search-hit__info col row no-gutters">
        <div *ngIf="env.showImagePreview !== false"
             class="col-3 search-hit__info-preview">
          <a class="search-hit__info-preview-container"
             [title]="'search-results.to_doc' | translate"
             [routerLink]="['/detail' | localize, doc.id]"
             [state]="{cursor: doc.search_after_values}">
            <img [src]="buildPreviewUrls(doc.preview_image, doc.object_type)[0]"
                 [appBrokenImage]="buildPreviewUrls(doc.preview_image, doc.object_type)"
            />
          </a>
        </div>
        <div class="search-hit__info-text" [class.col-9]="env.showImagePreview !== false">
          <ng-container *ngIf="empty(doc.object_type)">
            <div class="search-hit__type">
              {{indexInResultList}}.
            </div>
          </ng-container>
          <ng-container *ngIf="!empty(doc.object_type)">
            <div class="search-hit__type">
              {{indexInResultList}}.
              <app-search-hit-value [fieldValue]="doc.object_type"></app-search-hit-value>
            </div>
          </ng-container>
          <ng-container *ngIf="!empty(doc.title)">
            <a class="search-hit__title-link link-black"
               [title]="'search-results.to_doc' | translate"
               [routerLink]="['/detail' | localize, doc.id]"
               [state]="{cursor: doc.search_after_values}">
              <h3 class="search-hit__title">
                <app-search-hit-value [fieldValue]="doc.title"></app-search-hit-value>
              </h3>
            </a>
          </ng-container>
          <div class="d-none d-sm-block d-md-block d-lg-block d-xl-block">
            <ng-container *ngIf="!empty(doc.line1)">
              <h4 class="search-hit__line_1">
                <app-search-hit-value [fieldValue]="doc.line1"></app-search-hit-value>
              </h4>
            </ng-container>
            <a class="search-hit__title-link link-black text-decoration-none"
               [title]="'search-results.to_doc' | translate"
               [routerLink]="['/detail' | localize, doc.id]"
               [state]="{cursor: doc.search_after_values}">
              <div class="field" [innerHTML]="doc.fulltext_snippet"></div>
            </a>
          </div>
        </div>
        <div class="d-block d-sm-none d-md-none d-lg-none d-xl-none">
          <ng-container *ngIf="!empty(doc.line1)">
            <h4 class="search-hit__line_1">
              <app-search-hit-value [fieldValue]="doc.line1"></app-search-hit-value>
            </h4>
          </ng-container>
          <div class="field" [innerHTML]="doc.fulltext_snippet"></div>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchHitComponent {
  @Input() doc: any;
  @Input() indexInResultList: number;

  constructor(private _searchStore: Store<fromSearch.State>) {
  }

  objectTypeImageUrl(objectType): string {
    return '/assets/img/icon-' + objectType + '.svg';
  }

  empty(obj: any): boolean {
    return empty(obj);
  }

  get env(): SettingsModel {
    return environment;
  }

  buildPreviewUrls(previewImage, objectType): string[] {
    const list = [];
    if (previewImage) {
      list.push(previewImage + '/full/120,/0/default.jpg');
    }
    list.push(this.objectTypeImageUrl(objectType));
    list.push('/assets/img/icon-default.svg');
    return list;
  }
}
