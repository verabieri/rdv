/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, SimpleChanges, ViewChild} from "@angular/core";
import * as fromFormActions from "../actions/form.actions";
import {Observable} from "rxjs/index";
import {select, Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import * as fromQueryActions from "../actions/query.actions";
import {environment} from "../../../environments/environment";
import {FacetFieldsModel} from "../../shared/models/settings.model";
import {InFacetSearchComponent} from "./in-facet-search.component";
import {SimpleFacetComponent} from "./simple-facet.component";

/**
 * Displays simple subcat facet.
 */
@Component({
  selector: 'app-subcat-facet',
  template: `
    <ng-template #facetValueTemplate
                 let-key="key"
                 let-value="value"
                 let-selected="selected"
                 let-subvalueSelection="subvalueSelection">
      <button *ngIf="!selected"
              (click)="facetAction(key, value, selected)"
              type="button"
              class="facet__entry link-black {{selected ? 'facet__entry--selected' : ''}}">
        <span class="facet__name">{{value.label}}</span>
        <span class="facet__count"> ({{value.count}})</span>
      </button>
      <div *ngIf="selected && hasAvailableSubvalues(value.sub_values, subvalueSelection)"
           class="facet__entry link-black facet__entry--selected facet__entry--selected-with-subvalues">
        <span class="facet__name">{{value.label}}</span>
      </div>
      <ng-container *ngIf="hasAvailableSubvalues(value.sub_values, subvalueSelection)">
        <app-collapsible
          title="{{'subcategory.more' | translate}}"
          key="{{key + '/' + value.label + '/subvalues'}}">
          <ul class="facet__list facet__list--available">
            <li *ngFor="let sv of onlyAvailableSubvalues(value.sub_values, subvalueSelection)">
              <button
                (click)="facetAction(key, sv, subvalueSelection[sv.value.id] === true)"
                type="button"
                class="facet__entry link-black {{(subvalueSelection[sv.value.id] === true) ? 'facet__entry--selected' : ''}}">
                <span class="facet__name">{{sv.label}}</span>
                <span class="facet__count"> ({{sv.count}})</span>
              </button>
            </li>
          </ul>
        </app-collapsible>
      </ng-container>
    </ng-template>
    <div class="facet subcat-facet">
      <app-operator-selector
        [key]="key"
        (changed)="doSearch()"
        [facetValueSelector]="fromSearch.getFacetValuesByKey"></app-operator-selector>
      <ul class="facet__list" *ngIf="(facetFieldByKey$ | async)(key).values?.length > 0">
        <li *ngFor="let value of (facetFieldByKey$ | async)(key).values">
          <button
            (click)="facetAction(key, value, true)"
            type="button"
            class="facet__entry link-black facet__entry--selected">
            <span class="facet__name">{{value.label}}</span>
          </button>
        </li>
      </ul>
      <app-in-facet-search #inFacetSearch
                           *ngIf="facetFieldsConfig[key].searchWithin"
                           [key]="key" (changed)="updateFacets($event)"></app-in-facet-search>
      <ul class="facet__list">
        <li *ngIf="loading" class="rotating"></li>
        <app-expandable
          [itemTemplate]="facetValueTemplate"
          [itemsPerExpansion]="facetFieldsConfig[key].expandAmount"
          [items]="buildTemplateItems(facetFieldByKey$ | async, facetFieldCountByKey$ | async)"></app-expandable>
      </ul>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubcatFacetComponent extends SimpleFacetComponent {

  hasAvailableSubvalues(subvalues, subvalueSelection) {
    return this.onlyAvailableSubvalues(subvalues, subvalueSelection).length > 0;
  }

  onlyAvailableSubvalues(subvalues, subvalueSelection) {
    if (!subvalues) {
      return [];
    }
    return subvalues.filter((sv) => subvalueSelection[sv.value.id] !== true);
  }

  buildTemplateItems(facetFieldByKey, facetFieldCountByKey) {
    this.loading = false;
    const facet = facetFieldByKey(this.key);
    let values = facetFieldCountByKey(facet.field);
    if (!values) {
      values = [];
    }
    return values
      .filter((v) => {
        // maybe sub value is selected
        const ids = (v.sub_values || []).reduce((a, cv) => a.concat(cv.value.id), [v.value.id]);
        return !this.isSelected(facet, ids);
      })
      .map((v) => {
          const subvalueSelection = {};
          if (v.sub_values) {
            v.sub_values.map((v2) => subvalueSelection[v2.value.id] = this.isSelected(facet, [v2.value.id]));
          }

          const ri = {
            key: this.key,
            value: v,
            selected: this.isSelected(facet, [v.value.id]),
            subvalueSelection: subvalueSelection
          };
          return ri;
        }
      );
  }

  protected isSelected(facet, ids): boolean {
    const selected = facet.values.filter((f) => ids.indexOf(f.value.id) >= 0);
    // if all ids are selected, don't show facet in selection area
    return selected.length === ids.length;
  }

}
