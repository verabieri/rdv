/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnDestroy, Output, ViewChild} from "@angular/core";
import {select, Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import {Subscription} from "rxjs/internal/Subscription";

@Component({
  selector: 'app-in-facet-search',
  template: `
    <div class="in-facet-search">
      <input class="in-facet-search__prefix"
             type="text"
             #searchPrefix
             (keyup)="updateInFacetSearchValue(searchPrefix.value)"
             placeholder="{{'in-facet-search.prefix_placeholder' | translate}}">
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InFacetSearchComponent implements OnDestroy {
  @Input() key: string;
  @ViewChild("searchPrefix", {static: false}) inputFieldRef: ElementRef<HTMLInputElement>;
  @Output() changed = new EventEmitter<{ prefix, immediate }>(false);

  protected lastSearch: string;
  private querySubscription: Subscription;

  constructor(protected searchStore: Store<fromSearch.State>) {
    this.lastSearch = "";
    this.querySubscription = this.searchStore
      .pipe(select(fromSearch.getCombinedInFacetSearchQueries))
      .subscribe((v) => this.queryChanged(v));
  }

  updateInFacetSearchValue(prefix: string, immediate?: boolean) {
    prefix = prefix.trim();
    if (prefix !== this.lastSearch) {
      this.changed.emit({prefix, immediate});
      this.lastSearch = prefix;
    }
  }

  protected queryChanged(v) {
    if (this.inputFieldRef) {
      const processedNormalSearch = !v.inFacetSearchField;
      // query changed and a normal search (non in-facet search) was done ... reset the in-facet filter
      if (processedNormalSearch) {
        this.inputFieldRef.nativeElement.value = "";
        this.lastSearch = "";
      }
    }
  }

  clearSearchField() {
    this.lastSearch = "x"; // any string different to "" in order to force emit
    this.inputFieldRef.nativeElement.value = "";
    this.updateInFacetSearchValue("", true);
  }

  ngOnDestroy(): void {
    this.querySubscription.unsubscribe();
  }
}
