/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';

import {environment} from '../../../environments/environment';
import * as fromRoot from '../../reducers';
import * as fromFormActions from '../actions/form.actions';
import * as fromSearch from '../reducers';

/**
 * Displays search filters
 */
@Component({
  selector: 'app-filters',
  template: `
      <div *ngFor="let filter of filterFields | objectKeys" class="filterBlock">
          <div class="h6">{{filterFields[filter].label | translate}}</div>
          <label class="btn btn-sm btn-outline-primary mr-1"
                 *ngFor="let option of filterFields[filter].options"
                 (click)="toggleChecked(filter, option.value)"
                 [class.active]="(filterFieldsByKey$ | async)(filter).values.includes(option.value)">
        <span class="fa" [class.fa-check-circle]="(filterFieldsByKey$ | async)(filter).values.includes(option.value)"
              [class.fa-circle-thin]="!(filterFieldsByKey$ | async)(filter).values.includes(option.value)">
        </span>
              <span> {{option.label | translate}}</span>
          </label>
      </div>
  `,
  styles: [`
      label {
          margin-bottom: 0;
      }

      .filterBlock + .filterBlock {
          margin-top: 10px;
      }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FiltersComponent {
  filterFields: any;
  filterFieldsByKey$: Observable<any>;

  constructor(private _formBuilder: FormBuilder,
              private _rootStore: Store<fromRoot.State>,
              private _searchStore: Store<fromSearch.State>) {
    this.filterFields = environment.filterFields;
    this.filterFieldsByKey$ = _searchStore.pipe(select(fromSearch.getFilterValuesByKey))
  }

  toggleChecked(filter: string, value: string) {
    this._searchStore.dispatch(new fromFormActions.ToggleFilterValue({filter: filter, value: value}));
  }
}
