/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, Input, OnDestroy} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import * as fromQueryActions from "../actions/query.actions";
import {Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import {ToastrService} from "ngx-toastr";
import {Subscription} from "rxjs/internal/Subscription";
import {Observable} from "rxjs/internal/Observable";
import {searchParamObserver} from "../../shared/models/observed-util";

/**
 * Embed IIIF universal viewer manifest link suitable for Mirador.
 */
@Component({
  selector: 'app-uv-manifest-link',
  template: `
    <div class="uv-manifest-link d-flex align-items-center">
      <button
        class="search-results__mode ml-0"
        [disabled]="persistUrl || showUrl"
        [title]="'uv-manifest-link.show-url.title' | translate"
        (click)="persistUrl = true;"
      >
        <img src="../assets/img/logo-iiif-34x30.png" [alt]="'app-uv-manifest-link.drag-drop.image-alt' | translate" height="20">
      </button>
      <a *ngIf="showUrl && !persistUrl" [href]="buildMiradorUrl()" class="ml-2">
        {{buildMiradorUrl()}}
      </a>
      <div *ngIf="persistUrl" class="uv-manifest-link__question">
        {{'uv-manifest-link.show-url.question' | translate}}
        <button class="search-results__mode" (click)="sendPersistRequest()">{{'yes' | translate}}</button>
        <button class="search-results__mode" (click)="doNotStoreUrl()">{{'no' | translate}}</button>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UvManifestLinkComponent implements OnDestroy {
  @Input() manifestUrl: string;
  showUrl?: boolean;
  persistUrl?: boolean;

  protected watchSubscription: Subscription;
  protected watch$: Observable<any>;

  constructor(protected searchStore: Store<fromSearch.State>,
              protected toastr: ToastrService,
              protected translate: TranslateService) {
    this.watch$ = searchParamObserver(searchStore);
    this.watchSubscription = this.watch$.subscribe((vals) => this.queryParamsChanged(vals));
  }

  buildMiradorUrl(): string {
    // Mirador < 3
    // return '/default_target?manifest=' + encodeURIComponent(this.manifestUrl);
    // Mirador >= 3: no Drag & Drop
    return this.manifestUrl;
  }

  sendPersistRequest() {
    this.searchStore.dispatch(new fromQueryActions.SimpleSearch({persist: true}));
    this.persistUrl = false;
    this.showUrl = true;
  }

  doNotStoreUrl() {
    this.persistUrl = false;
    const title = this.translate.instant("uv-manifest-link.show-url.no-title");
    const msg = this.translate.instant("uv-manifest-link.show-url.no-message");
    this.toastr.warning(msg, title);
  }

  ngOnDestroy(): void {
    if (this.watchSubscription) {
      this.watchSubscription.unsubscribe();
    }
  }

  protected queryParamsChanged(vals: any) {
    // search query changed, hide manifest URL
    if (this.showUrl) {
      const title = this.translate.instant("uv-manifest-link.show-url.hide-title");
      const msg = this.translate.instant("uv-manifest-link.show-url.hide-message");
      this.toastr.info(msg, title);
    }
    this.showUrl = false;
  }
}
