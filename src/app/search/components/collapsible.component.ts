/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from "@angular/core";

// to use this component in a template, a global state is needed!
const collapsed = {};

/**
 * Displays collapsible container.
 * Don't implement OnDestroy!
 */
@Component({
  selector: 'app-collapsible',
  template: `
    <div class="collapsible">
      <div [class.collapsible--with-delete]="showDelButton">
        <button type="button"
                [class]="buttonsClass()"
                (click)="toggleCollapsible()">
          {{title}}
        </button>
        <button *ngIf="showDelButton"
                [title]="'collapsible.delete-all-label' | translate"
                class="button--delete"
                (click)="shallDelete()"></button>
      </div>
      <div class="collapsible__container pl-4 pl-sm-0 pl-lg-4" [class.collapsed]="collapsedState()" *ngIf="!collapsedState()">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollapsibleComponent implements OnInit {
  @Input() buttonClass: string;
  @Input() title: string;
  @Input() key: string;
  @Input() showDelButton?: boolean;
  @Output() changed = new EventEmitter<boolean>(false);
  @Output() delete = new EventEmitter<boolean>(false);

  toggleCollapsible() {
    collapsed[this.key] = !collapsed[this.key];
    this.changed.emit(collapsed[this.key]);
  }

  shallDelete() {
    this.delete.emit(true);
  }

  buttonsClass(): string {
    let css = this.buttonClass || "";
    css += " collapsible__title";
    if (this.collapsedState()) {
      css += " collapsed";
    }
    return css;
  }

  collapsedState(): boolean {
    return collapsed[this.key];
  }

  ngOnInit(): void {
    if (collapsed[this.key] === undefined) {
      collapsed[this.key] = true;
    }
  }

}
