/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BackendSearchService} from '../../shared/services/backend-search.service';
import {Location} from "@angular/common";
import {empty} from "../../shared/utils";
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";
import * as fromQueryActions from "../actions/query.actions";
import * as fromSearch from "../reducers";
import * as fromDetailedResultActions from "../actions/detailed-result.actions";
import {select, Store} from "@ngrx/store";
import {Observable, Subscription} from "rxjs/index";
import {take} from "rxjs/operators";
import {LocalizeRouterService} from "@gilsdav/ngx-translate-router";
import {environment} from "../../../environments/environment";
import {TranslateService} from "@ngx-translate/core";
import {SettingsModel} from "../../shared/models/settings.model";

/**
 * Shows detailed information on a document
 */
@Component({
  selector: 'app-detailed',
  template: `
    <div>
      <app-top></app-top>
      <div class="detailed">
        <nav class="detailed-nav-toolbar d-flex">
          <div class="mr-auto">
            <a class="back-to-search link-black" (click)="backToSearch()">{{'detailed.search_result' | translate}}</a>
          </div>
          <div *ngIf="!!cursor" class="nav">
            <button type="button" class="previous-doc btn btn-link link-black" (click)="showPreviousDoc()"></button>
            <button type="button" class="next-doc btn btn-link link-black" (click)="showNextDoc()"></button>
          </div>
        </nav>
        <ng-container *ngIf="(detailedViewIds$ |async) && (meta$ | async)">
          <ng-container *ngIf="!empty((meta$ | async).object_type)">
            <div class="search-hit__type">
              <app-search-hit-value [fieldValue]="(meta$ | async).object_type"></app-search-hit-value>
            </div>
          </ng-container>
          <div class="row no-gutters">
            <h3 *ngIf="!empty((meta$ | async).title)" class="detailed__title col">
              <app-search-hit-value [fieldValue]="(meta$ | async).title"></app-search-hit-value>
            </h3>
          </div>
          <div>
            <app-basket-icon *ngIf="env?.showExportList?.basket === true" [basketElement]="(meta$ | async).id"></app-basket-icon>
          </div>

          <ul class="no-list-style detailed__fields">
            <li *ngFor="let field of (fields$ | async)">
              <app-detailed-field [label]="field.label" [value]="field.value" [link]="field.link"></app-detailed-field>
            </li>
          </ul>

        </ng-container>
        <app-universal-viewer [manifestUrl]="manifestUrl"></app-universal-viewer>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailedComponent implements OnInit, OnDestroy {
  /**
   * Identifier of document
   */
  id: any;

  /**
   * If called from a search result list, it is possible to navigate to the "left" or "right" neighbour doc.
   */
  cursor?: any;

  manifestUrl: string;

  /**
   * Object with document information
   */
  meta$ = new BehaviorSubject<any>(undefined);
  fields$ = new BehaviorSubject<any>(undefined);
  detailedView$: Observable<any>;
  detailedViewIds$: Observable<any>;

  detailedViewSubscription: Subscription;
  languageSubscription: Subscription;

  location: Location;
  translate: TranslateService;

  parentRoute: ActivatedRoute;

  /**
   * @ignore
   */
  constructor(private _backendSearchService: BackendSearchService,
              private _route: ActivatedRoute,
              protected router: Router,
              private _location: Location,
              private _searchStore: Store<fromSearch.State>,
              protected localize: LocalizeRouterService,
              protected activeRoute: ActivatedRoute,
              protected _translate: TranslateService) {
    this.location = _location;
    const state: any = _location.getState();
    this.cursor = state.cursor;
    this.detailedView$ = _searchStore.pipe(select(fromSearch.getAllDetailedResults));
    this.detailedViewIds$ = _searchStore.pipe(select(fromSearch.getDetailedResultsIds));
    this.translate = _translate;
    this.languageSubscription = this.translate.onLangChange.subscribe((l) => {
      this.showCurrentDoc();
    });
    this.detailedViewSubscription = this.detailedViewIds$.subscribe((res) => this.detailedDataSubscriber(res));
  }

  /**
   * Retrieves document data from backend
   */
  ngOnInit() {
    this._route.paramMap.pipe(take(1)).subscribe(params => {
      this.id = params.get('id');
      this.showCurrentDoc();
    });
    this.parentRoute = this.activeRoute.parent;
  }

  protected showCurrentDoc() {
    this._searchStore.dispatch(new fromDetailedResultActions.ClearDetailedResults());
    this._searchStore.dispatch(new fromQueryActions.MakeDetailedSearchRequest({id: this.id, fullRecord: false}));
  }

  empty(obj: any): boolean {
    return empty(obj);
  }

  showPreviousDoc() {
    this._searchStore.dispatch(new fromDetailedResultActions.ClearDetailedResults());
    this._searchStore.dispatch(new fromQueryActions.MakePreviousDetailedSearchRequest(this.cursor));
  }

  showNextDoc() {
    this._searchStore.dispatch(new fromDetailedResultActions.ClearDetailedResults());
    this._searchStore.dispatch(new fromQueryActions.MakeNextDetailedSearchRequest(this.cursor));
  }

  backToSearch() {
    // if detail view was first page
    if (!this.cursor) {
      this.router.navigateByUrl(this.localize.translateRoute("/") as string).then(() => {
          // normally, search view starts empty search, but not if previous url was the detail view
          // so start search here
          this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
        }
      );
    } else {
      // this.location.back();
      this.router.navigate(['.'], {relativeTo: this.parentRoute});
    }
  }

  detailedDataSubscriber(ids) {
    let detailedViews;
    this.detailedView$.pipe(take(1)).subscribe((docs) => detailedViews = docs);
    if (ids && ids.length > 0 && detailedViews) {
      const newId = ids[ids.length - 1];
      const res = detailedViews[newId];
      if (res && res.func_metadata) {
        if (res.func_metadata.search_after_values && res.func_metadata.search_after_values !== '[]') {
          if (res.func_metadata.search_after_values !== "[]") {
            this.cursor = res.func_metadata.search_after_values;
          }
          this.id = newId;
          const newUrl = this.localize.translateRoute('/detail/' + encodeURIComponent(newId));
          // don't push new url to browser's history, so that browser back returns to search view
          this.router.navigate([newUrl], {replaceUrl: true});
        }
        this.meta$.next(res.func_metadata);
        this.fields$.next(res.desc_metadata);
        if (res.func_metadata.iiif_manifest) {
          this.manifestUrl = res.func_metadata.iiif_manifest;
        }
      }
    }
  }

  ngOnDestroy(): void {
    if (this.detailedViewSubscription) {
      this.detailedViewSubscription.unsubscribe();
    }
    if (this.languageSubscription) {
      this.languageSubscription.unsubscribe();
    }
  }

  get env(): SettingsModel {
    return environment;
  }

}
