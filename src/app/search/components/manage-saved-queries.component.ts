/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';

import * as fromSearch from '../reducers';
import * as fromSavedQueryActions from '../actions/saved-query.actions';
import * as fromFormActions from '../actions/form.actions';

/**
 * Provides elements for listing, loading and deleting saved queries
 */
@Component({
  selector: 'app-manage-saved-queries',
  template: `
      <hr *ngIf="(numberOfSavedQueries$ | async)">
      <div class="mt-2" *ngIf="(numberOfSavedQueries$ | async)">
          <div class="h6">Meine Suchen</div>
          <div class="no-gutters">
              <div *ngFor="let savedQuery of savedQueries$ | async"
                   class="input-group input-group-sm col-md-6 mt-1">
      <span class="input-group-btn">
              <button class="btn btn-primary fa fa-play"
                      type="button"
                      (click)="loadUserQuery(savedQuery.id)">
              </button>
            </span>
                  <input type="text"
                         class="form-control"
                         disabled
                         title="Name der Query"
                         [value]=savedQuery.name>
                  <span class="input-group-btn">
              <app-copy-link
                      [data]="savedQuery.query">
              </app-copy-link>
            </span>
                  <span class="input-group-btn">
              <button class="btn btn-danger fa fa-trash"
                      type="button"
                      (click)="deleteUserQuery(savedQuery.id)"></button>
            </span>
              </div>
          </div>
      </div>
  `,
  styles: [`
      .input-group-btn select {
          border-color: #ccc;
      }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ManageSavedQueriesComponent {
  savedQueries$: Observable<any>;
  numberOfSavedQueries$: Observable<number>;

  private _savedQueryEntities: any;

  constructor(private _searchStore: Store<fromSearch.State>) {
    this.savedQueries$ = _searchStore.pipe(select(fromSearch.getAllSavedQueries));
    _searchStore.pipe(select(fromSearch.getSavedQueryEntities)).subscribe(entities => this._savedQueryEntities = entities);
    this.numberOfSavedQueries$ = _searchStore.pipe(select(fromSearch.getSavedQueriesCount));
  }

  loadUserQuery(index: string) {
    const key = 'queryParams';
    const {[key]: value, ...formValues} = this._savedQueryEntities[index].query;
    this._searchStore.dispatch(new fromFormActions.UpdateEntireForm(formValues));
  }

  deleteUserQuery(index: string) {
    this._searchStore.dispatch(new fromSavedQueryActions.DeleteSavedQuery({id: index}));
  }
}
