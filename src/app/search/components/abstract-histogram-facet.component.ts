/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Input, ViewChild} from "@angular/core";
import * as fromFormActions from "../actions/form.actions";
import {Observable} from "rxjs/index";
import {select, Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import * as fromQueryActions from "../actions/query.actions";
import {environment} from "../../../environments/environment";
import {FacetFieldsModel} from "../../shared/models/settings.model";
import {filter, take} from "rxjs/operators";
import {buildId} from "../reducers/facet.reducer";

export interface MinMaxProvider {
  clearFields();
}

/**
 * Either ok is set or error.
 */
export interface ValidatorResult {
  ok?: EmittedValues;
  error?: ('empty' | 'fromGreaterThanTo' | 'processingFailure');
}

export interface EmittedValues {
  from?: string;
  to?: string;
}

/**
 * Abstract histogram facet component.
 */
export abstract class AbstractHistogramFacetComponent<T extends MinMaxProvider> {
  @Input() key: string;
  @ViewChild("inputFields", {static: false}) inputFields: T;

  facetFieldByKey$: Observable<any>;
  facetFieldCountByKey$: Observable<any>;

  facetFieldsConfig: FacetFieldsModel;

  static buildLabelAndValue(from: string, to: string): { value: any, label: string } {
    const value: any = {};
    let label = "";

    if (from !== undefined) {
      value.gte = from;
      label = from;
    } else {
      label = "...";
    }

    label += " - ";

    if (to !== undefined) {
      value.lte = to;
      label += to;
    } else {
      label += "...";
    }

    return {label, value};
  }

  constructor(protected _searchStore: Store<fromSearch.State>) {
    this.facetFieldByKey$ = _searchStore.pipe(select(fromSearch.getHistogramValues));
    this.facetFieldCountByKey$ = _searchStore.pipe(
      select(fromSearch.getFacetHistogramCount),
      filter(x => typeof(x) === 'object' && x !== null),
    );
    this.facetFieldsConfig = environment.facetFields;
  }

  facetAction(key, value, selected) {
    this.updateSearch(key, value.label, value.id, value.value, selected);
  }

  selectRange(key, fromStr, toStr) {
    try {
      // can't use pipe expression in action handler!
      let facetFieldByKey;
      this.facetFieldByKey$.pipe(take(1)).subscribe((v) => facetFieldByKey = v);
      const {label, value} = AbstractHistogramFacetComponent.buildLabelAndValue(fromStr, toStr);
      const facet = facetFieldByKey[this.key];
      const id = buildId(value);
      this.updateSearch(key, label, id, value, this.isSelected(facet, id));
    } catch (e) {
      console.error("Bad range input", e);
    }
  }

  updateSearch(key, label, id, value, selected) {
    if (selected) {
      this._searchStore.dispatch(new fromFormActions.RemoveHistogramBoundary({key: key, id: id}));
    } else {
      this._searchStore.dispatch(new fromFormActions.AddHistogramBoundaries({
        key: key,
        id: id,
        label: label,
        value: value
      }));
    }
    this._searchStore.dispatch(new fromFormActions.SetShowMissingValuesInRange(key, false));
    this.doSearch();
    this.inputFields.clearFields();
  }

  doSearch() {
    this._searchStore.dispatch(new fromQueryActions.SetOffset(0));
    this._searchStore.dispatch(new fromFormActions.SetInFacetSearch(undefined, undefined));
    this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  buildTemplateItems(facetFieldByKey, facetFieldCountByKey) {
    const facet = facetFieldByKey[this.key];
    if (!facet) {
      return [];
    }
    let values;
    if (facetFieldCountByKey) {
      values = facetFieldCountByKey[facet.field];
    }
    if (!values) {
      values = [];
    }
    const selectedValues = [];
    return selectedValues.concat(values
      .filter((v) => !this.isSelected(facet, v.id))
      .map((v) => ({
        key: this.key,
        value: this.formatLabel(v),
        selected: false
      })));
  }

  protected formatLabel(v) {
    return v;
  }

  protected isSelected(facet, id): boolean {
    const selected = facet.values.filter((f) => f.id === id);
    return selected.length !== 0;
  }

  get fromSearch() {
    return fromSearch;
  }

  replaceFacetValue(key, value) {
    this._searchStore.dispatch(new fromFormActions.ResetHistogramBoundaryTo({key: key, id: value.id}));
    this.doSearch();
  }
}
