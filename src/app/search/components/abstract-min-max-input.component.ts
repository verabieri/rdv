/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ElementRef, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {EmittedValues, MinMaxProvider} from "./abstract-histogram-facet.component";
import {FormControl, FormGroup} from "@angular/forms";

/**
 * Abstract base class for UI components which display two input fields - for a from and to int value.
 */
export abstract class AbstractMinMaxInputComponent<T> implements MinMaxProvider {
  @Input() key: string;
  @Input() min: T;
  @Input() max: T;
  @Output() submitted = new EventEmitter<EmittedValues>(false);
  @ViewChild("from", {static: false}) from: ElementRef;
  @ViewChild("to", {static: false}) to: ElementRef;

  formGroup: FormGroup;
  fromGreaterThanTo = false;
  submitIsDisabled = 'disabled';

  constructor() {
    this.formGroup = new FormGroup({
      firstName: new FormControl()
    });
  }

  clearFields() {
    this.from.nativeElement.value = "";
    this.to.nativeElement.value = "";
    this.submitIsDisabled = 'disabled';
  }

  abstract onSubmit(fromStr: string, toStr: string);

  checkSubmit(fromStr: string, toStr: string) {
    this.submitIsDisabled = (fromStr.trim().length === 0 && toStr.trim().length === 0) ? 'disabled' : '';
  }

}
