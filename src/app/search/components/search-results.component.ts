/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, OnDestroy} from "@angular/core";
import {Observable, Subscription} from "rxjs/index";
import {select, Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import {SettingsModel, SortFieldsModel} from "../../shared/models/settings.model";
import {environment} from "../../../environments/environment";
import * as fromQueryActions from "../actions/query.actions";
import {map, take} from "rxjs/operators";
import * as fromFormActions from "../actions/form.actions";
import {combineLatest} from "rxjs";
import {buildId} from "../reducers/facet.reducer";
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-search-results',
  template: `
    <div class="search-results">
      <div class="search-results__header row pl-3 mr-1">
        <h4 class="search-results__title">{{'search-results.title' | translate:{value: (searchCount$ | async)} }}</h4>
        <div class="pr-0 pl-0 col-sm search-results__sorting align-self-center mt-2 text-left
            align-self-sm-end text-sm-right mt-sm-3">
          <div class="styled-select d-md-inline-block">
            <select
              [title]="'search-results.sort.title' | translate"
              [ngModel]="sorting"
              name="sorting"
              (ngModelChange)="switchSorting($event)">
              <option *ngFor="let sortOption of sortFields" [ngValue]="sortOption">{{sortOption.display | translate}}</option>
            </select>
          </div>
          <app-rows-per-page
            class="d-sm-block d-md-inline-block ml-3 text-sm-right text-md-center"
            [rowsPerPage]="rowsPerPage$ | async"
            (changeRowsPerPage)="changeRowsPerPage($event)"></app-rows-per-page>
          <div *ngIf="env.iiifProxyUrl" class="search-results__mode-container"
               class="d-sm-block d-md-inline-block align-top">
            <button class="search-results__mode search-results__mode--list"
                    [title]="'search-results.view-mode-help.List' | translate"
                    [disabled]="!(iiifResultDisplayMode$ | async)"
                    (click)="changeResultViewMode()">
              {{'search-results.view-mode.List' | translate}}
            </button>
            <button class="search-results__mode search-results__mode--iiif"
                    [title]="'search-results.view-mode-help.IIIF' | translate"
                    [disabled]="(iiifResultDisplayMode$ | async)"
                    (click)="changeResultViewMode()">
              {{'search-results.view-mode.IIIF' | translate}}
            </button>
          </div>
        </div>
      </div>
      <div class="mt-2 row" [class.iiifmode]="(iiifResultDisplayMode$ | async)">
        <div class="col-12 col-sm-4 search-results__facets">
          <app-collapsible-facets></app-collapsible-facets>
        </div>
        <div class="col-12 col-sm-8 search-results__hit-container">
          <ng-container *ngIf="!(iiifResultDisplayMode$ | async)">
            <div class="search-results__hit"
                 *ngFor="let doc of (docs$ | async); index as i">
              <app-search-hit [doc]="doc" [indexInResultList]="i + 1"></app-search-hit>
            </div>
            <div class="row">
              <div class="col-1 d-none d-sm-block"></div>
              <div class="col-11 text-center text-sm-left" *ngIf="(docs$ | async).length < (searchCount$ | async)">
                <button type="button" class="btn search-results__more btn-red"
                        (click)="loadMore()">{{'search-results.load_more' | translate}}
                </button>
              </div>
            </div>
          </ng-container>
          <ng-container *ngIf="(iiifResultDisplayMode$ | async)">
            <app-uv-manifest-link [manifestUrl]="(iiifUrl$ | async)"></app-uv-manifest-link>
            <app-universal-viewer [manifestUrl]="(iiifUrl$ | async)"></app-universal-viewer>
            <div class="col-11 text-center search-results__hit-container-actions">
              <button *ngIf="(offset$ | async) > 0"
                      type="button"
                      class="btn search-results__more btn-red"
                      (click)="loadPrevPage()">{{'search-results.load_prev' | translate}}
              </button>
              <div class="search-results__hit-container-actions-page d-sm-inline-block">
                Seite {{((offset$ | async) / (rowsPerPage$ | async)) + 1}}
              </div>
              <button *ngIf="((offset$ | async) + (rowsPerPage$ | async)) < (searchCount$ | async)"
                      type="button"
                      class="btn search-results__more btn-red"
                      (click)="loadNextPage()">{{'search-results.load_next' | translate}}
              </button>
            </div>
          </ng-container>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchResultsComponent implements OnDestroy {
  searchCount$: Observable<number>;
  iiifUrl$: Observable<string>;
  docs$: Observable<any>;
  offset$: Observable<number>;
  rowsPerPage$: Observable<number>;
  sort$: Observable<any>;
  selectedFacetValueByKey$: Observable<any>;
  iiifResultDisplayMode$: Observable<boolean>;

  readonly sortFields = environment.sortFields;
  protected currentOffset = 0;
  protected rowsPerPage;

  protected offsetSubscription: Subscription;
  protected rowsPerPageSubscription: Subscription;

  sorting: SortFieldsModel;
  protected sortSubscriber: Subscription;

  constructor(private _searchStore: Store<fromSearch.State>,
              private toastr: ToastrService,
              private translate: TranslateService) {
    this.searchCount$ = _searchStore.pipe(select(fromSearch.getTotalResultsCount));
    this.iiifUrl$ = _searchStore.pipe(select(fromSearch.getIIIFResultUrl));
    this.docs$ = _searchStore.pipe(select(fromSearch.getAllResults));
    this.offset$ = _searchStore.pipe(select(fromSearch.getResultOffset));
    this.offsetSubscription = this.offset$.subscribe((newOffset) => {
      this.currentOffset = newOffset;
    });
    this.rowsPerPage$ = _searchStore.pipe(select(fromSearch.getResultRows));
    this.rowsPerPageSubscription = this.rowsPerPage$.subscribe((newRowsPerPage) => {
      this.rowsPerPage = newRowsPerPage;
    });

    this.sort$ = combineLatest([
      _searchStore.pipe(select(fromSearch.getResultSortField)),
      _searchStore.pipe(select(fromSearch.getResultSortOrder))
    ]);
    this.sortSubscriber = this.sort$.subscribe((v) => {
      this.sorting = this.findSortField(v[0], v[1]);
    });
    this.selectedFacetValueByKey$ = combineLatest([
      this._searchStore.select(fromSearch.getFacetValues),
      this._searchStore.select(fromSearch.getHistogramValues),
      this._searchStore.select(fromSearch.getHierarchyValues),
    ]).pipe(map(([facets, histograms, hierarchies]) => ((key: string) => {
      return hierarchies[key] ?
        hierarchies[key].values : (histograms[key] ?
          histograms[key].values : facets[key].values);
    })));

    this.iiifResultDisplayMode$ = this._searchStore.pipe(select(fromSearch.getDisplayResultInIIIF));
  }

  changeResultViewMode() {
    let currentResultListMode = false;
    this.iiifResultDisplayMode$.subscribe((flag) => currentResultListMode = flag);
    const newResultListMode = !currentResultListMode;
    this._searchStore.dispatch(new fromQueryActions.SetResultListDisplayMode(newResultListMode));
    const iiifRestrictions = environment.iiifRestrictions;
    let found = false;
    if (newResultListMode && iiifRestrictions) {
      for (const {field, messageTitleKey, messageTextKey, values} of iiifRestrictions) {
        const allowedIds = values.map((fv) => buildId(fv.value));
        let selectedFacets;
        this.selectedFacetValueByKey$.pipe(take(1)).subscribe((v) => selectedFacets = v(field));
        if (selectedFacets) {
          for (const sfv of selectedFacets) {
            if (allowedIds.indexOf(sfv.id) < 0) {
              this._searchStore.dispatch(new fromFormActions.RemoveFacetValue({facet: field, id: sfv.id}));
            } else {
              found = true;
            }
          }
        }
        if (!found) {
          if (messageTextKey) {
            const msg = this.translate.instant(messageTextKey);
            const title = messageTitleKey ? this.translate.instant(messageTitleKey) : undefined;
            this.toastr.info(msg, title, {disableTimeOut: true, closeButton: true});
          }
          this._searchStore.dispatch(new fromFormActions.AddFacetValue(
            {facet: field, id: allowedIds[0], label: values[0].label, value: values[0].value}));
        }
      }
    }
    // iiif search endpoint differs from list based endpoint, so search again is needed
    this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  findSortField(sortField: string, sortDir: string): SortFieldsModel {
    if (this.sortFields) {
      for (const sortFieldConfig of this.sortFields) {
        if (sortField === sortFieldConfig.field && sortDir === sortFieldConfig.order.toString()) {
          return sortFieldConfig;
        }
      }
    }
    return undefined;
  }

  loadMore() {
    let lastId;
    this.docs$.pipe(take(1)).subscribe((docs) => {
      if (docs && docs.length > 0) {
        lastId = docs[docs.length - 1].search_after_values;
      }
    });
    if (lastId) {
      this._searchStore.dispatch(new fromQueryActions.SetOffset(this.currentOffset + this.rowsPerPage));
      this._searchStore.dispatch(new fromQueryActions.NextPage(lastId));
    }
  }

  loadPrevPage() {
    this._searchStore.dispatch(new fromQueryActions.SetOffset(this.currentOffset - this.rowsPerPage));
    this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  loadNextPage() {
    this._searchStore.dispatch(new fromQueryActions.SetOffset(this.currentOffset + this.rowsPerPage));
    this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  switchSorting(newSorting: SortFieldsModel) {
    this._searchStore.dispatch(new fromQueryActions.SetSortOrder(newSorting.order));
    this._searchStore.dispatch(new fromQueryActions.SetSortField(newSorting.field));
    this._searchStore.dispatch(new fromQueryActions.SetOffset(0));
    // sorting doesn't change facets, but only a few values are returned, so reset in-facet search too
    this._searchStore.dispatch(new fromFormActions.SetInFacetSearch(undefined, undefined));
    this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  changeRowsPerPage(no: number) {
    this._searchStore.dispatch(new fromQueryActions.SetOffset(0));
    this._searchStore.dispatch(new fromQueryActions.SetRows(no));
    // sorting doesn't change facets, but only a few values are returned, so reset in-facet search too
    this._searchStore.dispatch(new fromFormActions.SetInFacetSearch(undefined, undefined));
    this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  ngOnDestroy(): void {
    if (this.offsetSubscription) {
      this.offsetSubscription.unsubscribe();
    }
    if (this.rowsPerPageSubscription) {
      this.rowsPerPageSubscription.unsubscribe();
    }
    if (this.sortSubscriber) {
      this.sortSubscriber.unsubscribe();
    }
  }

  get env(): SettingsModel {
    return environment;
  }

}
