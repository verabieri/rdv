/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';

import * as fromSearch from '../reducers';
import * as fromQueryActions from "../actions/query.actions";
import * as fromFormActions from "../actions/form.actions";
import {take} from "rxjs/operators";

/**
 * Displays text search fields
 */
@Component({
  selector: 'app-simple-search',
  template: `
    <div class="simple-search">
      <h3 class="simple-search__title">{{'simple-search.title' | translate}}</h3>
      <!-- TODO what if several searchFields are configured? -->
      <div class="row no-gutters simple-search__field-container" *ngFor="let field of searchFields$ | async | objectKeys">
        <div class="col-8">
          <input class="form-control form-control-sm simple-search__field"
                 type="text"
                 [value]="(searchFieldByKey$ | async)(field).value"
                 #searchValue
                 (keydown.enter)="updateSearchValue(field, searchValue.value)"
                 placeholder="{{'simple-search.enter_search_phrase' | translate}}">
        </div>
        <div class="col-4">
          <button type="button" class="btn btn-red simple-search__search simple-search__search--large d-none d-sm-block"
                  (click)="updateSearchValue(field, searchValue.value)">
            {{'simple-search.search' | translate}}
          </button>
          <button type="button" class="btn btn-red simple-search__search simple-search__search--small d-block d-sm-none"
                  (click)="updateSearchValue(field, searchValue.value)">
          </button>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SimpleSearchComponent {
  searchFields$: Observable<any>;
  searchFieldByKey$: Observable<any>;

  constructor(protected searchStore: Store<fromSearch.State>) {
    this.searchFields$ = searchStore.pipe(select(fromSearch.getSearchValues));
    this.searchFieldByKey$ = searchStore.pipe(select(fromSearch.getSearchValuesByKey));
  }

  updateSearchValue(fieldName: string, value: string) {
    // subscribe once only ...
    this.searchFields$.pipe(take(1)).subscribe((oldValue) => {
      const reset = value !== oldValue;
      if (reset) {
        this.searchStore.dispatch(new fromQueryActions.Reset());
      }
      this.searchStore.dispatch(new fromFormActions.UpdateSearchFieldValue({field: fieldName, value: value}));
      this.searchStore.dispatch(new fromQueryActions.SimpleSearch());
    });
  }
}
