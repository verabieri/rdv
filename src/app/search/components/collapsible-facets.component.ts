/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, ElementRef, ViewChild} from "@angular/core";
import {Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import {combineLatest, Observable} from "rxjs/index";
import * as fromFormActions from "../actions/form.actions";
import * as fromQueryActions from "../actions/query.actions";
import {environment} from "../../../environments/environment";
import {FacetFieldType, HistogramFieldModel, HistogramFieldType} from "../../shared/models/settings.model";
import {map, take} from "rxjs/operators";
import {DateFormatService} from "../../shared/services/date-format.service";

interface Group {
  pos?: string;
  members: any[];
}

/**
 * Displays collapsible facets.
 */
@Component({
  selector: 'app-collapsible-facets',
  template: `
    <div class="d-block d-sm-none">
      <button class="btn btn-red collapsible-facets__toggler" (click)="toggleFacets()">
        {{'collapsible-facets.filter' | translate}}
      </button>
    </div>
    <div #facetContainer class="collapsible-facets__container">
      <div class="collapsible-facets__container-overlay"></div>
      <div class="collapsible-facets__container-elements">
        <div class="d-block d-sm-none">
          <button class="btn collapsible-facets__use-filter link-black" (click)="toggleFacets()">
            {{'collapsible-facets.use_filter' | translate}}
          </button>
        </div>
        <div class="facet">
          <ul class="facet__list facet__list--selected">
            <ng-container *ngFor="let key of facetFieldsConfig | objectKeys">
              <ng-container *ngIf="(selectedFacetValueByKey$ | async)(key)?.length > 0">
                <li *ngFor="let g of group((selectedFacetValueByKey$ | async)(key))">
                  <button *ngIf="g.members.length === 1"
                          type="button"
                          (click)="resetFacetFilter(key, g.members[0].id, true)"
                          class="facet__entry facet__entry--selected link-black">
                    <span>{{facetFieldsConfig[key].label | translate}}: {{formatLastLabel(g, facetFieldsConfig[key].data_type)}}</span>
                  </button>
                  <app-collapsible
                    *ngIf="g.members.length > 1"
                    [key]="key + '/selected'"
                    [title]="(facetFieldsConfig[key].label | translate) + ': ' + formatLastLabel(g, facetFieldsConfig[key].data_type)"
                    buttonClass="facet__entry facet__entry--selected-group link-black"
                    [showDelButton]="true"
                    (delete)="removeAllFacetValues(key)"
                  >
                    <button *ngFor="let vg of g.members"
                            type="button"
                            (click)="resetFacetFilter(key, vg.id, false)"
                            class="facet__entry facet__entry--selected link-black">
                      <span>{{formatLabel(vg, facetFieldsConfig[key].data_type).label}}</span>
                    </button>
                  </app-collapsible>
                </li>
              </ng-container>
            </ng-container>
          </ul>
        </div>
        <ng-container *ngFor="let key of
        filterCountKeys(selectedFacetValueByKey$ | async, facetFieldCountByKey$ | async); last as isLast;">
          <div [class.active]="facetFieldsConfig[key].order == 1"
               [class.last]="isLast"
               class="collapsible-facets"
               role="listitem">
            <ng-container [ngSwitch]="facetFieldsConfig[key].facetType">
              <app-collapsible-facet *ngSwitchCase="simpleType()" [key]="key"
                                     [title]="facetFieldsConfig[key].label | translate"></app-collapsible-facet>
              <ng-container *ngSwitchCase="subcategoryType()">
                <app-collapsible [title]="facetFieldsConfig[key].label | translate" [key]="key">
                  <app-subcat-facet [key]="key"></app-subcat-facet>
                </app-collapsible>
              </ng-container>
              <ng-container *ngSwitchCase="histogramType()">
                <ng-container [ngSwitch]="facetFieldsConfig[key].data_type">
                  <app-collapsible *ngSwitchCase="histogramIntType()" [title]="facetFieldsConfig[key].label | translate" [key]="key">
                    <app-int-histogram-facet *ngIf="!facetFieldsConfig[key].showAggs" [key]="key"></app-int-histogram-facet>
                    <app-int-hierarchic-facet *ngIf="facetFieldsConfig[key].showAggs" [key]="key"></app-int-hierarchic-facet>
                  </app-collapsible>
                  <app-collapsible *ngSwitchCase="histogramDateType()" [title]="facetFieldsConfig[key].label | translate" [key]="key">
                    <app-date-histogram-facet *ngIf="!facetFieldsConfig[key].showAggs" [key]="key"></app-date-histogram-facet>
                    <app-date-hierarchic-facet *ngIf="facetFieldsConfig[key].showAggs" [key]="key"></app-date-hierarchic-facet>
                  </app-collapsible>
                  <app-collapsible *ngSwitchCase="histogramGeoType()" [title]="facetFieldsConfig[key].label | translate" [key]="key">
                    <app-geo-histogram-facet *ngIf="!facetFieldsConfig[key].showAggs" [key]="key"></app-geo-histogram-facet>
                    <app-geo-hierarchic-facet *ngIf="facetFieldsConfig[key].showAggs" [key]="key"></app-geo-hierarchic-facet>
                  </app-collapsible>
                </ng-container>
              </ng-container>
              <app-collapsible *ngSwitchCase="hierarchicType()" [title]="facetFieldsConfig[key].label | translate" [key]="key">
                <app-hierarchic-facet [key]="key"></app-hierarchic-facet>
              </app-collapsible>
            </ng-container>
          </div>
        </ng-container>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollapsibleFacetsComponent {
  @ViewChild("facetContainer", {static: false}) facetsRef: ElementRef<HTMLDivElement>;

  facetFieldsConfig: any;

  selectedFacetValueByKey$: Observable<any>;
  facetFieldCountByKey$: Observable<any>;
  inFacetSearch$: Observable<any>;

  constructor(protected searchStore: Store<fromSearch.State>, protected dateFormatService: DateFormatService) {
    this.facetFieldsConfig = environment.facetFields;

    this.selectedFacetValueByKey$ = combineLatest([
      searchStore.select(fromSearch.getFacetValues),
      searchStore.select(fromSearch.getHistogramValues),
      searchStore.select(fromSearch.getHierarchyValues),
    ]).pipe(map(([facets, histograms, hierarchies]) => ((key: string) => {
      return hierarchies[key] ?
        hierarchies[key].values : (histograms[key] ?
          histograms[key].values : facets[key].values);
    })));

    this.facetFieldCountByKey$ = combineLatest([
      searchStore.select(fromSearch.getFacetFieldCountByKey),
      searchStore.select(fromSearch.getFacetHistogramCountByKey),
      searchStore.select(fromSearch.getHierarchicFacetCountByKey),
    ]).pipe(map(([facets, histograms, hierarchies]) => ((field: string) => {
      let v = hierarchies(field);
      if (v) {
        return v;
      }
      v = histograms(field);
      return v ? v : facets(field);
    })));

    this.inFacetSearch$ = searchStore.select(fromSearch.getInFacetSearchValues);
  }

  filterCountKeys(selectedFacetValueByKey, facetFieldCountByKey) {
    let inFacetSearchField;
    this.inFacetSearch$.pipe(take(1)).subscribe((v) => inFacetSearchField = v.field);
    const config = this.facetFieldsConfig;
    // console.log("---- FILTER", inFacetSearchField);
    return Object.keys(config)
      .filter((key) => {
        const fieldConfig = config[key];
        // console.log("FIELD", key, fieldConfig);
        const fieldName = fieldConfig.field;
        // always show int histogram facets
        if (fieldConfig.facetType === FacetFieldType.HISTOGRAM) {
          const histogramFacet = fieldConfig as HistogramFieldModel;
          if (histogramFacet.data_type === HistogramFieldType.INT || histogramFacet.data_type === HistogramFieldType.GEO) {
            // console.log("SHOW HISTO", key);
            return true;
          }
        }
        const values = facetFieldCountByKey(fieldName);
        const selectedFacetValues = selectedFacetValueByKey(key);
        // console.log("VALUES", key, "values=", values, "sel=", selectedFacetValues);
        // show facet if either has selected values or unselected values
        // but keep facet even if empty if an in-facet search was done
        return (selectedFacetValues && selectedFacetValues.length > 0)
          || (values && values.length > 0)
          || (inFacetSearchField === fieldName);
      })
      .sort((a, b) => config[a].order - config[b].order);
  }

  protected resetFacetFilter(key, id, forceRemove: boolean) {
    if (this.isSimpleFacet(key)) {
      this.searchStore.dispatch(new fromFormActions.RemoveFacetValue({facet: key, id: id}));
    }
    if (this.facetFieldsConfig[key].facetType === FacetFieldType.HISTOGRAM) {
      if (!forceRemove && this.facetFieldsConfig[key].showAggs) {
        this.searchStore.dispatch(new fromFormActions.ResetHistogramBoundaryTo({key: key, id: id}));
      } else {
        this.searchStore.dispatch(new fromFormActions.RemoveHistogramBoundary({key: key, id: id}));
      }
    }
    if (this.facetFieldsConfig[key].facetType === FacetFieldType.HIERARCHIC) {
      if (forceRemove) {
        this.searchStore.dispatch(new fromFormActions.RemoveHierarchicFacetValue({key: key, id: id}));
      } else {
        this.searchStore.dispatch(new fromFormActions.ResetHierarchicFacetValueTo({key: key, id: id}));
      }
    }
    this.doSearch();
  }

  protected doSearch() {
    this.searchStore.dispatch(new fromQueryActions.SetOffset(0));
    this.searchStore.dispatch(new fromFormActions.SetInFacetSearch(undefined, undefined));
    this.searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  protected isSimpleFacet(key) {
    return this.facetFieldsConfig[key].facetType === FacetFieldType.SIMPLE
      || this.facetFieldsConfig[key].facetType === FacetFieldType.SUBCATEGORY;
  }

  protected removeAllFacetValues(key) {
    if (this.isSimpleFacet(key)) {
      this.searchStore.dispatch(new fromFormActions.RemoveAllFacetValue(key));
    }
    if (this.facetFieldsConfig[key].facetType === FacetFieldType.HISTOGRAM) {
      this.searchStore.dispatch(new fromFormActions.RemoveAllHistogramBoundary(key, false));
    }
    if (this.facetFieldsConfig[key].facetType === FacetFieldType.HIERARCHIC) {
      this.searchStore.dispatch(new fromFormActions.RemoveAllHierarchicFacetValue(key));
    }
    this.doSearch();
  }

  protected simpleType(): string {
    return FacetFieldType.SIMPLE.toString();
  }

  protected subcategoryType(): string {
    return FacetFieldType.SUBCATEGORY.toString();
  }

  protected histogramType(): string {
    return FacetFieldType.HISTOGRAM.toString();
  }

  protected histogramIntType(): string {
    return HistogramFieldType.INT.toString();
  }

  protected histogramDateType(): string {
    return HistogramFieldType.DATE.toString();
  }

  protected histogramGeoType(): string {
    return HistogramFieldType.GEO.toString();
  }

  protected hierarchicType(): string {
    return FacetFieldType.HIERARCHIC.toString();
  }

  toggleFacets() {
    const showClass = "collapsible-facets--show-in-overlay";
    const isShowingFacets = this.facetsRef.nativeElement.classList.contains(showClass);
    if (isShowingFacets) {
      this.facetsRef.nativeElement.classList.remove(showClass);
      document.documentElement.classList.remove(showClass);
      document.body.classList.remove(showClass);
    } else {
      this.facetsRef.nativeElement.classList.add(showClass);
      document.documentElement.classList.add(showClass);
      document.body.classList.add(showClass);
    }
  }

  group(facetValues: any[]) {
    facetValues.sort((f1, f2) => (f1.pos || "").length - (f2.pos || "").length);

    const groups: Group[] = [];
    for (const f of facetValues) {
      let found = false;
      if (f.value.pos) {
        for (const g of groups) {
          if (g.pos.length > 0 && f.value.pos.indexOf(g.pos) === 0) {
            g.members.push(f);
            found = true;
          }
        }
      }
      if (!found) {
        groups.push({pos: f.value.pos || "", members: [f]});
      }
    }

    return groups;
  }

  formatLabel(v, type: HistogramFieldType) {
    if (type === HistogramFieldType.DATE && !v.value.type) {
      v = {...v, value: {...v.value, type: 'day-range'}};
    }
    return this.dateFormatService.formatLabel(v);
  }

  formatLastLabel(g: Group, type: HistogramFieldType) {
    return this.formatLabel(g.members[g.members.length - 1], type).label;
  }
}
