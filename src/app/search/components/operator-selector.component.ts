/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  ViewChild
} from "@angular/core";
import {Observable} from "rxjs/index";
import * as fromSearch from "../reducers";
import {environment} from "../../../environments/environment";
import {MemoizedSelector, select, Store} from "@ngrx/store";
import * as fromFormActions from "../actions/form.actions";
import {take} from "rxjs/operators";

/**
 * Displays simple facet.
 */
@Component({
  selector: 'app-operator-selector',
  template: `
    <ng-container *ngIf="facetFieldsConfig[key].operators?.length > 1">
      {{'operator-selector.label' | translate}}
      <div class="styled-select">
        <select title="Verknüpfungsart" class="btn btn-sm" #sel (change)="changeOperator(key, sel.value)">
          <option *ngFor="let operator of facetFieldsConfig[key].operators"
                  [value]="operator"
                  [selected]="isSelected(operator)"
          >
            {{'operator-selector.' + operator | translate}}
          </option>
        </select>
      </div>
    </ng-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OperatorSelectorComponent {
  @Input() key;
  @Input() facetValueSelector: MemoizedSelector<any, any>;
  @Output() changed = new EventEmitter<string>(false);

  facetFieldsConfig: any;

  // facetFieldByKey$: Observable<any>;

  constructor(protected searchStore: Store<fromSearch.State>) {
    this.facetFieldsConfig = environment.facetFields;
  }

  isSelected(op): boolean {
    let facetFieldByKey;
    this.searchStore.pipe(select(this.facetValueSelector)).pipe(take(1)).subscribe((v) => facetFieldByKey = v);
    const facet = (typeof(facetFieldByKey) === "function") ? facetFieldByKey(this.key) : facetFieldByKey[this.key];
    if (facet) {
      return op === facet.operator;
    }
    return op === this.facetFieldsConfig[this.key].operator;
  }

  changeOperator(facetKey: string, op: string) {
    this.searchStore.dispatch(new fromFormActions.UpdateFacetOperator({facet: facetKey, value: op}));
    this.changed.emit(op);
  }

}
