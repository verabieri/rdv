/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {AddFacetValue, FormActions, FormActionTypes, UpdateFacetOperator} from '../actions/form.actions';
import {environment} from '../../../environments/environment';
import {FacetFieldModel, FacetFieldType, HistogramFieldModel, RangeFieldModel} from '../../shared/models/settings.model';
import {buildId} from "./facet.reducer";

/**
 * Search form state
 */
export interface State {
  /**
   * Selected facets
   */
  facetFields: any;
  /**
   * Set filters
   */
  filterFields: any;
  /**
   * Selected ranges
   */
  rangeFields: any;
  /**
   * Selected histograms
   */
  histogramFields: any;
  /**
   * Selected hierarchic facets
   */
  hierarchyFields: any;
  /**
   * Search field values
   */
  searchFields: any;
  /**
   * Currently searched facet values.
   */
  facetSearchField: string;
  facetSearchPrefix: string;
}

/**
 * @ignore
 */
export const initialState: State = {
  facetFields: Object.keys(environment.facetFields)
    .filter((key: string) => facetType(key) === FacetFieldType.SIMPLE
      || facetType(key) === FacetFieldType.SUBCATEGORY)
    .reduce((agg, k) => {
      const h = environment.facetFields[k] as FacetFieldModel;
      agg[k] = {
        field: h.field,
        values: [],
        operator: h.operator,
      };
      return agg;
    }, {}),
  filterFields: Object.keys(environment.filterFields).reduce((agg, k) => {
    agg[k] = {
      field: environment.filterFields[k].field,
      values: environment.filterFields[k].options.map(x => x.value),
    };
    return agg;
  }, {}),
  rangeFields: Object.keys(environment.rangeFields).reduce((agg, k) => {
    const h = environment.rangeFields[k] as RangeFieldModel;
    agg[k] = {
      field: h.field,
      min: h.min,
      from: h.from,
      to: h.to,
      max: h.max,
      showMissingValues: h.showMissingValues,
    };
    return agg;
  }, {}),
  histogramFields: Object.keys(environment.facetFields)
    .filter((key: string) => facetType(key) === FacetFieldType.HISTOGRAM)
    .reduce((agg, k) => {
      const h = environment.facetFields[k] as HistogramFieldModel;
      agg[k] = {
        field: h.field,
        operator: h.operator,
        data_type: h.data_type.toString(),
        values: [],
        showAggs: h.showAggs,
      };
      return agg;
    }, {}),
  hierarchyFields: Object.keys(environment.facetFields)
    .filter((key: string) => facetType(key) === FacetFieldType.HIERARCHIC)
    .reduce((agg, k) => {
      const h = environment.facetFields[k] as FacetFieldModel;
      agg[k] = {
        field: h.field,
        values: [],
        operator: h.operator,
      };
      return agg;
    }, {}),
  searchFields: environment.searchFields.preselect.reduce((agg, k, index) => {
    agg['search' + index] = {
      field: k,
      value: '',
    };
    return agg;
  }, {}),
  facetSearchField: undefined,
  facetSearchPrefix: undefined
};

function collectionByFacetType(ft, defaultValue) {
  return (ft === FacetFieldType.SIMPLE) ? 'facetFields'
    : ((ft === FacetFieldType.HIERARCHIC) ? 'hierarchyFields'
      : ((ft === FacetFieldType.HISTOGRAM) ? 'histogramFields' : defaultValue));
}

function addFacetValue(collection, action: AddFacetValue) {
  return Object.keys(collection).reduce((agg, k) => {
    agg[k] = k === action.payload.facet ?
      {
        ...collection[k],
        values: collection[k].values.concat({
          id: action.payload.id,
          label: action.payload.label,
          value: action.payload.value
        })
      } :
      collection[k];
    return agg;
  }, {});
}

function updateFacetOperator(collection, action: UpdateFacetOperator) {
  return Object.keys(collection).reduce((agg, k) => {
    agg[k] = k === action.payload.facet ? {...collection[k], operator: action.payload.value} : collection[k];
    return agg;
  }, {})
}

function facetType(key) {
  return environment.facetFields[key].facetType;
}

// build inverse lookup table: field -> facet key
const inverseFacetLookUp: { [key: string]: string } = {};
Object.keys(environment.facetFields)
  .forEach((key) => inverseFacetLookUp[environment.facetFields[key].field] = key);

/**
 * @ignore
 */
export function reducer(state = initialState, action: FormActions): State {
  switch (action.type) {

    case FormActionTypes.UpdateEntireForm:
      return {
        ...action.payload
      };

    case FormActionTypes.ToggleFilterValue:
      return {
        ...state,
        filterFields: Object.keys(state['filterFields']).reduce((agg, k) => {
          agg[k] = k === action.payload.filter ?
            {
              ...state.filterFields[k],
              values: state.filterFields[k].values.includes(action.payload.value) ?
                state.filterFields[k].values.filter(x => x !== action.payload.value) :
                state.filterFields[k].values.concat(action.payload.value)
            } :
            state['filterFields'][k];
          return agg;
        }, {}),
      };

    case FormActionTypes.UpdateSearchFieldType:
      return {
        ...state,
        searchFields: Object.keys(state['searchFields']).reduce((agg, k) => {
          agg[k] = k === action.payload.field ?
            {...state.searchFields[k], field: action.payload.type} :
            state['searchFields'][k];
          return agg;
        }, {}),
      };

    case FormActionTypes.UpdateSearchFieldValue:
      return {
        ...state,
        searchFields: Object.keys(state['searchFields']).reduce((agg, k) => {
          agg[k] = k === action.payload.field ?
            {...state.searchFields[k], value: action.payload.value} :
            state['searchFields'][k];
          return agg;
        }, {}),
      };

    case FormActionTypes.AddFacetValue: {
      // use 'facetFields' as fallback to create new state, although unchanged
      const updateCollectionName = collectionByFacetType(facetType(action.payload.facet), 'facetFields');
      const collection = state[updateCollectionName];
      return {...state, [updateCollectionName]: addFacetValue(collection, action)};
    }

    case FormActionTypes.RemoveFacetValue:
      return {
        ...state,
        facetFields: Object.keys(state['facetFields']).reduce((agg, k) => {
          agg[k] = k === action.payload.facet ?
            {...state.facetFields[k], values: state.facetFields[k].values.filter(x => x.id !== action.payload.id)} :
            state['facetFields'][k];
          return agg;
        }, {}),
      };

    case FormActionTypes.RemoveAllFacetValue:
      return {
        ...state,
        facetFields: Object.keys(state['facetFields']).reduce((agg, k) => {
          agg[k] = k === action.facet ? {...state.facetFields[k], values: []} : state['facetFields'][k];
          return agg;
        }, {}),
      };

    case FormActionTypes.UpdateFacetOperator: {
      // use 'facetFields' as fallback to create new state, although unchanged
      const updateCollectionName = collectionByFacetType(facetType(action.payload.facet), 'facetFields');
      const collection = state[updateCollectionName];
      return {
        ...state,
        [updateCollectionName]: updateFacetOperator(collection, action),
      };
    }

    case FormActionTypes.SelectFacets: {
      const newState = {...initialState, searchFields: state.searchFields};
      Object.keys(action.selectedFacets).map((facetName) => {
        const facetKey = inverseFacetLookUp[facetName];
        // use 'facetFields' as fallback to create new state, although unchanged
        const updateCollectionName = collectionByFacetType(facetType(facetKey), 'facetFields');
        for (const sfv of action.selectedFacets[facetName].values) {
          const id = buildId(sfv.value);
          const label = sfv.label || undefined; // ("" + sfv.value);
          newState[updateCollectionName] = addFacetValue(
            newState[updateCollectionName],
            new AddFacetValue({facet: facetKey, id, label, value: sfv.value}));
        }
        const operator = action.selectedFacets[facetName].operator;
        if (operator) {
          newState[updateCollectionName] = updateFacetOperator(
            newState[updateCollectionName],
            new UpdateFacetOperator({facet: facetKey, value: operator}));
        }
      });
      return newState;
    }

    case FormActionTypes.UpdateRangeBoundaries:
      return {
        ...state,
        rangeFields: Object.keys(state['rangeFields']).reduce((agg, k) => {
          agg[k] = k === action.payload.key ?
            {...state.rangeFields[k], from: action.payload.from, to: action.payload.to} :
            state['rangeFields'][k];
          return agg;
        }, {}),
      };

    case FormActionTypes.AddHistogramBoundaries:
      return {
        ...state,
        histogramFields: Object.keys(state['histogramFields']).reduce((agg, k) => {
          agg[k] = k === action.payload.key ?
            {
              ...state.histogramFields[k],
              values: state.histogramFields[k].values.concat({
                id: action.payload.id,
                label: action.payload.label,
                value: action.payload.value
              })
            } :
            state['histogramFields'][k];
          return agg;
        }, {}),
      };

    case FormActionTypes.RemoveHistogramBoundary:
      return {
        ...state,
        histogramFields: Object.keys(state['histogramFields']).reduce((agg, k) => {
          agg[k] = k === action.payload.key ?
            {...state.histogramFields[k], values: state.histogramFields[k].values.filter(x => x.id !== action.payload.id)} :
            state['histogramFields'][k];
          return agg;
        }, {}),
      };

    case FormActionTypes.RemoveAllHistogramBoundary:
      return {
        ...state,
        histogramFields: Object.keys(state['histogramFields']).reduce((agg, k) => {
          let newState;
          if (k === action.key) {
            let newValues = [];
            if (!action.manualsToo) {
              newValues = state.histogramFields[k].values.filter((v) => v.value.pos === undefined);
            }
            newState = {...state.histogramFields[k], values: newValues};
          } else {
            newState = state['histogramFields'][k];
          }
          agg[k] = newState;
          return agg;
        }, {}),
      };

    case FormActionTypes.AddHierarchicFacetValue:
      return {
        ...state,
        hierarchyFields: Object.keys(state.hierarchyFields).reduce((agg, k) => {
          agg[k] = k === action.payload.key ?
            {
              ...state.hierarchyFields[k],
              values: state.hierarchyFields[k].values.concat({
                id: action.payload.id,
                label: action.payload.label,
                value: action.payload.value,
              })
            } :
            state.hierarchyFields[k];
          return agg;
        }, {}),
      };

    case FormActionTypes.RemoveHierarchicFacetValue:
      return {
        ...state,
        hierarchyFields: Object.keys(state.hierarchyFields).reduce((agg, k) => {
          agg[k] = k === action.payload.key ?
            {...state.hierarchyFields[k], values: state.hierarchyFields[k].values.filter(x => x.id !== action.payload.id)} :
            state.hierarchyFields[k];
          return agg;
        }, {}),
      };

    case FormActionTypes.RemoveAllHierarchicFacetValue:
      return {
        ...state,
        hierarchyFields: Object.keys(state.hierarchyFields).reduce((agg, k) => {
          agg[k] = k === action.key ? {...state.hierarchyFields[k], values: []} : state.hierarchyFields[k];
          return agg;
        }, {}),
      };

    case FormActionTypes.ResetHierarchicFacetValueTo:
      return {
        ...state,
        hierarchyFields: Object.keys(state.hierarchyFields).reduce((agg, k) => {
          agg[k] = k === action.payload.key ?
            {
              ...state.hierarchyFields[k],
              values: state.hierarchyFields[k].values.slice(0,
                1 + state.hierarchyFields[k].values.findIndex((v) => v.id === action.payload.id))
            } :
            state.hierarchyFields[k];
          return agg;
        }, {}),
      };

    case FormActionTypes.ResetHistogramBoundaryTo:
      return {
        ...state,
        histogramFields: Object.keys(state.histogramFields).reduce((agg, k) => {
          let newState;
          const histogramField = state.histogramFields[k];
          if (k === action.payload.key) {
            const histogramValues = histogramField.values;
            const idPos = histogramValues.findIndex((v) => v.id === action.payload.id);
            // preserve manually specified boundaries
            const manualValues = histogramValues.slice(1 + idPos).filter((v) => v.value.pos === undefined);
            newState = {
              ...histogramField,
              values: histogramValues.slice(0, 1 + idPos).concat(manualValues)
            };
          } else {
            newState = histogramField;
          }
          agg[k] = newState;
          return agg;
        }, {}),
      };

    case FormActionTypes.ShowMissingValuesInRange:
      return {
        ...state,
        rangeFields: Object.keys(state['rangeFields']).reduce((agg, k) => {
          agg[k] = k === action.payload ?
            {...state.rangeFields[k], showMissingValues: !state.rangeFields[k].showMissingValues} :
            state['rangeFields'][k];
          return agg;
        }, {})
      };

    case FormActionTypes.SetShowMissingValuesInRange:
      return {
        ...state,
        rangeFields: Object.keys(state['rangeFields']).reduce((agg, k) => {
          agg[k] = k === action.payload ?
            {...state.rangeFields[k], showMissingValues: action.flag} :
            state['rangeFields'][k];
          return agg;
        }, {})
      };

    case FormActionTypes.ResetRange:
      return {
        ...state,
        rangeFields: Object.keys(environment.rangeFields).reduce((agg, k) => {
          agg[k] = action.payload === k ? {
              field: environment.rangeFields[k].field,
              min: environment.rangeFields[k].min,
              from: environment.rangeFields[k].from,
              to: environment.rangeFields[k].to,
              max: environment.rangeFields[k].max,
              showMissingValues: environment.rangeFields[k].showMissingValues,
            } :
            state.rangeFields[k];
          return agg;
        }, {}),
      };

    case FormActionTypes.ResetAll:
      return initialState;

    case FormActionTypes.SetInFacetSearch:
      return {
        ...state,
        facetSearchPrefix: action.prefix,
        facetSearchField: action.field
      };

    default:
      return state;
  }
}
