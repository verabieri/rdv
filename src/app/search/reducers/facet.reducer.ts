/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {FacetActions, FacetActionTypes} from '../actions/facet.actions';
import {FacetFieldModel, FacetFieldType} from "../../shared/models/settings.model";
import {environment} from '../../../environments/environment';

/**
 * Facet state. Comprises different aggregations of current query.
 */
export interface State {
  /**
   * Number of results per subject in respective facet field category
   */
  facetFields: {};
  /**
   * Number of results per step in range in respective facet range category
   */
  facetRanges: any[];
  /**
   * Number of results per step in histogram in respective facet histogram category
   */
  facetHistograms: {};
  /**
   * Number of results per step in hierarchic facets
   */
  hierarchicFacetFields: {};
  /**
   * Number of results per arbitrary query (e.g. has year yes/no) in respective facet queries category
   */
  facetQueries: any;
  /**
   * Number of total results
   */
  total: number;
}

/**
 * @ignore
 */
export const initialState: State = {
  facetFields: {},
  facetRanges: [],
  facetHistograms: {},
  hierarchicFacetFields: {},
  facetQueries: {},
  total: 0,
};

// build inverse lookup table: field -> facet
const inverseFacetLookUp: { [key: string]: FacetFieldModel } = {};
Object.keys(environment.facetFields)
  .forEach((key) => inverseFacetLookUp[environment.facetFields[key].field] = environment.facetFields[key]);

function filterByTypeAndSetId(payload: any, type: FacetFieldType[], defaultValue: any) {
  const valueCounts = {...defaultValue};
  Object.keys(payload).forEach((field) => {
    if (type.indexOf(inverseFacetLookUp[field].facetType) >= 0) {
      valueCounts[field] = payload[field];
      for (const v of valueCounts[field]) {
        v.id = buildId(v.value || v.label);
        if (!v.value) {
          v.value = v.label;
        }
        if (v.sub_values) {
          for (const sv of v.sub_values) {
            sv.id = buildId((sv.value || v.label));
          }
        }
      }
    }
  });
  return valueCounts;
}

/**
 * Build id for facet values. Keys of objects are sorted for a stable comparision.
 *
 * @param json
 * @returns {string}
 */
export function buildId(json): string {
  if (Array.isArray(json)) {
    let s = "[";
    for (const e of json) {
      if (s.length > 1) {
        s += ",";
      }
      s += buildId(e);
    }
    return s + "]";
  } else if (typeof (json) === 'object') {
    const keys = Object.keys(json).sort();
    let s = "{";
    for (const k of keys) {
      if (s.length > 1) {
        s += ",";
      }
      s += JSON.stringify(k) + ":" + buildId(json[k]);
    }
    return s + "}";
  } else {
    // number, string, ...
    return JSON.stringify(json);
  }
}

/**
 * @ignore
 */
export function reducer(state = initialState, action: FacetActions): State {

  switch (action.type) {

    case FacetActionTypes.UpdateFacetFields:
      return {
        ...state,
        facetFields: filterByTypeAndSetId(
          action.payload,
          [FacetFieldType.SIMPLE, FacetFieldType.SUBCATEGORY],
          action.replaceAll ? {} : state.facetFields)
      };

    case FacetActionTypes.UpdateFacetRanges:
      return {...state, facetRanges: action.payload};

    case FacetActionTypes.UpdateFacetHistograms:
      return {
        ...state,
        facetHistograms: filterByTypeAndSetId(
          action.payload,
          [FacetFieldType.HISTOGRAM],
          action.replaceAll ? {} : state.facetHistograms)
      };

    case FacetActionTypes.UpdateHierarchicFacets:
      return {
        ...state,
        hierarchicFacetFields: filterByTypeAndSetId(
          action.payload,
          [FacetFieldType.HIERARCHIC],
          action.replaceAll ? {} : state.hierarchicFacetFields)
      };

    case FacetActionTypes.UpdateFacetQueries:
      return {...state, facetQueries: action.payload};

    case FacetActionTypes.UpdateTotal:
      return {...state, total: action.payload};

    case FacetActionTypes.ResetAll:
      return initialState;

    default:
      return state;
  }
}
