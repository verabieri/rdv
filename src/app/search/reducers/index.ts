/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromForm from './form.reducer';
import * as fromLayout from './layout.reducer';
import * as fromRoot from '../../reducers';
import {memoize} from '../../shared/utils';
import * as fromBasket from './basket.reducer';
import * as fromSavedQuery from './saved-query.reducer';
import * as fromFacet from './facet.reducer';
import * as fromResult from './result.reducer';
import * as fromQuery from './query.reducer';
import {environment} from '../../../environments/environment';
import * as fromBasketResult from './basket-result.reducer';
import * as fromDetailedResult from './detailed-result.reducer';


/**
 * Global state for search
 */
export interface SearchState {
  form: fromForm.State;
  layout: fromLayout.State;
  basket: fromBasket.State;
  savedQuery: fromSavedQuery.State;
  result: fromResult.State;
  facet: fromFacet.State;
  query: fromQuery.State;
  basketResult: fromBasketResult.State;
  detailedResult: fromDetailedResult.State;
}

/**
 * @ignore
 */
export interface State extends fromRoot.State {
  search: SearchState;
}

/**
 * @ignore
 */
export const reducers: ActionReducerMap<SearchState> = {
  form: fromForm.reducer,
  layout: fromLayout.reducer,
  basket: fromBasket.reducer,
  savedQuery: fromSavedQuery.reducer,
  result: fromResult.reducer,
  facet: fromFacet.reducer,
  query: fromQuery.reducer,
  basketResult: fromBasketResult.reducer,
  detailedResult: fromDetailedResult.reducer,
};


/**
 * Feature selector for all state regarding search
 */
const _getSearch = createFeatureSelector<State, SearchState>('search');


/**
 * Selector for all layout state in search view
 */
const _getLayout = createSelector(
  _getSearch,
  (state) => state.layout,
);

/**
 * Selects currently displayed facet or range
 */
export const getShownFacetOrRange = createSelector(
  _getLayout,
  (state) => state.shownFacetOrRange,
);

/**
 * Selects currently display map flag.
 */
export const getDisplayMap = createSelector(
  _getLayout,
  (state) => state.shownMap,
);

/**
 * Selects currently display timeline flag.
 */
export const getDisplayTimeline = createSelector(
  _getLayout,
  (state) => state.shownTimeline,
);

/**
 * Selector for all form values
 */
export const getFormValues = createSelector(
  _getSearch,
  (state) => state.form,
);

/**
 * Selector for ranges-related state
 */
export const getRangeValues = createSelector(
  getFormValues,
  (formValues) => formValues.rangeFields,
);

/**
 * Selector for histogram-related state
 */
export const getHistogramValues = createSelector(
  getFormValues,
  (formValues) => formValues.histogramFields,
);

/**
 * Selector for hierarchic-related state
 */
export const getHierarchyValues = createSelector(
  getFormValues,
  (formValues) => formValues.hierarchyFields,
);

/**
 * Selector for in-facet-search related state
 */
export const getInFacetSearchValues = createSelector(
  getFormValues,
  (formValues) => ({field: formValues.facetSearchField, prefix: formValues.facetSearchPrefix}),
);

/**
 * Selects range values by key
 */
export const getRangeValuesByKey = createSelector(
  getRangeValues,
  (rangeFields) => memoize((key: string) => rangeFields[key]),
);

/**
 * Selector for facets-related state
 */
export const getFacetValues = createSelector(
  getFormValues,
  (formValues) => formValues.facetFields,
);

/**
 * Selects facet values by key
 */
export const getFacetValuesByKey = createSelector(
  getFacetValues,
  (facetFields) => memoize((key: string) => facetFields[key]),
);

/**
 * Selector for fields-related state
 */
export const getSearchValues = createSelector(
  getFormValues,
  (formValues) => formValues.searchFields,
);

/**
 * Selects search values by key
 */
export const getSearchValuesByKey = createSelector(
  getSearchValues,
  (searchFields) => memoize((key: string) => searchFields[key]),
);

/**
 * Selector for filters-related state
 */
const _getFilterValues = createSelector(
  getFormValues,
  (formValues) => formValues.filterFields,
);

/**
 * Selects filter values by key
 */
export const getFilterValuesByKey = createSelector(
  _getFilterValues,
  (filters) => memoize((key: string) => filters[key])
);


/**
 * Selector for baskets-related state
 */
const _getBaskets = createSelector(
  _getSearch,
  (state) => state.basket,
);

/**
 * Counts number of all available baskets
 */
export const getBasketCount = createSelector(
  _getBaskets,
  fromBasket.selectTotal,
);

/**
 * Gets ids of all available baskets
 */
export const getBasketIds = createSelector(
  _getBaskets,
  fromBasket.selectIds,
);

/**
 * Selects all available baskets as list
 */
export const getAllBaskets = createSelector(
  _getBaskets,
  fromBasket.selectAll,
);

/**
 * Selects all available baskets as {@link https://ngrx.io/api/entity/Dictionary Dictionary}
 */
export const getBasketEntities = createSelector(
  _getBaskets,
  fromBasket.selectEntities,
);

/**
 * Selects id of currently active basket
 */
export const getCurrentBasketId = createSelector(
  _getBaskets,
  fromBasket.selectCurrentBasketId,
);

/**
 * Selects currently active basket
 */
export const getCurrentBasket = createSelector(
  getBasketEntities,
  getCurrentBasketId,
  (entities, id) => entities[id],
);

/**
 * Gets number of displayed elements per page of currently active basket
 */
export const getCurrentBasketsDisplayedRows = createSelector(
  getCurrentBasket,
  (curBasket) => curBasket.queryParams.rows,
);

/**
 * Gets total number of elements of currently active basket
 */
export const getCurrentBasketElementsCount = createSelector(
  getCurrentBasket,
  (basket) => basket.ids.length,
);


/**
 * Selector for elements of currently active basket
 */
const _getBasketResults = createSelector(
  _getSearch,
  (state) => state.basketResult,
);

/**
 * Gets all elements of currently active basket as list
 */
export const getAllBasketResults = createSelector(
  _getBasketResults,
  fromBasketResult.selectAll,
);


/**
 * Selector for saved queries
 */
const _getSavedQueries = createSelector(
  _getSearch,
  (state) => state.savedQuery,
);

/**
 * Selects all saved queries as list
 */
export const getAllSavedQueries = createSelector(
  _getSavedQueries,
  fromSavedQuery.selectAll
);

/**
 * Selects all saved queries as {@link https://ngrx.io/api/entity/Dictionary Dictionary}
 */
export const getSavedQueryEntities = createSelector(
  _getSavedQueries,
  fromSavedQuery.selectEntities
);

/**
 * Counts number of saved queries
 */
export const getSavedQueriesCount = createSelector(
  _getSavedQueries,
  fromSavedQuery.selectTotal,
);


/**
 * Selector for facet state
 */
const _getFacetCounts = createSelector(
  _getSearch,
  (search) => search.facet,
);

/**
 * Selector for aggregated counts of subjects
 */
export const getFacetFieldCount = createSelector(
  _getFacetCounts,
  (facetCount) => facetCount.facetFields,
);

/**
 * Gets a specific aggregated count by key
 */
export const getFacetFieldCountByKey = createSelector(
  getFacetFieldCount,
  (facetCounts) => memoize((key: string) => facetCounts[key])
);

/**
 * Selector for counts of steps in ranges
 */
export const getFacetRangeCount = createSelector(
  _getFacetCounts,
  (facetCount) => facetCount.facetRanges,
);

/**
 * Selector for counts of steps in histogram
 */
export const getFacetHistogramCount = createSelector(
  _getFacetCounts,
  (facetCount) => facetCount.facetHistograms,
);

/**
 * Selector for counts of steps in histogram
 */
export const getFacetHistogramCountByKey = createSelector(
  _getFacetCounts,
  (facetCount) => ((key: string) => facetCount.facetHistograms[key]),
);

/**
 * Selector for counts of steps in hierarchic facets
 */
export const getHierarchicFacetCount = createSelector(
  _getFacetCounts,
  (facetCount) => facetCount.hierarchicFacetFields,
);

/**
 * Selector for counts of steps in hierarchic facets
 */
export const getHierarchicFacetCountByKey = createSelector(
  _getFacetCounts,
  (facetCount) => ((key: string) => facetCount.hierarchicFacetFields[key]),
);

/**
 * Selector for counts based on a query
 */
export const getFacetQueryCount = createSelector(
  _getFacetCounts,
  (facetCount) => facetCount.facetQueries,
);

/**
 * Gets a count based on a query by key
 */
export const getFacetQueryCountByKey = createSelector(
  getFacetQueryCount,
  (queryCounts) => memoize((key: string) =>
    queryCounts['{!ex=' + environment.rangeFields[key].field + '}' +
    environment.rangeFields[key].field + ':0']
  )
);

/**
 * Gets the total count of results
 */
export const getTotalResultsCount = createSelector(
  _getFacetCounts,
  (facetCount) => facetCount.total,
);


/**
 * Selector for search results
 */
const _getResults = createSelector(
  _getSearch,
  (search) => search.result,
);

/**
 * Selects all search results as list
 */
export const getAllResults = createSelector(
  _getResults,
  fromResult.selectAll,
);

/**
 * Selects current manifest url
 */
export const getIIIFResultUrl = createSelector(
  _getResults,
  (result) => result.iiifUrl
);

/**
 * Selects current popup info
 */
export const getPopup = createSelector(
  _getResults,
  (result) => result.popup
);


/**
 * Selector for query parameters
 */
export const getQueryParams = createSelector(
  _getSearch,
  (search) => search.query,
);

/**
 * Gets current offset in result list
 */
export const getResultOffset = createSelector(
  getQueryParams,
  (queryParams) => queryParams.offset,
);

/**
 * Gets current number of elements per result list page
 */
export const getResultRows = createSelector(
  getQueryParams,
  (queryParams) => queryParams.rows,
);

/**
 * Gets field by which the result list is sorted
 */
export const getResultSortField = createSelector(
  getQueryParams,
  (queryParams) => queryParams.sortField,
);

/**
 * Gets order by which the result list is sorted
 */
export const getResultSortOrder = createSelector(
  getQueryParams,
  (queryParams) => queryParams.sortOrder,
);

/**
 * Returns how to display search results. If true use
 * IIIF Universal Viewer, otherwise a list.
 */
export const getDisplayResultInIIIF = createSelector(
  getQueryParams,
  (queryParams) => queryParams.showInIIIF,
);


/**
 * Selects different parameters relevant as parameters for search queries. Use it to probe
 * if something has changed and thus a new search query is required.
 */
export const getCombinedSearchQueries = createSelector(
  getFacetValues,
  getRangeValues,
  getSearchValues,
  _getFilterValues,
  (facets, ranges, searchFields, filters) => {
    // const facets = {...origFacets}; // COPY! OTHERWISE form.reducer.ts's initial state is manipulated!
    return {
      facetFields: removeInternalProps(facets),
      rangeFields: {}, // TODO ranges, in old search view
      searchFields: searchFields,
      filterFields: filters,
    }
  }
);

/**
 * Removes e.g. internally used property "id". Only the "value" property is preserved.
 * @param facets
 * @returns {any}
 */
function removeInternalProps(facets) {
  return Object.keys(facets).reduce((agg, key) => {
    const field = {...facets[key]};
    field.values = field.values.map((v) => v.value);
    agg[key] = field;
    return agg;
  }, {});
}

function setFacetType(facets) {
  Object.keys(facets).map((key) => facets[key].facet_type = environment.facetFields[key].facetType);
  return facets;
}

/**
 * Selects different parameters relevant as parameters for search queries. Use it to probe
 * if something has changed and thus a new search query is required.
 */
export const getCombinedNewSearchQueries = createSelector(
  getFacetValues,
  getHistogramValues,
  getHierarchyValues,
  getSearchValues,
  _getFilterValues,
  (facets, histograms, hierarchicFacets, searchFields, filters) => {
    return {
      facetFields: setFacetType(removeInternalProps(facets)),
      histogramFields: removeInternalProps(histograms),
      hierarchicFields: removeInternalProps(hierarchicFacets),
      searchFields: searchFields,
      filterFields: filters,
    }
  }
);

function withoutOptionalInFacetField(facetFields, inFacetSearchField) {
  if (inFacetSearchField) {
    for (const key in facetFields) {
      if (facetFields[key].field === inFacetSearchField) {
        delete facetFields[key];
        break;
      }
    }
  }
  return facetFields;
}

/**
 * Collects in-facet search query params.
 */
export const getCombinedInFacetSearchQueries = createSelector(
  getFacetValues,
  getHistogramValues,
  getHierarchyValues,
  getInFacetSearchValues,
  getSearchValues,
  _getFilterValues,
  (facets, histograms, hierarchicFacets, inFacets, searchFields, filters) => {
    return {
      facetFields: setFacetType(withoutOptionalInFacetField(removeInternalProps(facets), inFacets.field)),
      histogramFields: removeInternalProps(histograms),
      hierarchicFields: removeInternalProps(hierarchicFacets),
      searchFields: searchFields,
      filterFields: filters,
      inFacetSearchPrefix: inFacets.prefix,
      inFacetSearchField: inFacets.field
    }
  }
);

/**
 * Selects detailed results
 */
const _getDetailedResults = createSelector(
  _getSearch,
  (state) => state.detailedResult,
);

/**
 * Gets ids of results of which detailed results are shown
 */
export const getDetailedResultsIds = createSelector(
  _getDetailedResults,
  fromDetailedResult.selectIds,
);

/**
 * Selects all displayed detailed results as {@link https://ngrx.io/api/entity/Dictionary Dictionary}
 */
export const getAllDetailedResults = createSelector(
  _getDetailedResults,
  fromDetailedResult.selectEntities,
);
