/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import * as fromActions from '../actions/query.actions';
import {environment} from '../../../environments/environment';
import {QueryParamsModel} from "../../shared/models/settings.model";

/**
 * Query state
 */
export type State = QueryParamsModel;


/**
 * @ignore
 */
export const initialState: State = {...environment.queryParams};


/**
 * @ignore
 */
export function reducer(state = initialState, action: fromActions.QueryActions): State {
  switch (action.type) {

    case fromActions.QueryActionTypes.SetOffset:
      return {
        ...state,
        offset: action.payload,
      };

    case fromActions.QueryActionTypes.SetRows:
      return {
        ...state,
        rows: action.payload,
      };

    case fromActions.QueryActionTypes.SetSortField:
      return {
        ...state,
        sortField: action.payload,
      };

    case fromActions.QueryActionTypes.SetSortOrder:
      return {
        ...state,
        sortOrder: action.payload,
      };

    case fromActions.QueryActionTypes.ResultListDisplayMode:
      return {
        ...state,
        showInIIIF: action.payload,
      };

    default:
      return state;
  }
}
