/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SearchComponent} from './components/search.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {ClipboardModule} from 'ngx-clipboard';
import {RouterModule, Routes} from '@angular/router';
import {ParamsSetComponent} from './components/params-set.component';
import {CopyLinkComponent} from './components/copy-link.component';
import {SaveQueryComponent} from './components/save-query.component';
import {ManageSavedQueriesComponent} from './components/manage-saved-queries.component';
import {BasketListComponent} from './components/basket-list.component';
import {FieldsComponent} from './components/fields.component';
import {FiltersComponent} from './components/filters.component';
import {VisualSearchComponent} from './components/visual-search.component';
import {PipesModule} from '../shared/pipes';
import {StoreModule} from '@ngrx/store';
import * as fromSearch from './reducers';
import {ManageSearchComponent} from './components/manage-search.component';
import {SearchParamsComponent} from './components/search-params.component';
import {EffectsModule} from '@ngrx/effects';
import {QueryEffects} from './effects/query.effects';
import {FacetsComponent} from './components/facets.component';
import {RangesComponent} from './components/ranges.component';
import {FormEffects} from './effects/form.effects';
import {ResultListsComponent} from './components/result-lists.component';
import {BasketResultsListComponent} from './components/basket-results-list.component';
import {SearchResultsListComponent} from './components/search-results-list.component';
import {BasketIconComponent} from './components/basket-icon.component';
import {ExportResultsListComponent} from './components/export-results-list.component';
import {DisplayLinkDirective} from "./shared/directives/display-link.directive";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {DetailedRequestInterceptor} from "../shared/services/detailed-request-interceptor.service";
import {ResultRowComponent} from './components/result-row.component';
import {ResultPagingComponent} from './components/result-paging.component';
import {ResultHeaderComponent} from './components/result-header.component';
import {RowsPerPageComponent} from './components/rows-per-page.component';
import {BasketRequestInterceptor} from "../shared/services/basket-request-interceptor.service";
import {SearchRequestInterceptor} from "../shared/services/search-request-interceptor.service";
import {NewSearchComponent} from "./components/new-search.component";
import {TopComponent} from "./components/top.components";
import {SimpleSearchComponent} from "./components/simple-search.component";
import {SearchResultsComponent} from "./components/search-results.component";
import {SearchHitComponent} from "./components/search-hit.component";
import {SearchHitValueComponent} from "./components/search-hit-value.component";
import {TranslateModule} from "@ngx-translate/core";
import {LocalizeRouterModule} from "@gilsdav/ngx-translate-router";
import {CollapsibleFacetsComponent} from "./components/collapsible-facets.component";
import {CollapsibleComponent} from "./components/collapsible.component";
import {SimpleFacetComponent} from "./components/simple-facet.component";
import {IntHistogramFacetComponent} from "./components/int-histogram-facet.component";
import {ExpandableComponent} from "./components/expandable.component";
import {MinMaxIntInputComponent} from "./components/min-max-int-input.component";
import {MinMaxDateInputComponent} from "./components/min-max-date-input.component";
import {DateHistogramFacetComponent} from "./components/date-histogram-facet.component";
import {MinMaxGeoInputComponent} from "./components/min-max-geo-input.component";
import {GeoHistogramFacetComponent} from "./components/geo-histogram-facet.component";
import {HierachicFacetComponent} from "./components/hierachic-facet.component";
import {InFacetSearchComponent} from "./components/in-facet-search.component";
import {SimpleCollapsibleFacetComponent} from "./components/simple-collapsible-facet.component";
import {BrokenImageLinkDirective} from "./shared/directives/broken-image-link.directive";
import {DetailedComponent} from "./components/detailed.component";
import {DetailedFieldComponent} from "./components/detailed-field.component";
import {SafePipe} from "./shared/directives/safe-src.directive";
import {SubcatFacetComponent} from "./components/subcat-facet.component";
import {OperatorSelectorComponent} from "./components/operator-selector.component";
import {UniversalViewerComponent} from "./components/universal-viewer.component";
import {UvManifestLinkComponent} from "./components/uv-manifest-link.component";
import {ToastrModule} from 'ngx-toastr';
import {PopupLandingComponent} from "./components/popup-landing.component";
import {DateHierarchicFacetComponent} from "./components/date-hierarchic-facet.component";
import {IntHierarchicFacetComponent} from "./components/int-hierarchic-facet.component";
import {GeoHierarchicFacetComponent} from "./components/geo-hierarchic-facet.component";

const routes: Routes = [
  {path: 'detail/:id', component: DetailedComponent},
];

/**
 * Provides components for search and display of search results
 */
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ChartsModule,
    // IonRangeSliderModule,
    BrowserAnimationsModule,
    NgxChartsModule,
    ClipboardModule,
    PipesModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature('search', fromSearch.reducers),
    EffectsModule.forFeature([QueryEffects, FormEffects]),
    TranslateModule.forChild(),
    LocalizeRouterModule.forChild(routes),
    FormsModule,
    ToastrModule,
  ],
  declarations: [
    SearchComponent,
    NewSearchComponent,
    PopupLandingComponent,
    TopComponent,
    SimpleSearchComponent,
    SearchResultsComponent,
    SearchHitComponent,
    SearchHitValueComponent,
    CollapsibleComponent,
    ExpandableComponent,
    SimpleFacetComponent,
    OperatorSelectorComponent,
    SimpleCollapsibleFacetComponent,
    IntHistogramFacetComponent,
    IntHierarchicFacetComponent,
    DateHistogramFacetComponent,
    DateHierarchicFacetComponent,
    GeoHistogramFacetComponent,
    GeoHierarchicFacetComponent,
    MinMaxIntInputComponent,
    MinMaxDateInputComponent,
    MinMaxGeoInputComponent,
    HierachicFacetComponent,
    CollapsibleFacetsComponent,
    InFacetSearchComponent,
    ManageSearchComponent,
    SearchParamsComponent,
    ParamsSetComponent,
    CopyLinkComponent,
    SaveQueryComponent,
    ManageSavedQueriesComponent,
    BasketListComponent,
    FieldsComponent,
    FiltersComponent,
    VisualSearchComponent,
    FacetsComponent,
    SubcatFacetComponent,
    RangesComponent,
    ResultListsComponent,
    BasketResultsListComponent,
    SearchResultsListComponent,
    BasketIconComponent,
    ExportResultsListComponent,
    DisplayLinkDirective,
    BrokenImageLinkDirective,
    SafePipe,
    ResultRowComponent,
    ResultPagingComponent,
    ResultHeaderComponent,
    RowsPerPageComponent,
    DetailedComponent,
    UniversalViewerComponent,
    UvManifestLinkComponent,
    DetailedFieldComponent,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SearchRequestInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DetailedRequestInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BasketRequestInterceptor,
      multi: true,
    },
  ],
  exports: [
    SearchComponent,
    NewSearchComponent
  ],
})
export class SearchModule {
}
