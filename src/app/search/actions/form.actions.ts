/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Action} from '@ngrx/store';
import {FacetSelection} from "./result.actions";

/***
 * @ignore
 */
export enum FormActionTypes {
  UpdateEntireForm = '[Form] Update entire form',
  ToggleFilterValue = '[Form] Toggle filter in form model',
  UpdateSearchFieldType = '[Form] Update search field type in form model',
  UpdateSearchFieldValue = '[Form] Update search field value in form model',
  AddFacetValue = '[Form] Add facet value in form model',
  RemoveFacetValue = '[Form] Remove facet value in form model',
  RemoveAllFacetValue = '[Form] Remove all facet values in form model',
  UpdateFacetOperator = '[Form] Update Facets in Form Model',
  SelectFacets = '[Result] Select facets',
  RangeBoundariesChanged = '[Form] Boundaries for range in form model changed',
  UpdateRangeBoundaries = '[Form] Set new boundaries for range in form model',
  AddHistogramBoundaries = '[Form] Add more boundaries for histogram in form model',
  RemoveHistogramBoundary = '[Form] Remove boundary of histogram in form model',
  RemoveAllHistogramBoundary = '[Form] Remove all boundary of histogram in form model',
  ResetHistogramBoundaryTo = '[Form] Reset boundary of histogram in form model to given value',
  AddHierarchicFacetValue = '[Form] Add more boundaries for hierarchic field in form model',
  RemoveHierarchicFacetValue = '[Form] Remove boundary of hierarchic field in form model',
  RemoveAllHierarchicFacetValue = '[Form] Remove all boundary of hierarchic field in form model',
  ResetHierarchicFacetValueTo = '[Form] Resets boundary of hierarchic field in form model to given value',
  ResetRange = '[Form] Reset Ranges in Form Model',
  ShowMissingValuesInRange = '[Form] Toggle Show Missing Values',
  SetShowMissingValuesInRange = '[Form] Set Show Missing Values',
  ResetAll = '[Form] Reset All',
  SetInFacetSearch = '[Form] Set in-facet search field and prefix',
}


/**
 * Updates values of the entire form. Currently only used when initializing the form
 * based on a saved query.
 */
export class UpdateEntireForm implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.UpdateEntireForm;

  /**
   * Contains all values of the form
   * @param payload {Object}
   */
  constructor(public payload: any) {
  }
}

/**
 * Switches a filter value
 */
export class ToggleFilterValue implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.ToggleFilterValue;

  /**
   * Contains filter name and new value
   * @param payload {Object}
   */
  constructor(public payload: { filter: string, value: string }) {
  }
}

/**
 * Changes the text search category
 */
export class UpdateSearchFieldType implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.UpdateSearchFieldType;

  /**
   * Contains the name of the affected element
   * and the category name of the new search
   * @param payload {Object}
   */
  constructor(public payload: { field: string, type: string }) {
  }
}

/**
 * Updates the value of the current text search category (i.e. the text in the field
 * has been changed by the user)
 */
export class UpdateSearchFieldValue implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.UpdateSearchFieldValue;

  /**
   * Contains the name of the affected element
   * and the updated value
   * @param payload {Object}
   */
  constructor(public payload: { field: string, value: string }) {
  }
}

/**
 * Adds a new facet filter to form
 */
export class AddFacetValue implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.AddFacetValue;

  /**
   * Contains names of facet and facet element
   * @param payload {Object}
   */
  constructor(public payload: { facet: string, id: string, label: string, value: any }) {
  }
}

/**
 * Removes a facet filter from form
 */
export class RemoveFacetValue implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.RemoveFacetValue;

  /**
   * Contains names of facet and facet element
   * @param payload {Object}
   */
  constructor(public payload: { facet: string, id: string }) {
  }
}

/**
 * Removes all facet filters from form
 */
export class RemoveAllFacetValue implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.RemoveAllFacetValue;

  /**
   * Contains names of facet and facet element
   * @param facet
   */
  constructor(public facet: string) {
  }
}

/**
 * Updates the operator of a facet with which the specific elements are combined
 */
export class UpdateFacetOperator implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.UpdateFacetOperator;

  /**
   * Contains name of facet and new operator value
   * @param payload {Object}
   */
  constructor(public payload: { facet: string, value: string }) {
  }
}

/**
 * If a search returns implied facet selections.
 */
export class SelectFacets implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.SelectFacets;

  /**
   * Contains popup info to be displayed
   * @param selectedFacets
   */
  constructor(public selectedFacets: FacetSelection) {
  }
}

/**
 * Announces that the extension range has been changed by the user.
 *
 * @see UpdateRangeBoundaries
 */
export class RangeBoundariesChanged implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.RangeBoundariesChanged;

  /**
   * Contains id and the new extension coordinates of the range
   * @param payload {Object}
   */
  constructor(public payload: {
    key: string,
    from: number,
    to: number,
  }) {
  }
}

/**
 * Updates the extension of the range. Is normally applied with a short delay
 * after a {@link RangeBoundariesChanged} action was triggered.
 *
 * @see RangeBoundariesChanged
 */
export class UpdateRangeBoundaries implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.UpdateRangeBoundaries;

  /**
   * Contains id and the new extension coordinates of the range
   * @param payload {Object}
   */
  constructor(public payload: {
    key: string,
    from: number,
    to: number,
  }) {
  }
}

/**
 * Adds the extension of the histogram.
 */
export class AddHistogramBoundaries implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.AddHistogramBoundaries;

  /**
   * Contains label and the new extension coordinates of the histogram
   * @param payload {Object}
   */
  constructor(public payload: {
    key: string,
    id: string,
    label: string,
    value: any
  }) {
  }
}

/**
 * Removes the extension from the histogram.
 */
export class RemoveHistogramBoundary implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.RemoveHistogramBoundary;

  /**
   * Contains label of the extension to remove
   * @param payload {Object}
   */
  constructor(public payload: {
    key: string,
    id: string
  }) {
  }
}

/**
 * Removes all extensions from the histogram.
 */
export class RemoveAllHistogramBoundary implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.RemoveAllHistogramBoundary;

  /**
   * Contains label of the extension to remove
   * @param key
   */
  constructor(public key: string, public manualsToo: boolean) {
  }
}

/**
 * Replaces the extension of the hierarchic field.
 */
export class ResetHistogramBoundaryTo implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.ResetHistogramBoundaryTo;

  /**
   * Contains label and the new extension coordinates of the hierarchic field
   * @param payload {Object}
   */
  constructor(public payload: {
    key: string,
    id: string
  }) {
  }
}

/**
 * Adds the extension of the hierarchic field.
 */
export class AddHierarchicFacetValue implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.AddHierarchicFacetValue;

  /**
   * Contains label and the new extension coordinates of the hierarchic field
   * @param payload {Object}
   */
  constructor(public payload: {
    key: string,
    id: string,
    label: string,
    value: any
  }) {
  }
}

/**
 * Removes the extension from the hierarchic field.
 */
export class RemoveHierarchicFacetValue implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.RemoveHierarchicFacetValue;

  /**
   * Contains label of the extension to remove
   * @param payload {Object}
   */
  constructor(public payload: {
    key: string,
    id: string
  }) {
  }
}

/**
 * Removes the extension from the hierarchic field.
 */
export class RemoveAllHierarchicFacetValue implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.RemoveAllHierarchicFacetValue;

  /**
   * Contains label of the extension to remove
   * @param key
   */
  constructor(public key: string) {
  }
}

/**
 * Resets the extension of the hierarchic field to the given value.
 */
export class ResetHierarchicFacetValueTo implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.ResetHierarchicFacetValueTo;

  /**
   * Contains label and the new extension coordinates of the hierarchic field
   * @param payload {Object}
   */
  constructor(public payload: {
    key: string,
    id: string
  }) {
  }
}

/**
 * Resets range coordinates to initial state
 */
export class ResetRange implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.ResetRange;

  /**
   * Contains name of affected range
   * @param payload {string}
   */
  constructor(public payload: string) {
  }
}

/**
 * Toggles if records without a value in the specific range should
 * be shown in the result list
 */
export class ShowMissingValuesInRange implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.ShowMissingValuesInRange;

  /**
   * Contains name of range
   * @param payload {string}
   */
  constructor(public payload: string) {
  }
}

/**
 * Sets if records without a value in the specific range should
 * be shown in the result list
 */
export class SetShowMissingValuesInRange implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.SetShowMissingValuesInRange;

  /**
   * Contains name of range
   * @param payload {string}
   * @param flag
   */
  constructor(public payload: string, public flag: boolean) {
  }
}

/**
 * Resets the entire form to initial state
 */
export class ResetAll implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.ResetAll;
}

/**
 * Store facet field and prefix to search in.
 */
export class SetInFacetSearch implements Action {
  /**
   * @ignore
   */
  readonly type = FormActionTypes.SetInFacetSearch;

  /**
   * @param {string} field
   * @param {string} prefix
   */
  constructor(public field: string, public prefix: string) {
  }
}

/**
 * @ignore
 */
export type FormActions
  = UpdateEntireForm
  | ToggleFilterValue
  | UpdateSearchFieldType
  | UpdateSearchFieldValue
  | AddFacetValue
  | RemoveFacetValue
  | RemoveAllFacetValue
  | UpdateFacetOperator
  | RangeBoundariesChanged
  | UpdateRangeBoundaries
  | AddHistogramBoundaries
  | RemoveHistogramBoundary
  | RemoveAllHistogramBoundary
  | ResetHistogramBoundaryTo
  | AddHierarchicFacetValue
  | RemoveHierarchicFacetValue
  | RemoveAllHierarchicFacetValue
  | ResetHierarchicFacetValueTo
  | ResetRange
  | ShowMissingValuesInRange
  | SetShowMissingValuesInRange
  | ResetAll
  | SetInFacetSearch
  | SelectFacets;
