/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Action} from '@ngrx/store';

/***
 * @ignore
 */
export enum FacetActionTypes {
  ResetAll = '[Facet] Reset all',
  UpdateFacetFields = '[Facet] Update facet fields',
  UpdateFacetRanges = '[Facet] Update facet ranges',
  UpdateFacetHistograms = '[Facet] Update facet histograms',
  UpdateHierarchicFacets = '[Facet] Update hierarchic facets',
  UpdateFacetQueries = '[Facet] Update facet queries',
  UpdateTotal = '[Facet] Update total count',
}

/**
 * Updates record counts per facet elements
 */
export class UpdateFacetFields {
  /**
   * @ignore
   */
  readonly type = FacetActionTypes.UpdateFacetFields;

  /**
   * Contains updated values of all facets
   * @param payload {Object}
   * @param replaceAll {boolean} replace whole value with payload or keep values not listed in payload
   */
  constructor(public payload: any, public replaceAll = true) {
  }
}

/**
 * Updates record counts per step in range facet
 */
export class UpdateFacetRanges {
  /**
   * @ignore
   */
  readonly type = FacetActionTypes.UpdateFacetRanges;

  /**
   * Contains updated values of all ranges
   * @param payload {Object}
   */
  constructor(public payload: any) {
  }
}

/**
 * Updates record counts per step in range facet
 */
export class UpdateFacetHistograms {
  /**
   * @ignore
   */
  readonly type = FacetActionTypes.UpdateFacetHistograms;

  /**
   * Contains updated values of all ranges
   * @param payload {Object}
   * @param replaceAll {boolean} replace whole value with payload or keep values not listed in payload
   */
  constructor(public payload: any, public replaceAll = true) {
  }
}

/**
 * Updates record counts per step in hierarchic facet
 */
export class UpdateHierarchicFacets {
  /**
   * @ignore
   */
  readonly type = FacetActionTypes.UpdateHierarchicFacets;

  /**
   * Contains updated values of all hierarchic facets
   * @param payload {Object}
   * @param replaceAll {boolean} replace whole value with payload or keep values not listed in payload
   */
  constructor(public payload: any, public replaceAll = true) {
  }
}

/**
 * Updates record counts per subquery facet
 */
export class UpdateFacetQueries {
  /**
   * @ignore
   */
  readonly type = FacetActionTypes.UpdateFacetQueries;

  /**
   * Contains updated values of all subqueries
   * @param payload
   */
  constructor(public payload: any) {
  }
}

/**
 * Resets all record counts to initial state
 */
export class ResetAll implements Action {
  /**
   * @ignore
   */
  readonly type = FacetActionTypes.ResetAll;
}

/**
 * Updates total record count
 */
export class UpdateTotal implements Action {
  /**
   * @ignore
   */
  readonly type = FacetActionTypes.UpdateTotal;

  /**
   * Contains new total count
   * @param payload {number}
   */
  constructor(public payload: number) {
  }
}

/**
 * @ignore
 */
export type FacetActions
  = ResetAll
  | UpdateFacetFields
  | UpdateFacetRanges
  | UpdateFacetHistograms
  | UpdateHierarchicFacets
  | UpdateFacetQueries
  | UpdateTotal;

