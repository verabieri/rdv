/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Action} from '@ngrx/store';
import {SortOrder} from "../../shared/models/settings.model";

/***
 * @ignore
 */
export enum QueryActionTypes {
  MakeSearchRequest = '[Query] Make search request',
  MakeDetailedSearchRequest = '[Query] Make detailed search request',
  MakeNextDetailedSearchRequest = '[Query] Execute following detailed search request',
  MakePreviousDetailedSearchRequest = '[Query] Execute preceding detailed search request',
  MakeBasketSearchRequest = '[Query] Make basket request',
  SetOffset = '[Query] Set offset of result view',
  SetRows = '[Query] Set number of results per page',
  SetSortField = '[Query] Set sort field',
  ResultListDisplayMode = '[Query] Set display mode of result list',
  SetSortOrder = '[Query] Set sort order',
  SearchSuccess = '[Query] Search successful',
  SearchFailure = '[Query] Search failed',
  DetailedSearchSuccess = '[Query] Detailed search successful',
  DetailedSearchFailure = '[Query] Detailed search failed',
  BasketSearchSuccess = '[Query] Basket search successful',
  BasketSearchFailure = '[Query] Basket search failed',
  NextPage = '[Query] Load next page',
  NextPageSuccess = '[Query] Next page loading succeeded',
  SimpleSearch = '[Query] Exec simple default search',
  Reset = '[Query] Reset query settings except rows/sort field/order',
  MakeInFacetSearchRequest = '[Query] Make in-facet search request',
  InFacetSearch = '[Query] Exec search in facet',
  InFacetSearchSuccess = '[Query] in-facet search request success',
}

/**
 * Initializes a new search request
 */
export class MakeSearchRequest implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.MakeSearchRequest;

  /**
   * Contains all set search parameters
   * @param payload {Object}
   */
  constructor(public payload: any) {
  }
}

/**
 * Initializes a new request for a detailed result
 */
export class MakeDetailedSearchRequest implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.MakeDetailedSearchRequest;

  /**
   * Contains id of result
   * @param payload
   */
  constructor(public payload: { id: string, fullRecord?: boolean }) {
  }
}

/**
 * Initializes a new request for a detailed result which follows the doc with the given id.
 */
export class MakeNextDetailedSearchRequest implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.MakeNextDetailedSearchRequest;

  /**
   * Contains cursor of result
   * @param payload {any}
   */
  constructor(public payload: any) {
  }
}

/**
 * Initializes a new request for a detailed result which precedes the doc with the given id.
 */
export class MakePreviousDetailedSearchRequest implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.MakePreviousDetailedSearchRequest;

  /**
   * Contains cursor of result
   * @param payload {any}
   */
  constructor(public payload: any) {
  }
}

/**
 * Initializes a new request request for fetching information of records
 * in a basket
 */
export class MakeBasketSearchRequest implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.MakeBasketSearchRequest;

  /**
   * Contains information of the basket and a list of the ids of the records
   * belonging to it
   * @param payload {Object}
   */
  constructor(public payload: any) {
  }
}

/**
 * Initializes a new in-facet search request
 */
export class MakeInFacetSearchRequest implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.MakeInFacetSearchRequest;

  /**
   * Contains all set search parameters
   * @param payload {Object}
   */
  constructor(public payload: any) {
  }
}

/**
 * Sets a new offset in the result list
 */
export class SetOffset implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.SetOffset;

  /**
   * Contains new offset
   * @param payload {number}
   */
  constructor(public payload: number) {
  }
}

/**
 * Sets number of rows on one page in the result list
 */
export class SetRows implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.SetRows;

  /**
   * Contains new number of rows
   * @param payload {number}
   */
  constructor(public payload: number) {
  }
}

/**
 * Sets order by which the result list is sorted
 */
export class SetSortOrder implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.SetSortOrder;

  /**
   * Contains new sort order
   * @param payload {SortOrder}
   */
  constructor(public payload: SortOrder) {
  }
}

/**
 * Sets new field by which the result list is sorted
 */
export class SetSortField implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.SetSortField;

  /**
   * Contains name of field
   * @param payload {string}
   */
  constructor(public payload: string) {
  }
}

/**
 * Sets result list's display mode. If true the IIIF Universal
 * Viewer displays the search results. Otherwise a simple list
 * is used.
 */
export class SetResultListDisplayMode implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.ResultListDisplayMode;

  /**
   * Contains new sort order
   * @param payload {boolean}
   */
  constructor(public payload: boolean) {
  }
}

/**
 * Issued when search is successful
 *
 * @see MakeSearchRequest
 */
export class SearchSuccess implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.SearchSuccess;

  /**
   * Contains result set
   * @param payload {Object}
   */
  constructor(public payload: any) {
  }
}

/**
 * Issued when search has failed
 *
 * @see MakeSearchRequest
 */
export class SearchFailure implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.SearchFailure;

  /**
   * Contains error message
   * @param payload {string}
   */
  constructor(public payload: string) {
  }
}

/**
 * Issued when detailed search is successful
 *
 * @see MakeDetailedSearchRequest
 */
export class DetailedSearchSuccess implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.DetailedSearchSuccess;

  /**
   * Contains result set
   * @param payload {Object}
   */
  constructor(public payload: any) {
  }
}

/**
 * Issued when detailed search has failed
 *
 * @see MakeDetailedSearchRequest
 */
export class DetailedSearchFailure implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.DetailedSearchFailure;

  /**
   * Contains error message
   * @param payload {string}
   */
  constructor(public payload: string) {
  }
}

/**
 * Issued when basket search is successful
 *
 * @see MakeBasketSearchRequest
 */
export class BasketSearchSuccess implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.BasketSearchSuccess;

  /**
   * Contains result set
   * @param payload {Object}
   */
  constructor(public payload: any) {
  }
}

/**
 * Issued when basket search has failed
 *
 * @see MakeBasketSearchRequest
 */
export class BasketSearchFailure implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.BasketSearchFailure;

  /**
   * Contains error message
   * @param payload
   */
  constructor(public payload: string) {
  }
}

/**
 * Load page wise next simple search results of current query and add them to the current results.
 */
export class NextPage implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.NextPage;

  /**
   * Contains success message
   * @param payload the last doc's id
   */
  constructor(public payload: any) {
  }
}

/**
 * Loading of next simple search results succeeded.
 */
export class NextPageSuccess implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.NextPageSuccess;

  /**
   * Contains success message
   * @param payload the received search results
   */
  constructor(public payload: any) {
  }
}

/**
 * Perform a simple text search.
 */
export class SimpleSearch implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.SimpleSearch;

  /**
   * Contains extra request params
   * @param payload the extra request params
   */
  constructor(public payload?: any) {
  }
}

/**
 * Reset search params except rows per page and sort field/order.
 */
export class Reset implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.Reset;
}

/**
 * Perform a in-facet text search.
 */
export class InFacetSearch implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.InFacetSearch;
}

/**
 * Issued when in-facet search is successful
 *
 * @see MakeInFacetSearchRequest
 */
export class InFacetSearchSuccess implements Action {
  /**
   * @ignore
   */
  readonly type = QueryActionTypes.InFacetSearchSuccess;

  /**
   * Contains result set
   * @param payload {Object}
   * @param inFacetSearchField
   */
  constructor(public payload: any, public inFacetSearchField: string) {
  }
}

/**
 * @ignore
 */
export type QueryActions
  = MakeSearchRequest
  | MakeBasketSearchRequest
  | MakeInFacetSearchRequest
  | MakeNextDetailedSearchRequest
  | MakePreviousDetailedSearchRequest
  | SetOffset
  | SetRows
  | SetSortField
  | SetSortOrder
  | SetResultListDisplayMode
  | SearchSuccess
  | SearchFailure
  | BasketSearchSuccess
  | BasketSearchFailure
  | NextPage
  | NextPageSuccess
  | SimpleSearch
  | Reset
  | InFacetSearch
  | InFacetSearchSuccess;
