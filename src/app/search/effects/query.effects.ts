/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';

import * as fromQueryActions from '../actions/query.actions';
import * as fromResultActions from '../actions/result.actions';
import * as fromFacetActions from '../actions/facet.actions';
import * as fromBasketResultActions from '../actions/basket-result.actions';
import * as fromDetailedBasketResultActions from '../actions/detailed-result.actions';
import {catchError, distinctUntilChanged, filter, flatMap, map, switchMap, tap, withLatestFrom} from 'rxjs/operators';
import {BackendSearchService} from '../../shared/services/backend-search.service';
import {of} from "rxjs";
import {BasketResult} from "../models/basket-result.model";
import * as fromFormActions from "../actions/form.actions";
import * as fromSearch from "../reducers";
import {select, Store} from "@ngrx/store";
import {isObject} from "../../shared/utils";
import {EMPTY} from "rxjs";
import {TranslateService} from "@ngx-translate/core";
import {QueryFormat} from "../../shared/models/query-format";

/**
 * Provides effects for queries
 */
@Injectable()
export class QueryEffects {

  /**
   * Triggers a search request when a {@link MakeSearchRequest} action has been
   * issued
   */
  @Effect()
  makeSearchRequest$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.MakeSearchRequest),
    map((action: fromQueryActions.MakeSearchRequest) => action.payload),
    switchMap(query => {
      return this.backendSearchService.getBackendDataComplex(query).pipe(
        map(result => new fromQueryActions.SearchSuccess(result)
        ),
        catchError(err => of(new fromQueryActions.SearchFailure(err)))
      );
    }),
  );

  /**
   * Triggers updates of result list and facets when a search request has been successful
   */
  @Effect()
  searchSuccess$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.SearchSuccess),
    map((action: fromQueryActions.SearchSuccess) => action.payload),
    filter(x => !!x),
    withLatestFrom(this.searchStore.pipe(select(fromSearch.getSearchValues))),
    flatMap(([res, searchFields]) => {
        const actions: any = [
          new fromResultActions.ClearResults(),
          new fromResultActions.AddResults({results: res.snippets}),
          new fromFacetActions.UpdateTotal(res.hits),
          new fromFacetActions.UpdateFacetFields(res.facets ? res.facets : {}),
          new fromFacetActions.UpdateFacetRanges(res.facet_counts ? res.facet_counts.facet_ranges : {}),
          new fromFacetActions.UpdateFacetHistograms(res.facets ? res.facets : {}),
          new fromFacetActions.UpdateHierarchicFacets(res.facets ? res.facets : {}),
          new fromFacetActions.UpdateFacetQueries(res.no_facet_values ? res.no_facet_values : {}),
          new fromResultActions.UpdateIIIFUrl(res.iiif_flex_url ? res.iiif_flex_url : undefined)
        ];
        if (res.selection && res.selection.query_string) {
          actions.push(new fromFormActions.UpdateSearchFieldValue(
            {field: Object.keys(searchFields)[0], value: res.selection.query_string}));
        }
        if (res.selection && res.selection.facets) {
          actions.push(new fromFormActions.SelectFacets(res.selection.facets));
        }
        // process popup info at the end where all response values were already processed, because
        // popup landing view waits on popup change and navigates to search view;
        // only now search view is able to display new values
        actions.push(new fromResultActions.UpdatePopup(res.popup ? res.popup : undefined));
        return actions;
      }
    ));

  /**
   * Triggers resetting of result lists and facets when a search request has failed
   */
  @Effect()
  searchFailure$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.SearchFailure),
    map((action: fromQueryActions.SearchFailure) => action.payload),
    tap(err => console.log(err)),
    flatMap(() => [
      new fromResultActions.ClearResults(),
      new fromFacetActions.ResetAll(),
    ]),
  );

  /**
   * Triggers a search request when a {@link MakeDetailedSearchRequest} action
   * has been issued
   */
  @Effect()
  makeDetailedSearchRequest$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.MakeDetailedSearchRequest),
    flatMap((action: fromQueryActions.MakeDetailedSearchRequest) =>
      this.backendSearchService.getBackendDetailData(action.payload.id, !!action.payload.fullRecord).pipe(
        map(result => new fromQueryActions.DetailedSearchSuccess(result)
        ),
        catchError(err => of(new fromQueryActions.DetailedSearchFailure(err)))
      )),
  );

  /**
   * Triggers a search request when a {@link MakeNextDetailedSearchRequest} action
   * has been issued
   */
  @Effect()
  makeNextDetailedSearchRequest$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.MakeNextDetailedSearchRequest),
    withLatestFrom(this.searchStore.select(fromSearch.getCombinedNewSearchQueries)),
    withLatestFrom(this.searchStore.pipe(select(fromSearch.getQueryParams), map(val => {
        return {
          rangeFields: {},
          queryParams: {
            rows: val.rows,
            sortDir: val.sortOrder,
            sortField: val.sortField,
            start: 0 // special endpoint needs 0! ignore val.offset
          }
        };
      }
    ))),
    map(([[actionData, searchQueries], searchParams]) => {
      const query = Object.assign(searchParams, searchQueries, {lang: this.translate.currentLang});
      return [(actionData as any).payload, query];
    }),
    switchMap(([id, query]) =>
      this.backendSearchService.navBackendDetailData(id, query, true).pipe(
        map(result => new fromQueryActions.DetailedSearchSuccess(result)
        ),
        catchError(err => of(new fromQueryActions.DetailedSearchFailure(err)))
      )),
  );

  /**
   * Triggers a search request when a {@link MakePreviousDetailedSearchRequest} action
   * has been issued
   */
  @Effect()
  makePreviousDetailedSearchRequest$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.MakePreviousDetailedSearchRequest),
    withLatestFrom(this.searchStore.select(fromSearch.getCombinedNewSearchQueries)),
    withLatestFrom(this.searchStore.pipe(select(fromSearch.getQueryParams), map(val => {
        return {
          rangeFields: {},
          queryParams: {
            rows: val.rows,
            sortDir: val.sortOrder,
            sortField: val.sortField,
            start: 0 // special endpoint needs 0! ignore val.offset
          }
        };
      }
    ))),
    map(([[actionData, searchQueries], searchParams]) => {
      const query = Object.assign(searchParams, searchQueries, {lang: this.translate.currentLang});
      return [(actionData as any).payload, query];
    }),
    switchMap(([id, query]) =>
      this.backendSearchService.navBackendDetailData(id, query, false).pipe(
        map(result => new fromQueryActions.DetailedSearchSuccess(result)
        ),
        catchError(err => of(new fromQueryActions.DetailedSearchFailure(err)))
      )),
  );

  /**
   * Triggers adding of detailed result when a search request for a detailed result
   * has been successful
   */
  @Effect()
  detailedSearchSuccess$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.DetailedSearchSuccess),
    map((action: fromQueryActions.DetailedSearchSuccess) => action.payload),
    filter(x => !!x && isObject(x)),
    map(res =>
      new fromDetailedBasketResultActions.AddDetailedResult({detailedResult: res}),
    ));

  /**
   * Prints an error message to console when a search request for a detailed result
   * has failed
   */
  @Effect()
  detailedSearchFailure$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.DetailedSearchFailure),
    map((action: fromQueryActions.DetailedSearchFailure) => action.payload),
    tap(err => console.log(err)),
    flatMap( () => EMPTY)
  );

  /**
   * Triggers a search request when a {@link MakeBasketSearchRequest} action
   * has been issued
   */
  @Effect()
  makeBasketRequest$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.MakeBasketSearchRequest),
    distinctUntilChanged(),
    map((action: fromQueryActions.MakeBasketSearchRequest) => action.payload),
    switchMap(query =>
      this.backendSearchService.getBackendDataBasket(query).pipe(
        map(result => new fromQueryActions.BasketSearchSuccess(result.snippets)
        ),
        catchError(err => of(new fromQueryActions.BasketSearchFailure(err)))
      )),
  );

  /**
   * Triggers adding of results to basket when search request has been successful
   */
  @Effect()
  basketSearchSuccess$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.BasketSearchSuccess),
    map((action: fromQueryActions.BasketSearchSuccess) => action.payload),
    filter((x: BasketResult[]) => x.length > 0),
    map(res =>
      new fromBasketResultActions.AddBasketResults({basketResults: res})
    ));

  /**
   * Removes all basket elements when search request has failed
   */
  @Effect()
  basketSearchFailure$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.BasketSearchFailure),
    map((action: fromQueryActions.BasketSearchFailure) => action.payload),
    tap(err => console.log(err)),
    flatMap(() => [
      new fromBasketResultActions.ClearBasketResults(),
    ]),
  );

  /**
   * Load next simple search results page.
   */
  @Effect()
  nextPage$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.NextPage),
    withLatestFrom(this.searchStore.select(fromSearch.getCombinedNewSearchQueries)),
    withLatestFrom(this.searchStore.pipe(select(fromSearch.getQueryParams), map(val => {
        return {
          rangeFields: {},
          queryParams: {
            rows: val.rows,
            sortDir: val.sortOrder.toString(),
            sortField: val.sortField,
            start: 0 // special endpoint needs 0! ignore val.offset
          }
        };
      }
    ))),
    map(([[actionData, searchQueries], searchParams]) => {
      const query = Object.assign(searchParams, searchQueries, {lang: this.translate.currentLang});
      query["search_after"] = (actionData as any).payload;
      return query;
    }),
    switchMap((query: QueryFormat) => {
      query.lang = this.translate.currentLang;
      return this.backendSearchService.getMoreBackendDataComplex(query).pipe(
        map(result => new fromQueryActions.NextPageSuccess(result)
        ),
        catchError(err => of(new fromQueryActions.SearchFailure(err)))
      );
    }),
  );

  /**
   * Loading of next simple search result page succeeded. The page is added to the existing search results.
   */
  @Effect()
  nextPageSuccess$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.NextPageSuccess),
    map((action: fromQueryActions.NextPageSuccess) => action.payload),
    filter(x => !!x),
    map(res =>
      new fromResultActions.AddResults({results: res}),
    ));

  /**
   * Cleans current search result. Resets facet/filter/range/field search params. Resets current offset to 0.
   */
  @Effect()
  reset$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.Reset),
    flatMap(() => [
        new fromResultActions.ClearResults(),
        new fromFormActions.ResetAll(),
        new fromFormActions.SetInFacetSearch(undefined, undefined),
        new fromQueryActions.SetOffset(0),
      ]
    )
  );

  /**
   * Process simple text search with current search settings.
   */
  @Effect()
  simpleSearch$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.SimpleSearch),
    map((action: fromQueryActions.SimpleSearch) => action.payload || {}),
    withLatestFrom(this.searchStore.select(fromSearch.getCombinedNewSearchQueries)),
    map(([extraParams, searchQueries]) => Object.assign(searchQueries, extraParams)),
    withLatestFrom(this.searchStore.pipe(select(fromSearch.getQueryParams), map(val => {
        return {
          rangeFields: {},
          queryParams: {
            rows: val.rows,
            sortDir: val.sortOrder,
            sortField: val.sortField,
            start: val.offset
          },
          resultInIIIF: val.showInIIIF
        };
      }
    ))),
    map(([searchQueries, searchParams]) => Object.assign(searchParams, searchQueries, {lang: this.translate.currentLang})),
    switchMap((query) => [new fromQueryActions.MakeSearchRequest(query)])
  );

  /**
   * Triggers a in-facet search request when a {@link MakeInFacetSearchRequest} action has been
   * issued
   */
  @Effect()
  makeInFacetSearchRequest$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.MakeInFacetSearchRequest),
    map((action: fromQueryActions.MakeInFacetSearchRequest) => action.payload),
    switchMap(query => {
      return this.backendSearchService.getInFacetSearchBackendData(query).pipe(
        map(result => new fromQueryActions.InFacetSearchSuccess(result, query.inFacetSearchField)
        ),
        catchError(err => of(new fromQueryActions.SearchFailure(err)))
      );
    }),
  );

  /**
   * Process simple text search with current search settings.
   */
  @Effect()
  inFacetSearch$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.InFacetSearch),
    withLatestFrom(this.searchStore.select(fromSearch.getCombinedInFacetSearchQueries)),
    map(([, searchQueries]) => searchQueries),
    withLatestFrom(this.searchStore.pipe(select(fromSearch.getQueryParams), map(val => {
        return {
          rangeFields: {},
          queryParams: {
            rows: val.rows,
            sortDir: val.sortOrder,
            sortField: val.sortField,
            start: val.offset
          }
        };
      }
    ))),
    map(([searchQueries, searchParams]) => Object.assign(searchParams, searchQueries, {lang: this.translate.currentLang})),
    switchMap((query) => [new fromQueryActions.MakeInFacetSearchRequest(query)])
  );

  /**
   * In-facet search succeeded. Replace facet values.
   */
  @Effect()
  inFacetSearchSuccess$ = this.actions$.pipe(
    ofType(fromQueryActions.QueryActionTypes.InFacetSearchSuccess),
    filter((action: fromQueryActions.InFacetSearchSuccess) =>
      !!action.payload && action.payload[action.inFacetSearchField]),
    flatMap((action: fromQueryActions.InFacetSearchSuccess) => [
      new fromFacetActions.UpdateFacetFields(action.payload, false),
      new fromFacetActions.UpdateFacetHistograms(action.payload, false),
      new fromFacetActions.UpdateHierarchicFacets(action.payload, false),
    ])
  );

  /**
   * @ignore
   */
  constructor(private actions$: Actions,
              private backendSearchService: BackendSearchService,
              private searchStore: Store<fromSearch.State>,
              protected translate: TranslateService) {
  }
}
