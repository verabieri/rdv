/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {RemoteFilterConfigsActions, RemoteFilterConfigsActionTypes} from '../actions/remote-filter-configs.actions';
import {environment} from '../../../environments/environment';
import {mergeDeep} from '../../shared/utils';


/**
 * State slice containing remotely fetched filter fields and probably fetching errors
 */
export interface State {
  /**
   * Object of filter fields
   */
  filterFields: any;
  /**
   * Fetching errors (if any)
   */
  error: any;
}


/**
 * @ignore
 */
export const initialState: State = {
  filterFields: environment.filterFields,
  error: '',
};

/**
 * Adds named filter field with options
 *
 * @param {string} key Name of filter field
 * @param {string} value Option
 */
function setFilterOption(key: string, value: string) {
  const filterFields = {filterFields: {}};
  filterFields[key] = {options: value};
  return filterFields;
}

/**
 * @ignore
 */
export function reducer(state = initialState, action: RemoteFilterConfigsActions): State {
  switch (action.type) {

    case RemoteFilterConfigsActionTypes.SetRemoteFilterFieldOptions:
      return mergeDeep(state, setFilterOption(action.payload.key, action.payload.value));

    case RemoteFilterConfigsActionTypes.OptionRetrieveError:
      return {
        ...state,
        error: action.payload,
      };

    default:
      return state;
  }
}
