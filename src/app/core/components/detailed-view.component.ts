/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BackendSearchService} from '../../shared/services/backend-search.service';

/**
 * Shows detailed information on a document
 */
@Component({
  selector: 'app-landing-page',
  template: `
      <div class="container mt-2"
           *ngIf="document">
          <div class="no-gutters p-2 bg-info text-white rounded">

              <!-- Data: {{landingpageData | json}} -->

              <h3>{{document.Titel}}</h3>
              <!--
              <h5>
                <div ="let person of landingpageData.Person">
                  {{person}} [Autor]
                </div>
              </h5>
            -->
              <ul>
                  <li>Verfasser/in: {{document['Verfasser\/in']}}</li>
                  <li>Sprache: {{document.Sprache}}</li>
                  <li>Form: {{document.Form}}</li>
                  <li>Jahr: {{document.Jahr}}</li>
                  <li>Kommentar: {{document.Kommentar}}</li>
              </ul>
          </div>
      </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailedViewComponent implements OnInit {
  /**
   * Identifier of document
   */
  id: string;

  /**
   * Object with document information
   */
  document;

  /**
   * @ignore
   */
  constructor(private _backendSearchService: BackendSearchService, private _route: ActivatedRoute) {
  }

  /**
   * Retrieves document data from backend
   */
  ngOnInit() {
    this._route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this._backendSearchService
        .getBackendDetailData(this.id, true)
        .subscribe(res => this.document = res);
    });
  }
}
