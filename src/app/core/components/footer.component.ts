/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component} from '@angular/core';
import {environment} from "../../../environments/environment";
import {SettingsModel} from "../../shared/models/settings.model";
import {TranslateService} from "@ngx-translate/core";

/**
 * Application footer component
 */
@Component({
  selector: 'app-footer',
  template: `
    <div class="to-top-container mr-3 mr-sm-0">
      <button title="{{'footer.to_top' | translate}}" (click)="scrollToTop()" class="btn to-top">
        {{'footer.to_top_hovered' | translate}}
      </button>
    </div>
    <footer class="footer-container">
      <div class="footer row no-gutters justify-content-start pb-5 pb-sm-0 align-items-center pt-3 pt-sm-0">
        <span class="d-block d-sm-inline">
          <a href="https://www.unibas.ch/" target="_blank">© Universität Basel</a> /&nbsp;
        </span>
        <div class="d-block d-sm-none" style="width: 100%;"></div>
        <span class="d-block d-sm-inline">
          <a [routerLink]="['/' | localize]">{{env.name}}</a> /&nbsp;
        </span>
        <span class="d-block d-sm-inline col-12 col-sm">
          <a href="{{'https://www.unibas.ch/' + currentLanguage() + '/Impressum.html'}}" target="_blank">Impressum</a>
        </span>
      </div>
    </footer>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent {

  constructor(protected translate: TranslateService) {
  }

  get env(): SettingsModel {
    return environment;
  }

  currentLanguage(): string {
    // currently www.unibas.ch only supports "de"
    return "de";
    // return this.translate.currentLang;
  }

  scrollToTop(): void {
    window.scrollTo({top: 0, left: 0, behavior: "smooth"});
  }
}
