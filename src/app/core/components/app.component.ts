/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component} from "@angular/core";
import {Store} from '@ngrx/store';

import {environment} from '../../../environments/environment';
import * as fromRoot from '../../reducers';
import * as fromActions from '../actions/remote-filter-configs.actions';
import {SettingsModel} from "../../shared/models/settings.model";

/**
 * Main entry point to the application.
 */
@Component({
  selector: 'app-root',
  template: `
    <app-beta-header *ngIf="env.headerSettings?.showBetaBar"
                     [contact]="env.headerSettings?.betaBarContact"></app-beta-header>
    <app-header></app-header>
    <router-outlet class="no-gutters"></router-outlet>
    <app-footer></app-footer>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class AppComponent {

  /**
   * @ignore
   */
  constructor(private _rootStore: Store<fromRoot.State>) {
    this._fetchRemoteFilterConfigs();
  }

  /**
   * Triggers remote fetching of filter options if respective filter value is an URL
   */
  private _fetchRemoteFilterConfigs() {
    for (const key of Object.keys(environment.filterFields)) {
      if (environment.filterFields[key].url) {
        this._rootStore.dispatch(
          new fromActions.GetRemoteFilterFieldOptions({key: key, url: environment.filterFields[key].url})
        );
      }
    }
  }

  get env(): SettingsModel {
    return environment;
  }
}
