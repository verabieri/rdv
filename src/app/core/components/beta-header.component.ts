/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {environment} from "../../../environments/environment";
import {SettingsModel} from "../../shared/models/settings.model";
import {TranslateService} from "@ngx-translate/core";

/**
 * Application footer component
 */
@Component({
  selector: 'app-beta-header',
  template: `
    <div class="beta-header d-flex align-items-center" *ngIf="hidden !== true">
      <span class="col beta-header__title">{{'beta-header.title' | translate}}</span>
      <span class="beta-header__contact" *ngIf="!!contact">
        {{'beta-header.contact' | translate}}
        <a class="beta-header__mailto" href="mailto:{{contact.email}}">{{contact.name}}</a>
      </span>
      <button [title]="'beta-header.hide' | translate"
              class="button--delete"
              (click)="hideBetaBar()"></button>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BetaHeaderComponent {
  @Input() contact?: { name: string, email: string };
  hidden: boolean;

  constructor(protected translate: TranslateService) {
  }

  get env(): SettingsModel {
    return environment;
  }

  hideBetaBar() {
    this.hidden = true;
  }

}
